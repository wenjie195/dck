<?php
class BonusReport {
    /* Member variables */
    // var $referralName,$referralId,$referrerName,$referrerId,$currentLevel,$topReferrerId,$dateCreated,$registerDownlineNo,$amount;
    var $referrerId,$referrerName,$referralId,$referralName,$currentLevel,$topReferrerId,$registerDownlineNo,$amount,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getReferrerId()
    {
        return $this->referrerId;
    }

    /**
     * @param mixed $id
     */
    public function setReferrerId($referrerId)
    {
        $this->referrerId = $referrerId;
    }

    /**
     * @return mixed
     */
    public function getReferrerName()
    {
        return $this->referrerName;
    }

    /**
     * @param mixed $id
     */
    public function setReferrerName($referrerName)
    {
        $this->referrerName = $referrerName;
    }

    /**
     * @return mixed
     */
    public function getReferralId()
    {
        return $this->referralId;
    }

    /**
     * @param mixed $id
     */
    public function setReferralId($referralId)
    {
        $this->referralId = $referralId;
    }

    /**
     * @return mixed
     */
    public function getReferralName()
    {
        return $this->referralName;
    }

    /**
     * @param mixed $id
     */
    public function setReferralName($referralName)
    {
        $this->referralName = $referralName;
    }

    /**
     * @return mixed
     */
    public function getCurrentLevel()
    {
        return $this->currentLevel;
    }

    /**
     * @param mixed $id
     */
    public function setCurrentLevel($currentLevel)
    {
        $this->currentLevel = $currentLevel;
    }

    /**
     * @return mixed
     */
    public function getTopReferrerId()
    {
        return $this->topReferrerId;
    }

    /**
     * @param mixed $id
     */
    public function setTopReferrerId($topReferrerId)
    {
        $this->topReferrerId = $topReferrerId;
    }

    /**
     * @return mixed
     */
    public function getRegisterDownlineNo()
    {
        return $this->registerDownlineNo;
    }

    /**
     * @param mixed $id
     */
    public function setRegisterDownlineNo($registerDownlineNo)
    {
        $this->registerDownlineNo = $registerDownlineNo;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $id
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $id
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $id
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getBonusReport($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("referrer_id","referrer_name","referral_id","referral_name","current_level",
        "top_referrer_id","register_downline_no","amount","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"bonus");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        // $stmt->bind_result($referralName,$referralId,$referrerName,$referrerId,$currentLevel,$topReferrerId,$dateCreated,$registerDownlineNo,$amount);
      
        $stmt->bind_result($referrerId,$referrerName,$referralId,$referralName,$currentLevel,$topReferrerId,$registerDownlineNo,$amount,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new BonusReport();
            $class->setReferrerId($referrerId);
            $class->setReferrerName($referrerName);
            $class->setReferralId($referralId);
            $class->setReferralName($referralName);
            $class->setCurrentLevel($currentLevel);
            $class->setTopReferrerId($topReferrerId);
            $class->setRegisterDownlineNo($registerDownlineNo);
            $class->setAmount($amount);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }

}
