<?php
class Country{
    var $id, $sortname, $name, $phonecode;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSortname()
    {
        return $this->sortname;
    }

    /**
     * @param mixed $sortname
     */
    public function setSortname($sortname)
    {
        $this->sortname = $sortname;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPhonecode()
    {
        return $this->phonecode;
    }

    /**
     * @param mixed $phonecode
     */
    public function setPhonecode($phonecode)
    {
        $this->phonecode = $phonecode;
    }

}

function getCountry($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","sortname","name","phonecode");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"countries");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $sortname, $name, $phonecode);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Country();
            $class->setId($id);
            $class->setSortname($sortname);
            $class->setName($name);
            $class->setPhonecode($phonecode);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function getCountryIdByPhoneCode($conn,$countryCode){
    $countryRows = getCountry($conn," WHERE phonecode = ? ",array("phonecode"),array($countryCode),"i");
    if($countryRows){
        return $countryRows[0]->getId();
    }else{
        return null;
    }
}