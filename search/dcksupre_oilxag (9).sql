-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 08, 2019 at 09:40 AM
-- Server version: 5.7.28-cll-lve
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dcksupre_oilxag`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_privileges`
--

CREATE TABLE `admin_privileges` (
  `admin_id` int(255) NOT NULL,
  `admin_uid` varchar(255) CHARACTER SET utf8 NOT NULL,
  `admin_status` int(2) NOT NULL DEFAULT '0' COMMENT '0 = Inactive,1 = Active',
  `admin_accessCrudAdmin` int(2) NOT NULL DEFAULT '0' COMMENT '0 = cannot access, 1 = can access',
  `admin_accessViewSales` int(2) NOT NULL DEFAULT '0' COMMENT '0 = cannot access, 1 = can access',
  `admin_accessViewPayout` int(2) NOT NULL DEFAULT '0' COMMENT '0 = cannot access, 1 = can access',
  `admin_accessShippingRequest` int(2) NOT NULL DEFAULT '0' COMMENT '0 = cannot access, 1 = can access',
  `admin_accessPresetReason` int(2) NOT NULL DEFAULT '0' COMMENT '0 = cannot access, 1 = can access',
  `admin_accessCrudProduct` int(2) NOT NULL DEFAULT '0' COMMENT '0 = cannot access, 1 = can access',
  `admin_accessViewProduct` int(2) NOT NULL DEFAULT '0' COMMENT '0 = cannot access, 1 = can access',
  `admin_accessAnnouncement` int(2) NOT NULL DEFAULT '0' COMMENT '0 = cannot access, 1 = can access',
  `admin_accessWithdrawalRequest` int(2) NOT NULL DEFAULT '0' COMMENT '0 = cannot access, 1 = can access',
  `admin_accessAdjustComission` int(2) NOT NULL DEFAULT '0' COMMENT '0 = cannot access, 1 = can access'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `announce_id` int(255) NOT NULL,
  `announce_message` varchar(25000) NOT NULL,
  `announce_showThis` int(2) NOT NULL,
  `announce_dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `announce_lastUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`announce_id`, `announce_message`, `announce_showThis`, `announce_dateCreated`, `announce_lastUpdated`) VALUES
(1, 'Thanks for joining us, We welcome u to our journey', 0, '2019-08-14 04:17:07', '2019-10-15 07:09:50'),
(2, 'Have A nice day ! Fizo', 0, '2019-08-14 04:29:46', '2019-10-15 07:09:53'),
(3, 'Welcome To Oilxag People', 0, '2019-08-14 06:24:43', '2019-10-15 07:09:56'),
(4, 'Mun cun  eeedsafafasdfasd', 0, '2019-08-15 02:01:49', '2019-08-15 02:24:18'),
(5, 'asdsdaasdccc vvvvvvvv', 0, '2019-08-15 02:26:06', '2019-08-15 02:26:17'),
(6, '123123213', 0, '2019-08-15 02:26:24', '2019-10-15 07:09:46');

-- --------------------------------------------------------

--
-- Table structure for table `bonus`
--

CREATE TABLE `bonus` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referrer_name` varchar(255) DEFAULT NULL,
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `register_downline_no` int(255) DEFAULT NULL,
  `amount` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bonus`
--

INSERT INTO `bonus` (`id`, `referrer_id`, `referrer_name`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `register_downline_no`, `amount`, `date_created`, `date_updated`) VALUES
(45, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', '8d335b3a10125e39ce46d611d88b2b4a', 'DCK1', 1, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-16 01:41:39', '2019-10-24 02:37:45'),
(46, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', '91fa223cd25fec9982d166cf551639d6', 'DCK2', 1, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-16 01:42:54', '2019-10-24 02:37:47'),
(47, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 'f9b60b8d0eb3c59b2ab4b36e3ddbcd61', 'DCK3', 1, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-10-16 01:43:07', '2019-10-24 02:37:50'),
(48, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 'f15107a4ec3d75eebc75e1b912dc8d63', 'DCK4', 1, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-10-16 01:43:16', '2019-10-24 02:37:52'),
(49, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 'a7a9de836484b55e3bb06d568a722400', 'DCK5', 1, '78ae38b91ca93c0f76b1a5487376d02d', 5, 50, '2019-10-16 01:43:23', '2019-10-24 02:37:54'),
(50, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 'f27cdb430d061807a0e4f4993d7af9df', 'DCK6', 1, '78ae38b91ca93c0f76b1a5487376d02d', 6, 50, '2019-10-16 01:43:31', '2019-10-24 02:37:56'),
(56, '8d335b3a10125e39ce46d611d88b2b4a', 'DCK1', '86d0613407b61357a5bb1300832b86ff', 'Jacky181', 2, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-19 07:40:09', '2019-10-24 02:37:16'),
(57, '86d0613407b61357a5bb1300832b86ff', 'Jacky181', '7d85a3e759a8e44d79b00749e33d5391', 'wendy', 3, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-19 07:49:54', '2019-10-24 02:01:29'),
(58, '86d0613407b61357a5bb1300832b86ff', 'Jacky181', '3631890bc9a862b5f7ceac83c4f5d1e8', 'Ectan', 3, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-19 08:02:21', '2019-10-24 02:01:31'),
(59, '86d0613407b61357a5bb1300832b86ff', 'Jacky181', '53fca61f0dff423502d344054b799ba2', 'Chia2123', 3, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-10-19 08:12:16', '2019-10-24 02:01:33'),
(60, '86d0613407b61357a5bb1300832b86ff', 'Jacky181', '0c396af354a0bda95e6114a77ecc4cfa', 'shuennchi2020', 3, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-10-19 08:22:53', '2019-10-24 02:01:35'),
(61, '91fa223cd25fec9982d166cf551639d6', 'DCK2', '7673d904296328fe996510a9a7c504b0', 'Jwong', 2, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-19 08:37:59', '2019-10-24 02:37:18'),
(62, 'f9b60b8d0eb3c59b2ab4b36e3ddbcd61', 'DCK3', '2402a716d533bd79d3d764be9c1d645a', 'Windlim', 2, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-19 09:04:02', '2019-10-24 02:37:20'),
(63, '2402a716d533bd79d3d764be9c1d645a', 'Windlim', 'a30ac6b232b13c4b187ec873ae16f1a1', 'Teik', 3, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-19 09:10:46', '2019-10-24 02:09:59'),
(64, '2402a716d533bd79d3d764be9c1d645a', 'Windlim', '38eada4b5b84b758002df7d74dc4722e', 'Erickooi', 3, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-19 09:17:43', '2019-10-24 02:10:01'),
(65, 'f15107a4ec3d75eebc75e1b912dc8d63', 'DCK4', '39b50f80956cd29ebf4f975c0b579c53', 'dy7329', 2, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-19 22:47:59', '2019-10-24 02:37:24'),
(66, '39b50f80956cd29ebf4f975c0b579c53', 'dy7329', '197c08e85e66915dc9aeca5759cce4ec', 'cchow', 3, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-19 22:55:23', '2019-10-24 02:12:04'),
(67, '197c08e85e66915dc9aeca5759cce4ec', 'cchow', 'c075645287643d2d5a0a590cbabe84bb', 'cschang', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-19 23:01:56', '2019-10-24 02:12:51'),
(68, '197c08e85e66915dc9aeca5759cce4ec', 'cchow', 'e516b51931fb08b4612433fb51005c56', 'meiyong', 4, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-19 23:07:11', '2019-10-24 02:12:54'),
(69, '197c08e85e66915dc9aeca5759cce4ec', 'cchow', '773a0ae6095f0ba243ec16c3ea818ff6', 'Layyean', 4, '78ae38b91ca93c0f76b1a5487376d02d', 3, 150, '2019-10-19 23:13:17', '2019-10-24 02:12:56'),
(70, '197c08e85e66915dc9aeca5759cce4ec', 'cchow', '415c193d1efa340d3a5ddf2455efa7e2', 'Kimleang', 4, '78ae38b91ca93c0f76b1a5487376d02d', 4, 150, '2019-10-19 23:19:23', '2019-10-24 02:12:58'),
(71, '197c08e85e66915dc9aeca5759cce4ec', 'cchow', '876fd5c039eb5a7006d65f550941a81e', 'suiyuan', 4, '78ae38b91ca93c0f76b1a5487376d02d', 5, 150, '2019-10-19 23:23:48', '2019-10-24 02:13:00'),
(72, '197c08e85e66915dc9aeca5759cce4ec', 'cchow', '91ddb6bea21b3218bd7c52c8a13e4cbc', 'galaxykc', 4, '78ae38b91ca93c0f76b1a5487376d02d', 6, 150, '2019-10-19 23:29:49', '2019-10-24 02:13:03'),
(73, '38eada4b5b84b758002df7d74dc4722e', 'Erickooi', 'b535adb1e7e856a84134d9cbdb969e8a', 'Pinqin0426', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-20 00:46:28', '2019-10-24 02:10:51'),
(74, '86d0613407b61357a5bb1300832b86ff', 'Jacky181', '3f6145490f75b943f067e2506cea0454', 'Fattchai', 3, '78ae38b91ca93c0f76b1a5487376d02d', 5, 50, '2019-10-20 07:28:19', '2019-10-24 02:01:37'),
(75, '86d0613407b61357a5bb1300832b86ff', 'Jacky181', '430a4456ee40d1707c8a44cceb0c9f65', 'ilifestyles', 3, '78ae38b91ca93c0f76b1a5487376d02d', 6, 50, '2019-10-20 07:30:34', '2019-10-24 02:01:39'),
(76, '3631890bc9a862b5f7ceac83c4f5d1e8', 'Ectan', 'e57eba868ce93e56f3f31196f0e8caa0', 'Anson', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-20 07:49:37', '2019-10-24 02:35:36'),
(77, '53fca61f0dff423502d344054b799ba2', 'Chia2123', 'a9e242dc63ba85f2181e621b9085262b', 'chiayiting91', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-20 07:57:45', '2019-10-24 02:36:08'),
(78, '0c396af354a0bda95e6114a77ecc4cfa', 'shuennchi2020', '8ca2402c6c3f7643a93af5eeea795cdd', 'Josieloh', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-20 08:09:38', '2019-10-24 02:33:13'),
(79, '0c396af354a0bda95e6114a77ecc4cfa', 'shuennchi2020', '1b33037d3f15da3ebffe7511defe6c47', 'laypheng', 4, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-20 08:19:50', '2019-10-24 02:33:14'),
(80, '0c396af354a0bda95e6114a77ecc4cfa', 'shuennchi2020', 'e7f97247d154c2344218034b94b466c1', 'ktchew', 4, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-10-20 08:26:08', '2019-10-24 02:33:18'),
(81, '0c396af354a0bda95e6114a77ecc4cfa', 'shuennchi2020', 'bb97e777595704a90693bc6a0b0a7b92', 'Mike', 4, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-10-20 08:32:55', '2019-10-24 02:33:20'),
(82, '0c396af354a0bda95e6114a77ecc4cfa', 'shuennchi2020', '8ece43cdc970996fbdca121a9194815a', 'Leo', 4, '78ae38b91ca93c0f76b1a5487376d02d', 5, 50, '2019-10-20 08:42:02', '2019-10-24 02:33:21'),
(83, '0c396af354a0bda95e6114a77ecc4cfa', 'shuennchi2020', '54baf180ba2c6838a5aa0545d0ad1734', 'st4589', 4, '78ae38b91ca93c0f76b1a5487376d02d', 6, 50, '2019-10-20 08:50:37', '2019-10-24 02:33:23'),
(84, 'bb97e777595704a90693bc6a0b0a7b92', 'Mike', '47b4c06089ca56a99379a89e99e57dd1', 'Joelktan', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-20 09:07:22', '2019-10-24 02:33:44'),
(85, '1b33037d3f15da3ebffe7511defe6c47', 'laypheng', '593e0d4cf3db550d08da2a90cff39ed5', 'shunyun', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-20 09:15:31', '2019-10-24 02:32:13'),
(86, '39b50f80956cd29ebf4f975c0b579c53', 'dy7329', '8f2d875cf8c41c81548dacb37497c7cf', 'maihoa', 3, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-20 23:26:38', '2019-10-24 02:12:06'),
(87, '8f2d875cf8c41c81548dacb37497c7cf', 'maihoa', '48956f2889a02a6ce7b8fbda2f9b7de7', 'wenzhing', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-20 23:34:57', '2019-10-24 02:13:43'),
(88, '8f2d875cf8c41c81548dacb37497c7cf', 'maihoa', '1a768058fc1484d390e842842ed0a2fe', 'Yibhin', 4, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-20 23:44:15', '2019-10-24 02:13:45'),
(89, '8f2d875cf8c41c81548dacb37497c7cf', 'maihoa', '887ee291da411b05c8b8ef7c760ec19b', 'jackychen', 4, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-10-20 23:54:11', '2019-10-24 02:13:47'),
(90, '887ee291da411b05c8b8ef7c760ec19b', 'jackychen', 'ac94e0e345ff3974f98e89820a36a101', 'kiewkiew', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-21 00:04:01', '2019-10-24 02:14:14'),
(91, '8ece43cdc970996fbdca121a9194815a', 'Leo', '95bcdb2d0ee44e646cba4b5e9e27472c', 'Albert', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-21 00:21:24', '2019-10-24 02:36:23'),
(92, '887ee291da411b05c8b8ef7c760ec19b', 'jackychen', '722405502b37aeeb2c2bcfcf63f3dbe7', 'Peterchen', 5, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-21 00:41:22', '2019-10-24 02:14:15'),
(93, '8ca2402c6c3f7643a93af5eeea795cdd', 'Josieloh', 'de549b1d8c5fa5aa6e619f8a65f5e710', 'meifang', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-21 03:25:03', '2019-10-24 02:31:32'),
(94, '8ca2402c6c3f7643a93af5eeea795cdd', 'Josieloh', 'db234ab854958eefcb2f1cfdbf064725', 'vincentpeh', 5, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-21 03:33:43', '2019-10-24 02:31:35'),
(95, '8ca2402c6c3f7643a93af5eeea795cdd', 'Josieloh', 'e7ffbcf4843ef7f63be105adcaa91e43', 'hebaodan123', 5, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-10-21 04:01:57', '2019-10-24 04:09:37'),
(100, 'a7a9de836484b55e3bb06d568a722400', 'DCK5', '922d5d256639f7520ba01af3488a3e90', 'Girliechua', 2, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-21 05:13:13', '2019-10-24 02:37:26'),
(101, '922d5d256639f7520ba01af3488a3e90', 'Girliechua', '847c50938aaedc09b473f57ed6068075', 'Tommygoh', 3, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-21 06:50:42', '2019-10-24 02:09:20'),
(116, '7673d904296328fe996510a9a7c504b0', 'Jwong', '259e4f29527b5f97361b9431416626cd', 'Ongkc77', 3, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-23 19:47:10', '2019-10-24 06:45:35'),
(117, '7673d904296328fe996510a9a7c504b0', 'Jwong', '6afc28fde2616be86a619428e274de7f', 'Dankor', 3, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-23 20:02:48', '2019-10-24 06:45:38'),
(119, '7673d904296328fe996510a9a7c504b0', 'Jwong', '149815e890aa7825c56847835b76e1af', 'Limto', 3, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-10-23 20:08:28', '2019-10-24 06:45:44'),
(120, '7673d904296328fe996510a9a7c504b0', 'Jwong', 'ad2e06a7aa2b073ec0121dfe50fad30b', 'Evianlim', 3, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-10-23 20:19:09', '2019-10-24 06:45:47'),
(121, '7673d904296328fe996510a9a7c504b0', 'Jwong', '01a960f79d75935b58bac0178d6bfb98', 'Lida', 3, '78ae38b91ca93c0f76b1a5487376d02d', 5, 50, '2019-10-23 20:28:35', '2019-10-24 06:45:49'),
(122, '7673d904296328fe996510a9a7c504b0', 'Jwong', 'c4eda98f92ee6a1129ca16d1a80aef3f', 'Jun96', 3, '78ae38b91ca93c0f76b1a5487376d02d', 6, 50, '2019-10-23 21:13:02', '2019-10-24 06:45:51'),
(123, '922d5d256639f7520ba01af3488a3e90', 'Girliechua', 'aec0b0afe43de960cc4db7b8c38cc4bf', '1121188', 3, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-23 22:12:16', '2019-10-24 06:47:54'),
(124, 'aec0b0afe43de960cc4db7b8c38cc4bf', '1121188', 'b54ce303811b0a3ca98f21e95943f385', '10004292', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-23 22:21:39', '2019-10-24 06:48:30'),
(165, '8f2d875cf8c41c81548dacb37497c7cf', 'maihoa', 'f5b67cde35a74ecb0646b7ef4f90cdfa', 'Quak', 4, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-10-23 22:41:07', '2019-10-24 06:49:05'),
(170, '922d5d256639f7520ba01af3488a3e90', 'Girliechua', '8fa12e6b81af70b499f5f1e299087191', 'Fabian', 3, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-10-24 08:49:28', '2019-10-24 08:49:28'),
(171, 'c075645287643d2d5a0a590cbabe84bb', 'cschang', 'd42f85e1d6095ab0dddd68d48ffe6035', 'skng', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-24 08:59:05', '2019-10-24 08:59:05'),
(172, 'a7a9de836484b55e3bb06d568a722400', 'DCK5', '308bd5a36bcb5e03d63a1d52915b67b0', 'Cynthia', 2, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-24 09:14:44', '2019-10-24 09:14:44'),
(173, '308bd5a36bcb5e03d63a1d52915b67b0', 'Cynthia', 'd68160bb964940925c065915952f9004', 'Leena', 3, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-24 09:33:36', '2019-10-24 09:33:36'),
(174, 'd68160bb964940925c065915952f9004', 'Leena', 'ec9abee186248eab79c4e257a1a8c4c8', 'Abi', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-24 09:44:15', '2019-10-24 09:44:15'),
(175, 'd68160bb964940925c065915952f9004', 'Leena', 'fa06dd8c1aaeb0e644dfdf6ab9538cac', 'Jalil', 4, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-24 09:47:23', '2019-10-24 09:47:23'),
(176, '38eada4b5b84b758002df7d74dc4722e', 'Erickooi', '545ab794670fb8c88f22aefcd1a64526', 'yytay99', 4, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-24 11:52:14', '2019-10-24 11:52:14'),
(177, '308bd5a36bcb5e03d63a1d52915b67b0', 'Cynthia', '22dfebc6edf3014ee5f6b378fcc3e76f', 'Hareey', 3, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-24 15:53:49', '2019-10-24 15:53:49'),
(178, 'f27cdb430d061807a0e4f4993d7af9df', 'DCK6', '166dcd604b821b4a496ca901f6ff6310', 'ThePinnacle', 2, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-24 16:11:30', '2019-10-24 16:11:30'),
(179, '166dcd604b821b4a496ca901f6ff6310', 'ThePinnacle', 'a55ebdd5812ff91eba1bffd86089834e', 'ksaras2525', 3, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-25 05:37:11', '2019-10-25 05:37:11'),
(180, 'a55ebdd5812ff91eba1bffd86089834e', 'ksaras2525', '211a598c64df7b0149eed4a065dd9a11', 'Giri25', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-25 05:49:16', '2019-10-25 05:49:16'),
(182, 'a55ebdd5812ff91eba1bffd86089834e', 'ksaras2525', '67b9a6d50b962624617fb04054937d3e', 'shereenlim1', 4, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-25 11:25:19', '2019-10-25 11:25:19'),
(183, 'a55ebdd5812ff91eba1bffd86089834e', 'ksaras2525', '832d825bd80e28135e9a93580591149a', 'Jagan4313', 4, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-10-25 11:29:22', '2019-10-25 11:29:22'),
(184, 'a55ebdd5812ff91eba1bffd86089834e', 'ksaras2525', '3113539eeb804ee3e4240410d2569700', 'kavimohan', 4, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-10-25 11:34:14', '2019-10-25 11:34:14'),
(185, 'a55ebdd5812ff91eba1bffd86089834e', 'ksaras2525', '3f9e1ae442f49cf7fc9c80584834fd12', 'Dolly', 4, '78ae38b91ca93c0f76b1a5487376d02d', 5, 50, '2019-10-25 11:36:45', '2019-10-25 11:36:45'),
(186, 'a55ebdd5812ff91eba1bffd86089834e', 'ksaras2525', '91e134c76c2134f4ee387275bfe17605', 'FP', 4, '78ae38b91ca93c0f76b1a5487376d02d', 6, 50, '2019-10-25 11:42:42', '2019-10-25 11:42:42'),
(187, '67b9a6d50b962624617fb04054937d3e', 'shereenlim1', 'b8db4691eb744b346aaa318375ffd9b3', 'adrianisw', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-25 13:27:53', '2019-10-25 13:27:53'),
(188, 'b8db4691eb744b346aaa318375ffd9b3', 'adrianlsw', 'da34e98545ad21d7adf4e9b942e1cdee', 'Victorz', 6, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-25 13:42:42', '2019-10-25 13:42:42'),
(189, '922d5d256639f7520ba01af3488a3e90', 'Girliechua', '696c265f58499da739b52e5660035fa1', 'Jackyng', 3, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-10-25 14:39:08', '2019-10-25 14:39:08'),
(190, '696c265f58499da739b52e5660035fa1', 'Jackyng', '6bd848a66b4fdf0ad68042aac36d9c93', 'chengkee', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-25 15:33:29', '2019-10-25 15:33:29'),
(191, 'aec0b0afe43de960cc4db7b8c38cc4bf', '1121188', '6a38de7d5332758f156cdad51f16ef67', 'wawasan', 4, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-27 13:51:58', '2019-10-27 13:51:58'),
(192, '308bd5a36bcb5e03d63a1d52915b67b0', 'Cynthia', 'e0ba42f66f1e63ad790906447b5df998', 'skliew9', 3, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-10-27 13:58:38', '2019-10-27 13:58:38'),
(193, '2402a716d533bd79d3d764be9c1d645a', 'Windlim', 'e5a4922a94ef5db728b20f1d2fc0c011', 'Lee', 3, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-10-28 10:56:13', '2019-10-28 10:56:13'),
(196, '847c50938aaedc09b473f57ed6068075', 'Tommygoh', 'a53112e56048b34a46060f212a8008d8', 'kccheah', 4, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-29 10:36:28', '2019-10-29 10:36:28'),
(197, 'a53112e56048b34a46060f212a8008d8', 'kccheah', 'ca94f2b4e70886f87fa5a601c38ea66f', 'Haisong', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-10-29 10:46:07', '2019-10-29 10:46:07'),
(198, '847c50938aaedc09b473f57ed6068075', 'Tommygoh', '30f3a507f500c23ac7437d8173880b73', 'Vanessa', 4, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-10-29 15:16:30', '2019-10-29 15:16:30'),
(201, '2402a716d533bd79d3d764be9c1d645a', 'Windlim', '495cbfbf29106f35140826824978a5aa', 'Mackensontan', 3, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-10-30 06:49:32', '2019-10-30 06:49:32'),
(203, '211a598c64df7b0149eed4a065dd9a11', 'Giri25', 'ca13c78fe860db1066698e82bd2a92cf', 'Praveen', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-11-04 14:26:26', '2019-11-04 14:26:26'),
(204, '211a598c64df7b0149eed4a065dd9a11', 'Giri25', '84f2e1c258036f9f6cdfa371320f15a8', 'Aina52', 5, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-11-04 14:29:21', '2019-11-04 14:29:21'),
(205, '211a598c64df7b0149eed4a065dd9a11', 'Giri25', 'f178369e2183b1d97704df861eced6f7', 'Spike07', 5, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-11-04 14:32:09', '2019-11-04 14:32:09'),
(206, '211a598c64df7b0149eed4a065dd9a11', 'Giri25', '61ab80fc5dcd02fbc7f6525c00f24b04', 'Reds233', 5, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-11-04 14:34:44', '2019-11-04 14:34:44'),
(207, '211a598c64df7b0149eed4a065dd9a11', 'Giri25', 'c4bf057e62874f1d0ac08a976fbd89f8', 'Ozzyben', 5, '78ae38b91ca93c0f76b1a5487376d02d', 5, 50, '2019-11-04 14:37:00', '2019-11-04 14:37:00'),
(208, 'f178369e2183b1d97704df861eced6f7', 'Spike07', '1a74603a1bf7b6d800693f1d17c1caa9', 'Kienkien21', 6, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-11-04 15:49:32', '2019-11-04 15:49:32'),
(209, '91e134c76c2134f4ee387275bfe17605', 'FP', '6451b38b2f3b7b6f54eaf0eed28bbaca', 'Dawn', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-11-04 17:01:34', '2019-11-04 17:01:34'),
(210, '832d825bd80e28135e9a93580591149a', 'Jagan4313', '2d30ddd67371f37586a419adf3f53716', 'Premashan', 5, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-11-05 03:04:29', '2019-11-05 03:04:29'),
(211, '832d825bd80e28135e9a93580591149a', 'Jagan4313', 'c067bb46a0986d11d4f4541f62adbd90', 'Kumaran', 5, '78ae38b91ca93c0f76b1a5487376d02d', 2, 150, '2019-11-05 03:14:29', '2019-11-05 03:14:29'),
(212, '832d825bd80e28135e9a93580591149a', 'Jagan4313', 'c3aeddda183272740d139fea3c7f38c6', 'Thialan', 5, '78ae38b91ca93c0f76b1a5487376d02d', 3, 50, '2019-11-05 03:19:13', '2019-11-05 03:19:13'),
(213, '2d30ddd67371f37586a419adf3f53716', 'Premashan', '46f4608805d4cc241b114971cb432484', 'Gopal', 6, '78ae38b91ca93c0f76b1a5487376d02d', 1, 150, '2019-11-05 03:48:28', '2019-11-05 03:48:28'),
(216, '8ca2402c6c3f7643a93af5eeea795cdd', 'Josieloh', '24e54274fe0f3991573ad54eaab32737', 'Appleooi', 5, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-11-05 14:15:01', '2019-11-05 14:15:01'),
(217, 'ca13c78fe860db1066698e82bd2a92cf', 'Praveen', '08c9889ec8f4bb33568861e8b09d245c', 'Rosel', 6, '78ae38b91ca93c0f76b1a5487376d02d', 7, 50, '2019-11-06 12:51:16', '2019-11-06 12:51:16'),
(218, 'e5a4922a94ef5db728b20f1d2fc0c011', 'Lee', '21004884ae70517ad3f30ea5f9a5537c', 'Khlim', 4, '78ae38b91ca93c0f76b1a5487376d02d', 8, 50, '2019-11-07 04:58:09', '2019-11-07 04:58:09'),
(219, '21004884ae70517ad3f30ea5f9a5537c', 'Khlim', '9f84e5ed3bd30d6a1cde40ab2cca7f30', 'Cam', 5, '78ae38b91ca93c0f76b1a5487376d02d', 9, 50, '2019-11-07 05:00:00', '2019-11-07 05:00:00'),
(220, '308bd5a36bcb5e03d63a1d52915b67b0', 'Cynthia', '8db2bccf185d95140389ea22d7a25a7d', 'Chyrie', 3, '78ae38b91ca93c0f76b1a5487376d02d', 10, 50, '2019-11-07 05:08:43', '2019-11-07 05:08:43'),
(221, '2d30ddd67371f37586a419adf3f53716', 'Premashan', '933d96d810218e875730b90af74b016d', 'Chitra', 6, '78ae38b91ca93c0f76b1a5487376d02d', 11, 50, '2019-11-07 07:44:30', '2019-11-07 07:44:30'),
(222, '91e134c76c2134f4ee387275bfe17605', 'FP', '10870f70fd7398fb5e1b67183ac98164', 'Mb281', 5, '78ae38b91ca93c0f76b1a5487376d02d', 12, 50, '2019-11-07 09:25:44', '2019-11-07 09:25:44'),
(223, '83ccbb881e19774f5d7a085a6003e245', 'michaelwong', 'afd9f21cc7935e7c1a8b6ef1032d4272', 'super01', 1, '83ccbb881e19774f5d7a085a6003e245', 1, 150, '2019-11-07 09:41:06', '2019-11-07 09:41:06'),
(224, 'afd9f21cc7935e7c1a8b6ef1032d4272', 'super01', 'cc6807bf135b2fc61c4c20f545cf0ddc', 'teestt', 2, '83ccbb881e19774f5d7a085a6003e245', 1, 150, '2019-11-07 09:43:37', '2019-11-07 09:43:37'),
(225, '83ccbb881e19774f5d7a085a6003e245', 'michaelwong', '44e8b0034b3d59f5d981194bb90b6932', 'Xx', 1, '83ccbb881e19774f5d7a085a6003e245', 2, 150, '2019-11-07 09:47:41', '2019-11-07 09:47:41'),
(226, '83ccbb881e19774f5d7a085a6003e245', 'michaelwong', 'd118196bb40ba1b4d58201db356ff390', 'Zz', 1, '83ccbb881e19774f5d7a085a6003e245', 3, 50, '2019-11-07 09:48:15', '2019-11-07 09:48:15'),
(227, '308bd5a36bcb5e03d63a1d52915b67b0', 'Cynthia', '0b7705fc7026b8ed73dcfd6bf604ed90', 'Choong908', 3, '78ae38b91ca93c0f76b1a5487376d02d', 4, 50, '2019-11-07 10:38:11', '2019-11-07 10:38:11'),
(228, '922d5d256639f7520ba01af3488a3e90', 'Girliechua', '9de7598ef605823ce722f226c41ad5ec', 'Kshslim', 3, '78ae38b91ca93c0f76b1a5487376d02d', 5, 50, '2019-11-07 12:05:14', '2019-11-07 12:05:14');

-- --------------------------------------------------------

--
-- Table structure for table `cash_to_point`
--

CREATE TABLE `cash_to_point` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `point` int(255) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cash_to_point`
--

INSERT INTO `cash_to_point` (`id`, `uid`, `name`, `point`, `date_create`, `status`) VALUES
(4, '8d335b3a10125e39ce46d611d88b2b4a', 'DCK1', 150, '2019-11-01 11:04:53', 'COMPLETED'),
(5, '83ccbb881e19774f5d7a085a6003e245', 'michaelwong', 300, '2019-11-04 14:08:33', 'COMPLETED'),
(6, '83ccbb881e19774f5d7a085a6003e245', 'michaelwong', 300, '2019-11-05 10:17:32', 'COMPLETED'),
(7, '83ccbb881e19774f5d7a085a6003e245', 'michaelwong', 300, '2019-11-05 10:19:36', 'COMPLETED'),
(8, '83ccbb881e19774f5d7a085a6003e245', 'michaelwong', 300, '2019-11-05 10:24:29', 'COMPLETED');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`) VALUES
(1, 'AF', 'Afghanistan', 93),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 672),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'US', 'United States', 1),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `group_commission`
--

CREATE TABLE `group_commission` (
  `id` int(255) NOT NULL,
  `commission` decimal(30,2) NOT NULL,
  `level` int(3) DEFAULT NULL,
  `count` int(3) DEFAULT NULL COMMENT 'This is for number of referrals that the user has referred and has purchased a product',
  `description` varchar(5000) DEFAULT NULL,
  `transaction_type_id` int(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_commission`
--

INSERT INTO `group_commission` (`id`, `commission`, `level`, `count`, `description`, `transaction_type_id`, `date_created`, `date_updated`) VALUES
(1, 5000.00, 1, NULL, 'in percent', 2, '2019-07-25 09:17:37', '2019-08-26 04:05:22'),
(2, 4000.00, 2, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-26 04:05:25'),
(3, 3000.00, 3, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-26 04:05:28'),
(4, 6.00, 4, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:17'),
(5, 4.00, 5, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:19'),
(6, 4.00, 6, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:20'),
(7, 2.00, 7, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:31'),
(8, 2.00, 8, NULL, 'in percent', 2, '2019-07-25 09:18:42', '2019-08-08 06:07:33'),
(9, 1.00, 9, NULL, 'in percent', 2, '2019-07-25 09:18:43', '2019-07-25 09:25:36'),
(10, 5000.00, NULL, 1, 'in points (RM1 = 100).first referral.', 1, '2019-07-25 09:50:48', '2019-07-25 09:50:58'),
(11, 10000.00, NULL, 2, 'in points (RM1 = 100).second referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:36'),
(12, 15000.00, NULL, 3, 'in points (RM1 = 100).third referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:39'),
(13, 5000.00, NULL, 0, 'in points (RM1 = 100).subsequent referral.', 1, '2019-07-25 09:53:00', '2019-07-25 09:53:43'),
(14, 1.00, 10, NULL, 'in percent', 2, '2019-08-08 06:06:46', '2019-08-08 06:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `pid` int(255) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`pid`, `filename`, `uploaded`, `status`) VALUES
(11, 'DCK1571297216.jpg', '2019-10-17 07:26:56', '1'),
(12, 'admin1571371772.jpg', '2019-10-18 04:09:32', '1'),
(13, 'michaelwong1571719252.jpg', '2019-10-22 04:40:52', '1'),
(14, 'michaelwong1571719283.jpg', '2019-10-22 04:41:23', '1'),
(15, 'michaelwong1571720110.jpg', '2019-10-22 04:55:10', '1'),
(16, 'DCK1571720170.jpg', '2019-10-22 04:56:10', '1'),
(17, 'Super061571728271.jpg', '2019-10-22 07:11:11', '1'),
(18, 'DCK1571987060.jpg', '2019-10-25 07:04:20', '1');

-- --------------------------------------------------------

--
-- Table structure for table `money_type`
--

CREATE TABLE `money_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `money_type`
--

INSERT INTO `money_type` (`id`, `name`, `description`, `date_created`, `date_updated`) VALUES
(1, 'user_real', 'this is user\'s real money. can withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00'),
(2, 'voucher', 'this money is gotten from certain commissions. cannot withdraw', '2019-07-25 06:09:00', '2019-07-25 06:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(200) DEFAULT NULL COMMENT 'account that login by user to make order',
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` int(255) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL COMMENT 'person to receive the product for delivery',
  `contactNo` varchar(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `address_line_1` varchar(2500) DEFAULT NULL,
  `address_line_2` varchar(2500) DEFAULT NULL,
  `address_line_3` varchar(2500) DEFAULT NULL,
  `city` varchar(500) DEFAULT NULL,
  `zipcode` varchar(100) DEFAULT NULL,
  `state` varchar(500) DEFAULT NULL,
  `country` varchar(500) DEFAULT NULL,
  `subtotal` decimal(50,0) DEFAULT NULL,
  `total` decimal(50,0) DEFAULT NULL COMMENT 'include postage fees',
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_amount` int(255) DEFAULT NULL,
  `payment_bankreference` varchar(255) DEFAULT NULL,
  `payment_date` varchar(20) DEFAULT NULL,
  `payment_time` varchar(20) DEFAULT NULL,
  `payment_status` varchar(200) DEFAULT NULL COMMENT 'PENDING, ACCEPTED, REJECT, NULL = nothing',
  `shipping_status` varchar(200) DEFAULT NULL COMMENT 'PENDING, SHIPPED, REJECT AND REFUND',
  `shipping_method` varchar(200) DEFAULT NULL,
  `shipping_date` varchar(20) DEFAULT NULL,
  `tracking_number` varchar(200) DEFAULT NULL,
  `reject_reason` varchar(255) DEFAULT NULL,
  `refund_method` varchar(255) DEFAULT NULL,
  `refund_amount` int(255) DEFAULT NULL,
  `refund_note` varchar(255) DEFAULT NULL,
  `refund_reason` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(5000) NOT NULL,
  `price` decimal(50,0) NOT NULL COMMENT 'With MYR as base and is in point system (RM1 = 100 point)',
  `stock` int(255) DEFAULT NULL,
  `buy_stock` int(255) NOT NULL DEFAULT '0',
  `total_price` int(255) NOT NULL DEFAULT '0',
  `display` int(20) DEFAULT '1',
  `type` int(255) NOT NULL DEFAULT '1' COMMENT '1 = normal product',
  `description` varchar(10000) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `stock`, `buy_stock`, `total_price`, `display`, `type`, `description`, `images`, `date_created`, `date_updated`) VALUES
(1, 'DCK Engine Oil Booster', 300, 1000, 0, 0, 1, 1, 'DCK ENGINE OIL BOOSTER', 'DCK-Engine-Oil-Booster.png', '2019-07-24 09:17:04', '2019-10-29 08:32:22'),
(2, 'DCK Synthetic Plus Motor Oil (SAE 5w – 30)', 300, 1000, 0, 0, 1, 1, 'DCK SYNTHETIC PLUS MOTOR OIL (SAE 5W – 30)', '5w.png', '2019-07-25 04:11:21', '2019-10-29 08:34:57'),
(3, 'DCK Synthetic Plus Motor Oil (SAE 10w – 40)', 300, 1000, 0, 0, 1, 1, 'DCK Synthetic Plus Motor Oil (SAE 10w – 40)', '10w.png', '2019-10-29 08:35:43', '2019-10-29 09:12:22');

-- --------------------------------------------------------

--
-- Table structure for table `product_orders`
--

CREATE TABLE `product_orders` (
  `id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `quantity` int(10) NOT NULL,
  `final_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `original_price` decimal(50,0) NOT NULL COMMENT 'In points',
  `discount_given` decimal(50,0) NOT NULL COMMENT 'in points',
  `totalProductPrice` decimal(50,0) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `referral_bonus` int(255) DEFAULT NULL,
  `commission` int(255) DEFAULT NULL,
  `conversion_point` int(255) DEFAULT NULL,
  `charges_withdraw` int(255) DEFAULT NULL,
  `point_voucher` int(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate`
--

INSERT INTO `rate` (`id`, `uid`, `referral_bonus`, `commission`, `conversion_point`, `charges_withdraw`, `point_voucher`) VALUES
(1, '', 20, 20, 20, 10, 100);

-- --------------------------------------------------------

--
-- Table structure for table `referral_history`
--

CREATE TABLE `referral_history` (
  `id` bigint(20) NOT NULL,
  `referrer_id` varchar(255) NOT NULL COMMENT 'the uid of the person that intro this user',
  `referral_id` varchar(255) NOT NULL COMMENT 'the uid of the person that gets invited to join this platform',
  `referral_name` varchar(255) NOT NULL,
  `current_level` int(100) NOT NULL,
  `top_referrer_id` varchar(100) NOT NULL COMMENT 'the topmost person''s uid',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `referral_history`
--

INSERT INTO `referral_history` (`id`, `referrer_id`, `referral_id`, `referral_name`, `current_level`, `top_referrer_id`, `date_created`, `date_updated`) VALUES
(45, '78ae38b91ca93c0f76b1a5487376d02d', '8d335b3a10125e39ce46d611d88b2b4a', 'DCK1', 1, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-16 09:41:39', '2019-10-16 09:41:39'),
(46, '78ae38b91ca93c0f76b1a5487376d02d', '91fa223cd25fec9982d166cf551639d6', 'DCK2', 1, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-16 09:42:54', '2019-10-16 09:45:36'),
(47, '78ae38b91ca93c0f76b1a5487376d02d', 'f9b60b8d0eb3c59b2ab4b36e3ddbcd61', 'DCK3', 1, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-16 09:43:07', '2019-10-16 09:43:07'),
(48, '78ae38b91ca93c0f76b1a5487376d02d', 'f15107a4ec3d75eebc75e1b912dc8d63', 'DCK4', 1, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-16 09:43:16', '2019-10-16 09:43:16'),
(49, '78ae38b91ca93c0f76b1a5487376d02d', 'a7a9de836484b55e3bb06d568a722400', 'DCK5', 1, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-16 09:43:23', '2019-10-16 09:43:23'),
(50, '78ae38b91ca93c0f76b1a5487376d02d', 'f27cdb430d061807a0e4f4993d7af9df', 'DCK6', 1, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-16 09:43:31', '2019-10-16 09:43:31'),
(56, '8d335b3a10125e39ce46d611d88b2b4a', '86d0613407b61357a5bb1300832b86ff', 'Jacky181', 2, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-19 15:40:09', '2019-10-19 15:40:09'),
(57, '86d0613407b61357a5bb1300832b86ff', '7d85a3e759a8e44d79b00749e33d5391', 'wendy', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-19 15:49:54', '2019-10-19 15:49:54'),
(58, '86d0613407b61357a5bb1300832b86ff', '3631890bc9a862b5f7ceac83c4f5d1e8', 'Ectan', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-19 16:02:21', '2019-10-19 16:02:21'),
(59, '86d0613407b61357a5bb1300832b86ff', '53fca61f0dff423502d344054b799ba2', 'Chia2123', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-19 16:12:16', '2019-10-19 16:12:16'),
(60, '86d0613407b61357a5bb1300832b86ff', '0c396af354a0bda95e6114a77ecc4cfa', 'shuennchi2020', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-19 16:22:53', '2019-10-19 16:22:53'),
(61, '91fa223cd25fec9982d166cf551639d6', '7673d904296328fe996510a9a7c504b0', 'Jwong', 2, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-19 16:37:59', '2019-10-19 16:37:59'),
(62, 'f9b60b8d0eb3c59b2ab4b36e3ddbcd61', '2402a716d533bd79d3d764be9c1d645a', 'Windlim', 2, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-19 17:04:02', '2019-10-19 17:04:02'),
(63, '2402a716d533bd79d3d764be9c1d645a', 'a30ac6b232b13c4b187ec873ae16f1a1', 'Teik', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-19 17:10:46', '2019-10-19 17:10:46'),
(64, '2402a716d533bd79d3d764be9c1d645a', '38eada4b5b84b758002df7d74dc4722e', 'Erickooi', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-19 17:17:43', '2019-10-19 17:17:43'),
(65, 'f15107a4ec3d75eebc75e1b912dc8d63', '39b50f80956cd29ebf4f975c0b579c53', 'dy7329', 2, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 06:47:59', '2019-10-20 06:47:59'),
(66, '39b50f80956cd29ebf4f975c0b579c53', '197c08e85e66915dc9aeca5759cce4ec', 'cchow', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 06:55:23', '2019-10-20 06:55:23'),
(67, '197c08e85e66915dc9aeca5759cce4ec', 'c075645287643d2d5a0a590cbabe84bb', 'cschang', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 07:01:56', '2019-10-20 07:01:56'),
(68, '197c08e85e66915dc9aeca5759cce4ec', 'e516b51931fb08b4612433fb51005c56', 'meiyong', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 07:07:11', '2019-10-20 07:07:11'),
(69, '197c08e85e66915dc9aeca5759cce4ec', '773a0ae6095f0ba243ec16c3ea818ff6', 'Layyean', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 07:13:17', '2019-10-20 07:13:17'),
(70, '197c08e85e66915dc9aeca5759cce4ec', '415c193d1efa340d3a5ddf2455efa7e2', 'Kimleang', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 07:19:23', '2019-10-20 07:19:23'),
(71, '197c08e85e66915dc9aeca5759cce4ec', '876fd5c039eb5a7006d65f550941a81e', 'suiyuan', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 07:23:48', '2019-10-20 07:23:48'),
(72, '197c08e85e66915dc9aeca5759cce4ec', '91ddb6bea21b3218bd7c52c8a13e4cbc', 'galaxykc', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 07:29:49', '2019-10-20 07:29:49'),
(73, '38eada4b5b84b758002df7d74dc4722e', 'b535adb1e7e856a84134d9cbdb969e8a', 'Pinqin0426', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 08:46:28', '2019-10-20 08:46:28'),
(74, '86d0613407b61357a5bb1300832b86ff', '3f6145490f75b943f067e2506cea0454', 'Fattchai', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 15:28:19', '2019-10-20 15:28:19'),
(75, '86d0613407b61357a5bb1300832b86ff', '430a4456ee40d1707c8a44cceb0c9f65', 'ilifestyles', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 15:30:34', '2019-10-20 15:30:34'),
(76, '3631890bc9a862b5f7ceac83c4f5d1e8', 'e57eba868ce93e56f3f31196f0e8caa0', 'Anson', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 15:49:37', '2019-10-20 15:49:37'),
(77, '53fca61f0dff423502d344054b799ba2', 'a9e242dc63ba85f2181e621b9085262b', 'chiayiting91', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 15:57:45', '2019-10-20 15:57:45'),
(78, '0c396af354a0bda95e6114a77ecc4cfa', '8ca2402c6c3f7643a93af5eeea795cdd', 'Josieloh', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 16:09:38', '2019-10-20 16:09:38'),
(79, '0c396af354a0bda95e6114a77ecc4cfa', '1b33037d3f15da3ebffe7511defe6c47', 'laypheng', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 16:19:50', '2019-10-20 16:19:50'),
(80, '0c396af354a0bda95e6114a77ecc4cfa', 'e7f97247d154c2344218034b94b466c1', 'ktchew', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 16:26:08', '2019-10-20 16:26:08'),
(81, '0c396af354a0bda95e6114a77ecc4cfa', 'bb97e777595704a90693bc6a0b0a7b92', 'Mike', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 16:32:55', '2019-10-20 16:32:55'),
(82, '0c396af354a0bda95e6114a77ecc4cfa', '8ece43cdc970996fbdca121a9194815a', 'Leo', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 16:42:02', '2019-10-20 16:42:02'),
(83, '0c396af354a0bda95e6114a77ecc4cfa', '54baf180ba2c6838a5aa0545d0ad1734', 'st4589', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 16:50:37', '2019-10-20 16:50:37'),
(84, 'bb97e777595704a90693bc6a0b0a7b92', '47b4c06089ca56a99379a89e99e57dd1', 'Joelktan', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 17:07:22', '2019-10-20 17:07:22'),
(85, '1b33037d3f15da3ebffe7511defe6c47', '593e0d4cf3db550d08da2a90cff39ed5', 'shunyun', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-20 17:15:31', '2019-10-20 17:15:31'),
(86, '39b50f80956cd29ebf4f975c0b579c53', '8f2d875cf8c41c81548dacb37497c7cf', 'maihoa', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 07:26:38', '2019-10-21 07:26:38'),
(87, '8f2d875cf8c41c81548dacb37497c7cf', '48956f2889a02a6ce7b8fbda2f9b7de7', 'wenzhing', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 07:34:57', '2019-10-21 07:34:57'),
(88, '8f2d875cf8c41c81548dacb37497c7cf', '1a768058fc1484d390e842842ed0a2fe', 'Yibhin', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 07:44:15', '2019-10-21 07:44:15'),
(89, '8f2d875cf8c41c81548dacb37497c7cf', '887ee291da411b05c8b8ef7c760ec19b', 'jackychen', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 07:54:11', '2019-10-21 07:54:11'),
(90, '887ee291da411b05c8b8ef7c760ec19b', 'ac94e0e345ff3974f98e89820a36a101', 'kiewkiew', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 08:04:01', '2019-10-21 08:04:01'),
(91, '8ece43cdc970996fbdca121a9194815a', '95bcdb2d0ee44e646cba4b5e9e27472c', 'Albert', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 08:21:24', '2019-10-21 08:21:24'),
(92, '887ee291da411b05c8b8ef7c760ec19b', '722405502b37aeeb2c2bcfcf63f3dbe7', 'Peterchen', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 08:41:22', '2019-10-21 08:41:22'),
(93, '8ca2402c6c3f7643a93af5eeea795cdd', 'de549b1d8c5fa5aa6e619f8a65f5e710', 'meifang', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 11:25:03', '2019-10-21 11:25:03'),
(94, '8ca2402c6c3f7643a93af5eeea795cdd', 'db234ab854958eefcb2f1cfdbf064725', 'vincentpeh', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 11:33:43', '2019-10-21 11:33:43'),
(95, '8ca2402c6c3f7643a93af5eeea795cdd', 'e7ffbcf4843ef7f63be105adcaa91e43', 'hebaodan123', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 12:01:57', '2019-10-21 12:01:57'),
(100, 'a7a9de836484b55e3bb06d568a722400', '922d5d256639f7520ba01af3488a3e90', 'Girliechua', 2, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 13:13:13', '2019-10-21 13:13:13'),
(101, '922d5d256639f7520ba01af3488a3e90', '847c50938aaedc09b473f57ed6068075', 'Tommygoh', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-21 14:50:42', '2019-10-21 14:50:42'),
(116, '7673d904296328fe996510a9a7c504b0', '259e4f29527b5f97361b9431416626cd', 'Ongkc77', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 03:47:10', '2019-10-24 03:47:10'),
(117, '7673d904296328fe996510a9a7c504b0', '6afc28fde2616be86a619428e274de7f', 'Dankor', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 04:02:48', '2019-10-24 04:02:48'),
(119, '7673d904296328fe996510a9a7c504b0', '149815e890aa7825c56847835b76e1af', 'Limto', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 04:08:28', '2019-10-24 04:08:28'),
(120, '7673d904296328fe996510a9a7c504b0', 'ad2e06a7aa2b073ec0121dfe50fad30b', 'Evianlim', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 04:19:09', '2019-10-24 04:19:09'),
(121, '7673d904296328fe996510a9a7c504b0', '01a960f79d75935b58bac0178d6bfb98', 'Lida', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 04:28:35', '2019-10-24 04:28:35'),
(122, '7673d904296328fe996510a9a7c504b0', 'c4eda98f92ee6a1129ca16d1a80aef3f', 'Jun96', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 05:13:02', '2019-10-24 05:13:02'),
(123, '922d5d256639f7520ba01af3488a3e90', 'aec0b0afe43de960cc4db7b8c38cc4bf', '1121188', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 06:12:16', '2019-10-24 06:12:16'),
(124, 'aec0b0afe43de960cc4db7b8c38cc4bf', 'b54ce303811b0a3ca98f21e95943f385', '10004292', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 06:21:39', '2019-10-24 06:21:39'),
(165, '8f2d875cf8c41c81548dacb37497c7cf', 'f5b67cde35a74ecb0646b7ef4f90cdfa', 'Quak', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 06:41:07', '2019-10-24 06:41:07'),
(170, '922d5d256639f7520ba01af3488a3e90', '8fa12e6b81af70b499f5f1e299087191', 'Fabian', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 08:49:28', '2019-10-24 08:49:28'),
(171, 'c075645287643d2d5a0a590cbabe84bb', 'd42f85e1d6095ab0dddd68d48ffe6035', 'skng', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 08:59:05', '2019-10-24 08:59:05'),
(172, 'a7a9de836484b55e3bb06d568a722400', '308bd5a36bcb5e03d63a1d52915b67b0', 'Cynthia', 2, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 09:14:44', '2019-10-24 09:14:44'),
(173, '308bd5a36bcb5e03d63a1d52915b67b0', 'd68160bb964940925c065915952f9004', 'Leena', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 09:33:36', '2019-10-24 09:33:36'),
(174, 'd68160bb964940925c065915952f9004', 'ec9abee186248eab79c4e257a1a8c4c8', 'Abi', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 09:44:15', '2019-10-24 09:44:15'),
(175, 'd68160bb964940925c065915952f9004', 'fa06dd8c1aaeb0e644dfdf6ab9538cac', 'Jalil', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 09:47:23', '2019-10-24 09:47:23'),
(176, '38eada4b5b84b758002df7d74dc4722e', '545ab794670fb8c88f22aefcd1a64526', 'yytay99', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 11:52:14', '2019-10-24 11:52:14'),
(177, '308bd5a36bcb5e03d63a1d52915b67b0', '22dfebc6edf3014ee5f6b378fcc3e76f', 'Hareey', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 15:53:49', '2019-10-24 15:53:49'),
(178, 'f27cdb430d061807a0e4f4993d7af9df', '166dcd604b821b4a496ca901f6ff6310', 'ThePinnacle', 2, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-24 16:11:30', '2019-10-24 16:11:30'),
(179, '166dcd604b821b4a496ca901f6ff6310', 'a55ebdd5812ff91eba1bffd86089834e', 'ksaras2525', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 05:37:11', '2019-10-25 05:37:11'),
(180, 'a55ebdd5812ff91eba1bffd86089834e', '211a598c64df7b0149eed4a065dd9a11', 'Giri25', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 05:49:16', '2019-10-25 05:49:16'),
(181, '8ca2402c6c3f7643a93af5eeea795cdd', '973c3b0dbaffaab3dde7402566e2dfd6', 'Allinto1', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 08:34:44', '2019-10-25 08:34:44'),
(182, 'a55ebdd5812ff91eba1bffd86089834e', '67b9a6d50b962624617fb04054937d3e', 'shereenlim1', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 11:25:19', '2019-10-25 11:25:19'),
(183, 'a55ebdd5812ff91eba1bffd86089834e', '832d825bd80e28135e9a93580591149a', 'Jagan4313', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 11:29:22', '2019-10-25 11:29:22'),
(184, 'a55ebdd5812ff91eba1bffd86089834e', '3113539eeb804ee3e4240410d2569700', 'kavimohan', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 11:34:14', '2019-10-25 11:34:14'),
(185, 'a55ebdd5812ff91eba1bffd86089834e', '3f9e1ae442f49cf7fc9c80584834fd12', 'Dolly', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 11:36:45', '2019-10-25 11:36:45'),
(186, 'a55ebdd5812ff91eba1bffd86089834e', '91e134c76c2134f4ee387275bfe17605', 'FP', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 11:42:42', '2019-10-25 11:42:42'),
(187, '67b9a6d50b962624617fb04054937d3e', 'b8db4691eb744b346aaa318375ffd9b3', 'adrianisw', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 13:27:53', '2019-10-25 13:27:53'),
(188, 'b8db4691eb744b346aaa318375ffd9b3', 'da34e98545ad21d7adf4e9b942e1cdee', 'Victorz', 6, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 13:42:42', '2019-10-25 13:42:42'),
(189, '922d5d256639f7520ba01af3488a3e90', '696c265f58499da739b52e5660035fa1', 'Jackyng', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 14:39:08', '2019-10-25 14:39:08'),
(190, '696c265f58499da739b52e5660035fa1', '6bd848a66b4fdf0ad68042aac36d9c93', 'chengkee', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-25 15:33:29', '2019-10-25 15:33:29'),
(191, 'aec0b0afe43de960cc4db7b8c38cc4bf', '6a38de7d5332758f156cdad51f16ef67', 'wawasan', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-27 13:51:58', '2019-10-27 13:51:58'),
(192, '308bd5a36bcb5e03d63a1d52915b67b0', 'e0ba42f66f1e63ad790906447b5df998', 'skliew9', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-27 13:58:38', '2019-10-27 13:58:38'),
(193, '2402a716d533bd79d3d764be9c1d645a', 'e5a4922a94ef5db728b20f1d2fc0c011', 'Lee', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-28 10:56:13', '2019-10-28 10:56:13'),
(196, '847c50938aaedc09b473f57ed6068075', 'a53112e56048b34a46060f212a8008d8', 'kccheah', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-29 10:36:28', '2019-10-29 10:36:28'),
(197, 'a53112e56048b34a46060f212a8008d8', 'ca94f2b4e70886f87fa5a601c38ea66f', 'Haisong', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-29 10:46:07', '2019-10-29 10:46:07'),
(198, '847c50938aaedc09b473f57ed6068075', '30f3a507f500c23ac7437d8173880b73', 'Vanessa', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-29 15:16:30', '2019-10-29 15:16:30'),
(201, '2402a716d533bd79d3d764be9c1d645a', '495cbfbf29106f35140826824978a5aa', 'Mackensontan', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-10-30 06:49:32', '2019-10-30 06:49:32'),
(203, '211a598c64df7b0149eed4a065dd9a11', 'ca13c78fe860db1066698e82bd2a92cf', 'Praveen', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-04 14:26:26', '2019-11-04 14:26:26'),
(204, '211a598c64df7b0149eed4a065dd9a11', '84f2e1c258036f9f6cdfa371320f15a8', 'Aina52', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-04 14:29:21', '2019-11-04 14:29:21'),
(205, '211a598c64df7b0149eed4a065dd9a11', 'f178369e2183b1d97704df861eced6f7', 'Spike07', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-04 14:32:09', '2019-11-04 14:32:09'),
(206, '211a598c64df7b0149eed4a065dd9a11', '61ab80fc5dcd02fbc7f6525c00f24b04', 'Reds233', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-04 14:34:44', '2019-11-04 14:34:44'),
(207, '211a598c64df7b0149eed4a065dd9a11', 'c4bf057e62874f1d0ac08a976fbd89f8', 'Ozzyben', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-04 14:37:00', '2019-11-04 14:37:00'),
(208, 'f178369e2183b1d97704df861eced6f7', '1a74603a1bf7b6d800693f1d17c1caa9', 'Kienkien21', 6, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-04 15:49:32', '2019-11-04 15:49:32'),
(209, '91e134c76c2134f4ee387275bfe17605', '6451b38b2f3b7b6f54eaf0eed28bbaca', 'Dawn', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-04 17:01:34', '2019-11-04 17:01:34'),
(210, '832d825bd80e28135e9a93580591149a', '2d30ddd67371f37586a419adf3f53716', 'Premashan', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-05 03:04:29', '2019-11-05 03:04:29'),
(211, '832d825bd80e28135e9a93580591149a', 'c067bb46a0986d11d4f4541f62adbd90', 'Kumaran', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-05 03:14:29', '2019-11-05 03:14:29'),
(212, '832d825bd80e28135e9a93580591149a', 'c3aeddda183272740d139fea3c7f38c6', 'Thialan', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-05 03:19:13', '2019-11-05 03:19:13'),
(213, '2d30ddd67371f37586a419adf3f53716', '46f4608805d4cc241b114971cb432484', 'Gopal', 6, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-05 03:48:28', '2019-11-05 03:48:28'),
(216, '8ca2402c6c3f7643a93af5eeea795cdd', '24e54274fe0f3991573ad54eaab32737', 'Appleooi', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-05 14:15:01', '2019-11-05 14:15:01'),
(217, 'ca13c78fe860db1066698e82bd2a92cf', '08c9889ec8f4bb33568861e8b09d245c', 'Rosel', 6, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-06 12:51:16', '2019-11-06 12:51:16'),
(218, 'e5a4922a94ef5db728b20f1d2fc0c011', '21004884ae70517ad3f30ea5f9a5537c', 'Khlim', 4, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-07 04:58:09', '2019-11-07 04:58:09'),
(219, '21004884ae70517ad3f30ea5f9a5537c', '9f84e5ed3bd30d6a1cde40ab2cca7f30', 'Cam', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-07 05:00:00', '2019-11-07 05:00:00'),
(220, '308bd5a36bcb5e03d63a1d52915b67b0', '8db2bccf185d95140389ea22d7a25a7d', 'Chyrie', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-07 05:08:43', '2019-11-07 05:08:43'),
(221, '2d30ddd67371f37586a419adf3f53716', '933d96d810218e875730b90af74b016d', 'Chitra', 6, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-07 07:44:30', '2019-11-07 07:44:30'),
(222, '91e134c76c2134f4ee387275bfe17605', '10870f70fd7398fb5e1b67183ac98164', 'Mb281', 5, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-07 09:25:44', '2019-11-07 09:25:44'),
(223, '83ccbb881e19774f5d7a085a6003e245', 'afd9f21cc7935e7c1a8b6ef1032d4272', 'super01', 1, '83ccbb881e19774f5d7a085a6003e245', '2019-11-07 09:41:06', '2019-11-07 09:41:06'),
(224, 'afd9f21cc7935e7c1a8b6ef1032d4272', 'cc6807bf135b2fc61c4c20f545cf0ddc', 'teestt', 2, '83ccbb881e19774f5d7a085a6003e245', '2019-11-07 09:43:37', '2019-11-07 09:43:37'),
(225, '83ccbb881e19774f5d7a085a6003e245', '44e8b0034b3d59f5d981194bb90b6932', 'Xx', 1, '83ccbb881e19774f5d7a085a6003e245', '2019-11-07 09:47:41', '2019-11-07 09:47:41'),
(226, '83ccbb881e19774f5d7a085a6003e245', 'd118196bb40ba1b4d58201db356ff390', 'Zz', 1, '83ccbb881e19774f5d7a085a6003e245', '2019-11-07 09:48:15', '2019-11-07 09:48:15'),
(227, '308bd5a36bcb5e03d63a1d52915b67b0', '0b7705fc7026b8ed73dcfd6bf604ed90', 'Choong908', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-07 10:38:11', '2019-11-07 10:38:11'),
(228, '922d5d256639f7520ba01af3488a3e90', '9de7598ef605823ce722f226c41ad5ec', 'Kshslim', 3, '78ae38b91ca93c0f76b1a5487376d02d', '2019-11-07 12:05:14', '2019-11-07 12:05:14');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE `transaction_history` (
  `id` bigint(20) NOT NULL,
  `money_in` decimal(50,0) NOT NULL DEFAULT '0' COMMENT 'in points',
  `money_out` decimal(50,0) NOT NULL DEFAULT '0' COMMENT 'in points',
  `uid` varchar(255) NOT NULL,
  `target_uid` varchar(255) DEFAULT NULL COMMENT 'this is the uid for the targeted user. it can be a downline''s uid because upline can get commissions from his downline. therefore this target_uid shall be downline''s uid while the uid field above will be upline''s uid',
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'The percentage assigned to this transaction. Usually for commissions use only',
  `original_value` decimal(50,0) DEFAULT NULL COMMENT '(in point)the original value that has been given before calculating the percentage. Usually for commissions use only',
  `status` int(3) DEFAULT NULL COMMENT '1 = pending, 2 = accepted/completed, 3 = rejected, NULL = nothing',
  `level` int(10) DEFAULT NULL COMMENT 'can be group sales commission given by a downline at a level',
  `order_id` bigint(20) DEFAULT NULL COMMENT 'refers to a particular order in order table and it refers to another table that lists out all the products and quantity purchased in that table with order_id as foreign key',
  `transaction_type_id` int(255) NOT NULL,
  `money_type_id` int(255) DEFAULT NULL,
  `source_transaction_id` bigint(20) DEFAULT NULL COMMENT 'this is referring to this table''s own id. Might be needed when some previous transaction is triggering new transactions',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction_type`
--

CREATE TABLE `transaction_type` (
  `id` int(255) NOT NULL,
  `name` varchar(2000) NOT NULL,
  `description` varchar(10000) DEFAULT NULL,
  `percentage` decimal(5,2) DEFAULT NULL COMMENT 'Percentage of the transaction, for example if transaction is withdrawal type, user need give 0.5% of total',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction_type`
--

INSERT INTO `transaction_type` (`id`, `name`, `description`, `percentage`, `date_created`, `date_updated`) VALUES
(1, 'referral', 'user can get commission by referring people and the referred person must at least buy a product for the referral to get this commission', NULL, '2019-07-25 08:46:07', '2019-07-25 08:46:07'),
(2, 'group_sales', 'This is when the user\'s downline bought a product, he will get a percentage from it. up to 9 levels with each level having different percentage. please refer to group_commission table', NULL, '2019-07-25 09:15:08', '2019-07-25 09:15:08'),
(3, 'buy_product', 'This is when user purchase a product', NULL, '2019-07-26 01:35:16', '2019-07-26 01:35:16'),
(4, 'transfer', 'point transfer between members', 0.25, '2019-07-31 02:46:22', '2019-07-31 02:46:22'),
(5, 'withdraw', 'withdraw money and transfer to bank', 0.50, '2019-07-31 02:46:22', '2019-07-31 02:46:22'),
(6, 'agent', 'this is for agent rewards, including district and master', NULL, '2019-08-26 02:22:19', '2019-08-26 02:22:19');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_point`
--

CREATE TABLE `transfer_point` (
  `id` int(255) NOT NULL,
  `send_uid` varchar(255) DEFAULT NULL,
  `send_name` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `receive_name` varchar(255) DEFAULT NULL,
  `receive_uid` varchar(255) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transfer_point`
--

INSERT INTO `transfer_point` (`id`, `send_uid`, `send_name`, `amount`, `receive_name`, `receive_uid`, `create_date`, `status`) VALUES
(5, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 1500, 'Jwong', '7673d904296328fe996510a9a7c504b0', '2019-10-24 03:41:47', 'RECEIVED'),
(6, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Jwong', '7673d904296328fe996510a9a7c504b0', '2019-10-24 05:10:19', 'RECEIVED'),
(7, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 600, 'Girliechua', '922d5d256639f7520ba01af3488a3e90', '2019-10-24 06:10:15', 'RECEIVED'),
(8, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, '1121188', 'aec0b0afe43de960cc4db7b8c38cc4bf', '2019-10-24 06:20:10', 'RECEIVED'),
(9, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'maihoa', '8f2d875cf8c41c81548dacb37497c7cf', '2019-10-24 06:38:57', 'RECEIVED'),
(10, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'cschang', 'c075645287643d2d5a0a590cbabe84bb', '2019-10-24 08:56:26', 'RECEIVED'),
(11, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'DCK5', 'a7a9de836484b55e3bb06d568a722400', '2019-10-24 09:12:33', 'RECEIVED'),
(12, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Cynthia', '308bd5a36bcb5e03d63a1d52915b67b0', '2019-10-24 09:31:31', 'RECEIVED'),
(13, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 600, 'Leena', 'd68160bb964940925c065915952f9004', '2019-10-24 09:40:08', 'RECEIVED'),
(14, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Erickooi', '38eada4b5b84b758002df7d74dc4722e', '2019-10-24 11:50:31', 'RECEIVED'),
(15, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Cynthia', '308bd5a36bcb5e03d63a1d52915b67b0', '2019-10-24 15:49:57', 'RECEIVED'),
(16, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'DCK6', 'f27cdb430d061807a0e4f4993d7af9df', '2019-10-24 16:05:14', 'RECEIVED'),
(17, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'ThePinnacle', '166dcd604b821b4a496ca901f6ff6310', '2019-10-25 05:34:57', 'RECEIVED'),
(18, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'ksaras2525', 'a55ebdd5812ff91eba1bffd86089834e', '2019-10-25 05:46:55', 'RECEIVED'),
(19, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 1200, 'ksaras2525', 'a55ebdd5812ff91eba1bffd86089834e', '2019-10-25 11:18:25', 'RECEIVED'),
(20, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'ksaras2525', 'a55ebdd5812ff91eba1bffd86089834e', '2019-10-25 11:21:35', 'RECEIVED'),
(21, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'shereenlim1', '67b9a6d50b962624617fb04054937d3e', '2019-10-25 13:25:33', 'RECEIVED'),
(22, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'adrianlsw', 'b8db4691eb744b346aaa318375ffd9b3', '2019-10-25 13:40:29', 'RECEIVED'),
(23, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Girliechua', '922d5d256639f7520ba01af3488a3e90', '2019-10-25 14:33:14', 'RECEIVED'),
(24, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Jackyng', '696c265f58499da739b52e5660035fa1', '2019-10-25 15:28:39', 'RECEIVED'),
(25, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, '1121188', 'aec0b0afe43de960cc4db7b8c38cc4bf', '2019-10-27 13:48:06', 'RECEIVED'),
(26, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Cynthia', '308bd5a36bcb5e03d63a1d52915b67b0', '2019-10-27 13:55:55', 'RECEIVED'),
(27, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Windlim', '2402a716d533bd79d3d764be9c1d645a', '2019-10-28 10:53:07', 'RECEIVED'),
(30, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Tommygoh', '847c50938aaedc09b473f57ed6068075', '2019-10-29 10:31:16', 'RECEIVED'),
(31, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'kccheah', 'a53112e56048b34a46060f212a8008d8', '2019-10-29 10:42:46', 'RECEIVED'),
(32, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Tommygoh', '847c50938aaedc09b473f57ed6068075', '2019-10-29 15:12:50', 'RECEIVED'),
(33, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Windlim', '2402a716d533bd79d3d764be9c1d645a', '2019-10-30 06:45:49', 'RECEIVED'),
(34, '83ccbb881e19774f5d7a085a6003e245', 'michaelwong', 300, 'Super888', 'f652d9089a1ad2b7e489a16d42e57774', '2019-10-30 10:27:45', 'RECEIVED'),
(35, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 1500, 'Giri25', '211a598c64df7b0149eed4a065dd9a11', '2019-11-04 14:20:44', 'RECEIVED'),
(36, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Spike07', 'f178369e2183b1d97704df861eced6f7', '2019-11-04 15:47:24', 'RECEIVED'),
(37, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'FP', '91e134c76c2134f4ee387275bfe17605', '2019-11-04 16:56:01', 'RECEIVED'),
(38, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 900, 'Jagan4313', '832d825bd80e28135e9a93580591149a', '2019-11-05 02:58:24', 'RECEIVED'),
(39, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Premashan', '2d30ddd67371f37586a419adf3f53716', '2019-11-05 03:42:34', 'RECEIVED'),
(40, '83ccbb881e19774f5d7a085a6003e245', 'michaelwong', 300, 'super01', 'bc8c6c41c1828b1dce9eb6d8fc6fadeb', '2019-11-05 10:17:47', 'RECEIVED'),
(41, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Josieloh', '8ca2402c6c3f7643a93af5eeea795cdd', '2019-11-05 14:12:11', 'RECEIVED'),
(42, '83ccbb881e19774f5d7a085a6003e245', 'michaelwong', 300, 'Super01', 'afd9f21cc7935e7c1a8b6ef1032d4272', '2019-11-07 10:07:11', 'RECEIVED'),
(43, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Cynthia', '308bd5a36bcb5e03d63a1d52915b67b0', '2019-11-07 10:34:41', 'RECEIVED'),
(44, '78ae38b91ca93c0f76b1a5487376d02d', 'DCK', 300, 'Girliechua', '922d5d256639f7520ba01af3488a3e90', '2019-11-07 12:02:28', 'RECEIVED');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL COMMENT 'For login probably if needed',
  `email` varchar(200) DEFAULT NULL COMMENT 'Can login with email too',
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `ic_no` varchar(200) DEFAULT NULL,
  `country_id` int(10) DEFAULT NULL,
  `full_name` varchar(200) DEFAULT NULL,
  `epin` char(64) DEFAULT NULL,
  `salt_epin` char(64) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT '1',
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT '0',
  `login_type` int(2) NOT NULL DEFAULT '1' COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT '1' COMMENT '0 = admin, 1 = normal user',
  `downline_accumulated_points` decimal(50,0) NOT NULL DEFAULT '0' COMMENT 'RM1 = 100 point',
  `can_send_newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `is_referred` tinyint(1) NOT NULL DEFAULT '0',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `address` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `car_model` varchar(200) DEFAULT NULL,
  `car_year` varchar(20) DEFAULT NULL,
  `picture_id` int(12) DEFAULT NULL,
  `register_downline_no` int(255) NOT NULL DEFAULT '0',
  `bonus` int(255) NOT NULL DEFAULT '0',
  `final_amount` int(255) NOT NULL DEFAULT '0',
  `point` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `username`, `email`, `password`, `salt`, `phone_no`, `ic_no`, `country_id`, `full_name`, `epin`, `salt_epin`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `login_type`, `user_type`, `downline_accumulated_points`, `can_send_newsletter`, `is_referred`, `date_created`, `date_updated`, `address`, `birthday`, `gender`, `bank_name`, `bank_account_holder`, `bank_account_no`, `car_model`, `car_year`, `picture_id`, `register_downline_no`, `bonus`, `final_amount`, `point`) VALUES
('01a960f79d75935b58bac0178d6bfb98', 'Lida', 'lida@gmail.com', '719c3b79db30dda37a33b2ed71e9d2ac9532969e75bb05aa9ee3eca859b1f8ba', '2deea69c3b24564393e580c634e8e6416c024bb2', '0189032099', 'C2798834', NULL, 'Tran Thi Cam Tu', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 04:28:35', '2019-10-24 04:48:50', NULL, NULL, 'Female', NULL, 'Tran Thi Cam Tu', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('08c9889ec8f4bb33568861e8b09d245c', 'Rosel', 'rosel@gmail.com', '268a7d00dc52a3afc688de2aa0307623a1937ebeee186c5d6406a6b87d6a1027', '54e60c658c0d240f04d75a1c3fcdb54b55624067', '0124469567', '720429065348', NULL, 'Roshaida Latif', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-06 12:51:16', '2019-11-06 12:54:42', NULL, '1972-04-29', 'Female', NULL, 'Roshaida Latif', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('0b7705fc7026b8ed73dcfd6bf604ed90', 'Choong908', 'choong908@yahoo.com', '1fc14050431be7dd1dbd5f222cf2a7f13fb9ef3ecc444c09ad7ceb1b9cee2da1', 'e54a87832706608482decac74b865c06df468c56', '0164407809', '810908075753', NULL, 'Choon Kah Meng', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 10:38:11', '2019-11-07 10:39:40', NULL, '1981-09-08', 'Male', NULL, 'Choon Kah Meng', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('0c396af354a0bda95e6114a77ecc4cfa', 'shuennchi2020', 'limshuennchi2020@gmail.com', 'ed245466e3c418d496b4f73794f3fce56da2e7cf5aa8911b4e62e915a93841db', '686a199a072e8df71c3462c341a226ff27affc9c', '0145212020', '811204075215', NULL, 'Lim Shuenn Chi', '3d93dfbb5e6371b93aec2da5bee91e7a5f5a71302ceadfc3931304d38d198872', 'e406c7785efedbf851ace5e405abcabc5063e703', NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-19 16:22:53', '2019-11-06 04:29:44', '7,cangkat minden, jalan 15, 11700 penang.', '1981-12-04', 'Male', 'AMBANK BERHAD', 'Lim Shuenn Chi', '0820070001653', 'Hyundai starex', '2016', NULL, 6, 500, 0, '0'),
('10870f70fd7398fb5e1b67183ac98164', 'Mb281', 'pubalansuppramaniam@ymail.com', '93fd98c3e6e25609edb4e1b59c9a8cc7aac7524be97174ef95c8937e41771d8d', 'ce7233c8e0dc409502565247f0958dffe33d181e', '0162774015', '660901135089', NULL, 'Pubalan Suppramaniam', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 09:25:44', '2019-11-07 09:27:39', NULL, '1966-09-11', NULL, NULL, 'Pubalan Suppramaniam', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('149815e890aa7825c56847835b76e1af', 'Limto', 'limto@gmail.com', 'dcce6bd337118001a21a360357c01c691016030e13df1fee4d9e0a3e64ad7556', '157b5e0b6cef3622093e53c85635e0771f2d331e', '0124293331', '680316075319', NULL, 'Lim Theong Oon', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 04:08:28', '2019-10-24 04:10:05', NULL, '1968-03-16', 'Male', NULL, 'Lim Theong Oon', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('166dcd604b821b4a496ca901f6ff6310', 'ThePinnacle', 'pinnacle.house.of.trading@gmail.com', 'a4aef737398fb2aa86a920a23f009a29520f015958cbb1d67fff235560cb4dd6', '69a5694877708496809c8b5c2f869e19d24ea195', '0124802525', 'SA0525045', NULL, 'Pinnacle House of Trading', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 16:11:30', '2019-11-07 07:08:26', 'A-2-02, Jalan SS6/5A, Dataran Glomac, Kelana Jaya 47301 Petaling Jaya, Selangor Darul Ehsan', '1958-11-01', 'Male', NULL, 'Pinnacle House of Trading', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('197c08e85e66915dc9aeca5759cce4ec', 'cchow', 'ah_how2005@yahoo.com', '8d9d7a02e96c0dcc775fbe88277bf6747f17adbaa28b81889cdcd8b08ff187b1', 'fd8608fbbeac932f31bdbf78978ee2c7dace4a8b', '0164222586', '641015025113', NULL, 'Chow Chean How', '9d7cced6d531efd43c9d9d8e2211a99b9fb49786db19996eb0ef9385d4878b18', 'e94eac69474c994ac031d5d1a6d7547ec15d6536', NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 06:55:23', '2019-11-07 05:20:45', '75,19-19,sri wangsa1 jalan pernawar 1,11600,georgetown,pulau pinang', '1964-10-15', 'Male', 'HONG LEONG BANK BERHAD', 'Chow Chean How', '11900006309', 'toyota awanza1.3', '2005', NULL, 6, 500, 0, '0'),
('1a74603a1bf7b6d800693f1d17c1caa9', 'Kienkien21', 'ooikienkien@gmail.com', '61fa9aed573b884af7c45c3e3c50b6169851cb4af3049be9c1e6907a020f9893', 'c4a1380bd76b0f833d925314d286748faf1afc92', '0162055315', '620521105332', NULL, 'Ooi Kien Kien', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-04 15:49:32', '2019-11-04 15:50:57', NULL, '1962-05-21', 'Female', NULL, 'Ooi Kien Kien', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('1a768058fc1484d390e842842ed0a2fe', 'Yibhin', 'tyb9961@gmail.com', '046a1c41eb869d9de3cdc14240ee675d33fa5ed09f6ada5ca8cad39c51cb4c51', '7508e0e252d10afb96db0314def57082c77827a2', '0174541007', '990601075899', NULL, 'Teoh Yi Bhin', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 07:44:15', '2019-10-21 07:48:50', NULL, '1999-06-01', 'Male', NULL, 'Teoh Yi Bhin', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('1b33037d3f15da3ebffe7511defe6c47', 'laypheng', 'c_laypheng@gmail.com', 'd4777e394f98920f5857020b89cd04f986c4dddc81aaefbf6de675328d0741c2', 'e628f0bc2f93336a8974a0153688708d343573ad', '0162300420', '810420075418', NULL, 'Cheah Lay Pheng', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 16:19:50', '2019-10-20 17:15:31', NULL, '1981-04-20', 'Female', NULL, 'Cheah Lay Pheng', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('21004884ae70517ad3f30ea5f9a5537c', 'Khlim', 'kh.marketing33@gmail.com', 'aea26c8a886f2d4a2614fe1e542c9ad3b5b9fe61673797f93ca528d234aeda00', '5d9e7168f643845e81e7e8f7a68bc1e39af4695b', '0164228133', '580207715091', NULL, 'Liem Khai Huat', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 04:58:09', '2019-11-07 05:33:22', NULL, '1958-02-07', 'Male', NULL, 'Liem Khai Huat', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('211a598c64df7b0149eed4a065dd9a11', 'Giri25', 'wowmasala.com@gmail.com', '78d9244a231d20ca904294c3b9bd702e2456212a3dc2a6255e56a4805de7143f', '065ef6b55471511f0bc11cbf81455c791a47fd03', '0133672525', '650525075561', NULL, 'Thilaganathan Kaliappan', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 05:49:16', '2019-11-07 15:47:44', 'A-3A-7 Sterling Condo SS7/19 Petaling Jaya 47301 Selangor', '1965-05-25', 'Male', 'MAYBANK', 'Thilaganathan Kaliappan', '114393145774', 'Subaru XV', '2017', NULL, 5, 450, 450, '0'),
('22dfebc6edf3014ee5f6b378fcc3e76f', 'Hareey', 'harikrishnsn.shanmugam@tssco.com.my', 'e0b14344eecf8093625379c6e9d15d61ee0919ea7242c3542217c42d3c69d571', 'd6c1a0c65d21fae43d7814822fc1a5d21fe4c538', '0192442278', '761204075087', NULL, 'Harikrishnan A/L Shanmugam', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 15:53:49', '2019-10-24 15:56:50', NULL, '1976-12-04', 'Male', NULL, 'Harikrishnan A/L Shanmugam', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('2402a716d533bd79d3d764be9c1d645a', 'Windlim', 'windlim1994@gmail.com', 'e4ccea89995aee2e60447e1afd1fc677219284a615a04dea757f68b90755250b', 'b5b1837cd9ca114d4c96ab73414467978ffb40e5', '0184000170', '941029075765', NULL, 'Lim Eng Hong', '5007b5c5192751127eb04b9fe0980f16c0f63486dedbe7f2a7c8598fb2219406', '41fefea58690f61eefe3215594db1ce1166b4056', NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-19 17:04:02', '2019-11-04 13:47:08', NULL, '1994-10-29', 'Male', 'MAYBANK', 'Lim Eng Hong', '157175178662', NULL, NULL, NULL, 4, 400, 400, '0'),
('24e54274fe0f3991573ad54eaab32737', 'Appleooi', 'owmusic1001@gmail.com', '3c6db54ef32aad3e2856a34e3f42038b77e9fa0301912bec67d258e6853eb231', '984540cee74de062cbd39f379d89e81d6bf5b09d', '0124723618', '770718076022', NULL, 'Ooi Choon Ying', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-05 14:15:01', '2019-11-05 14:17:08', NULL, '1977-07-18', 'Female', NULL, 'Ooi Choon Ying', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('259e4f29527b5f97361b9431416626cd', 'Ongkc77', 'ongkc77@gmail.com', '8bc02798ce98934ee93d12e5345b91536b8e8b21b038db998f4a0b092939d1fc', '73eabb773c4b2cad0c9858838693f1b5ed33cab3', '0124322442', '770529025875', NULL, 'Ong Kar Chun', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 03:47:10', '2019-10-24 03:52:53', NULL, '1977-05-29', 'Male', NULL, 'Ong Kar Chun', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('2d30ddd67371f37586a419adf3f53716', 'Premashan', 'premashan30@gmail.com', '57f551418470368369c2258f6cfbb93a65da33d509447555bb7eeea2f5426a26', 'f9b6838ba66a9a580f1d52059c53cb3905c93fd6', '0124128566', '591230075460', NULL, 'S.Prema A/P G.Shanmugam', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-05 03:04:29', '2019-11-05 03:48:28', NULL, '1959-12-30', 'Female', NULL, 'S.Prema A/P G.Shanmugam', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('308bd5a36bcb5e03d63a1d52915b67b0', 'Cynthia', 'diamond5720@yahoo.com', '540344fd7baf0988527293f4d6be1ca604f7c7e2f290d700787a98d78bfec44d', 'f58bff35bee2ac94e8c514a2b61353db0d77c8fd', '0165531559', '640209075720', NULL, 'Lim Joo Sim', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 09:14:44', '2019-11-07 10:38:11', NULL, '1964-02-09', 'Female', NULL, 'Lim Joo Sim', NULL, NULL, NULL, NULL, 4, 400, 400, '0'),
('30f3a507f500c23ac7437d8173880b73', 'Vanessa', 'sasatijo@gmail.com', 'e9b9f62aa2cc9883091d8192d3587b20537b1eb849fbf85ac387324dcf49271f', 'edd8673b7e30a825fa45b0b9af87b9ce45377f02', '0124930216', '781026075778', NULL, 'Lim Siew Chin', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-29 15:16:30', '2019-10-29 15:19:46', NULL, '1978-10-26', 'Female', NULL, 'Lim Siew Chin', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('3113539eeb804ee3e4240410d2569700', 'kavimohan', 'paru2067@gmail.com', '627db69c4f21e884fef44d84d2779c0f432c40c6086f662be650170391d5d0dc', 'dc206684d256877fc30c68e682deb48458bb3876', '0125151343', '600620075548', NULL, 'A Parkavi K Arumugam', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 11:34:14', '2019-10-25 11:58:05', NULL, '1960-06-20', 'Female', NULL, 'A Parkavi K Arumugam', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('3631890bc9a862b5f7ceac83c4f5d1e8', 'Ectan', 'tanengchye670618@gmail.com', 'b4534da26eabcdee934b80e244ebb7d749362d72ae18493b306361f4ca17a93e', '5fded3a75a70b1be16b862b29e7e5b059082e7b5', '0125218792', '670618075307', NULL, 'Tan Eng Chye', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-19 16:02:21', '2019-10-20 15:49:37', NULL, '1967-06-18', 'Male', NULL, 'Tan Eng Chye', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('38eada4b5b84b758002df7d74dc4722e', 'Erickooi', 'uniericpro@gmail.com', '9521fedc4d0d2777c6efea1cb21d4eb3c98e9a83e260b74e66595326869fb4c7', '545325b345ab8c3a8cd91468719a44fb3a1acfd3', '0124870678', '670422025025', NULL, 'Kooi Chore Kwang', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-19 17:17:43', '2019-10-24 11:52:14', NULL, '1967-04-22', 'Male', NULL, 'Kooi Chore Kwang', NULL, NULL, NULL, NULL, 2, 300, 300, '0'),
('39b50f80956cd29ebf4f975c0b579c53', 'dy7329', 'david.yeoh@yahoo.com', 'c303868c3fdb19955160fbe1218e6cd84b9b5da9b6f754142392d65ed18a9253', '2d8d926838129d7952fc58a659861c4161e579b6', '0174975898', '740829075447', NULL, 'Yeoh Seng Aik', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 06:47:59', '2019-11-05 10:48:02', NULL, '1974-08-29', 'Male', 'MAYBANK', 'Yeoh Seng Aik', '107031489377', 'Nissan Almera', '2014', NULL, 2, 300, 300, '0'),
('3f6145490f75b943f067e2506cea0454', 'Fattchai', 'siewyeefatt69@gmail.com', '7cb282c158aa47f455423ef6cf27273aecf87288093d98ef5eeef1d3a48f8a11', 'fd297c3e67082dc4e0c75fa558e684f80a5b671c', '0124461333', '690325085657', NULL, 'Siew Yee Fatt', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 15:28:19', '2019-10-20 15:42:36', NULL, '1969-03-25', 'Male', NULL, 'Siew Yee Fatt', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('3f9e1ae442f49cf7fc9c80584834fd12', 'Dolly', 'dollysharimi@gmail.com', 'ac3f09605b7e8969a1ba21645f76af87372504646d6653e82a8aed19ff45a587', '94eda93b861de4d5c38961b86c6e4321bc12ed50', '0163536209', '540724075444', NULL, 'Daljit Kaur A/P Sharimi Singh', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 11:36:45', '2019-10-25 12:02:45', NULL, '1954-07-24', 'Female', NULL, 'Daljit Kaur A/P Sharimi Singh', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('415c193d1efa340d3a5ddf2455efa7e2', 'Kimleang', 'ah_how2008@yahoo.com', '5d805e2c60c24d4798511e80b0827a972654ca90f0fb64af8b06d8abc130434c', 'fc90c5a1e9c8d2fb0968918547a04b3c0c990418', '0164222586', '670620025065', NULL, 'Chow Kim Leng', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 07:19:23', '2019-10-20 07:20:58', NULL, '1967-06-20', 'Male', NULL, 'Chow Kim Leng', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('430a4456ee40d1707c8a44cceb0c9f65', 'ilifestyles', 'ilifestyles@gmail.com', 'f4ede28814ad044b016c18647ee6b82627d4540c33431f971a6aa1fbd63f8c2e', 'bbe70cc77870dbf12dab948d173e8a6cb7db66aa', '0195600000', '801218075639', NULL, 'Lim Kar Loong', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 15:30:34', '2019-10-20 15:40:18', NULL, '1980-12-18', 'Male', NULL, 'Lim Kar Loong', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('44e8b0034b3d59f5d981194bb90b6932', 'Xx', NULL, 'c78dcbc269530a12b115738f5cfacbaa8b8d77b6704c3cacaf022ab0b72681d6', 'f92d2df09e7b9fae06458b85c10e8ed10f1cdaf1', NULL, '333', NULL, 'Xx', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 09:47:41', '2019-11-07 09:47:41', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('46f4608805d4cc241b114971cb432484', 'Gopal', 'gopalplp@gmail.com', '4ab2841e54d69425098392c415831b96a54fb241a75c660525d73167e42c46c7', 'f1e6c65ec0a69fa49a367cc66852b4773c2959f9', '0122991114', '550827075219', NULL, 'Gopal Balakrishnan A/L Pakarisamy', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-05 03:48:28', '2019-11-05 03:50:18', NULL, '1955-08-27', 'Male', NULL, 'Gopal Balakrishnan A/L Pakarisamy', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('47b4c06089ca56a99379a89e99e57dd1', 'Joelktan', 'joelkltan@gmail.com', '5a3970f01b9f40b31b3e6cbe3100215f2d5513649363e15623f0e6dcc8dfa680', '335650515e5b87ca9caa0e0a75ec0e433fafed2b', '0164285723', '900701075069', NULL, 'Joel Tan Kai Li', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 17:07:22', '2019-10-20 17:10:41', NULL, '1990-07-01', 'Male', NULL, 'Joel Tan Kai Li', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('48956f2889a02a6ce7b8fbda2f9b7de7', 'wenzhing', 'wenzhing0325@gmail.com', '3a00bb8b875fa558b13b5c601343f66767234ee5ffb688e175b588de6c7e95fc', 'd2e68923fa8fad59a6c3ad105fde3e2ca2f63e93', '0174430025', '980325355018', NULL, 'Teoh Wen Zhing', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 07:34:57', '2019-10-21 07:39:06', NULL, '1998-03-25', 'Female', NULL, 'Teoh Wen Zhing', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('495cbfbf29106f35140826824978a5aa', 'Mackensontan', 'hkvmackenson@gmail.com', '47eb7c81dbea61426c8095ec769e707bbc2f6f71510cfe7fa831fdd3c32503ea', '2805fdf8e2bd7a799a89aa7e0d6828d989f9a6c9', '0124413907', '630216075527', NULL, 'Tan Siah Khoon', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-30 06:49:32', '2019-10-30 08:07:08', NULL, '1963-02-16', 'Male', NULL, 'Tan Siah Khoon', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('53fca61f0dff423502d344054b799ba2', 'Chia2123', 'chia2152@gmail.com', '873b09481b0ae8d7643e7196b70213d503b5728ed17c81e20ba727766a06afcb', 'b44f47ad9cf236888b4f6e6ef7c55797de3abfaa', '0123292152', '640623085369', NULL, 'Chia Eng Hua', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-19 16:12:16', '2019-10-20 15:57:45', NULL, '1964-06-23', 'Male', NULL, 'Chia Eng Hua', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('545ab794670fb8c88f22aefcd1a64526', 'yytay99', 'yytay@gmail.com', 'b33864921551d98fc6bab3b9f141c20f5a526dbd09b85a47c0afc70a22d47ec9', '81ea9d93d8b9304653c63abd1a5f644d2f4a63a9', '0124835299', '740618075359', NULL, 'Tay Yew Huat', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 11:52:14', '2019-10-24 11:54:38', NULL, '1974-06-18', 'Male', NULL, 'Tay Yew Huat', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('54baf180ba2c6838a5aa0545d0ad1734', 'st4589', 'tch.sthour@hotmail.com', '2365370c931c82be4025c7cefe2d0ce286657f4eddfe853ee1d948c32743bb96', '06029925555dc5e7b2a034425ad938136030d736', '0109634589', '830628025733', NULL, 'Tan Chee Haur', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 16:50:37', '2019-10-20 16:52:33', NULL, '1983-06-28', 'Male', NULL, 'Tan Chee Haur', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('593e0d4cf3db550d08da2a90cff39ed5', 'shunyun', 'forcelim92@gmail.com', '00214dfab93e061fd2ee9beff39f1d538aaf0a61e7cd4dd79b6398d68bc965fe', '5f018891da684601ff89dd7614f2006830ea14fa', '0194448844', '921202075753', NULL, 'Lim Shun Yun', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 17:15:31', '2019-10-20 17:17:29', NULL, '1992-12-02', 'Female', NULL, 'Lim Shun Yun', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('61ab80fc5dcd02fbc7f6525c00f24b04', 'Reds233', 'reds233@gmail.com', 'ca879f9998402b5ac0e33ac3767e763cfd5bd89926e77584dcf5af061183e577', 'ad53a2303b775633531b3c87ac9fd29a553dad1b', '0133730835', '650315107385', NULL, 'Amos John Alphonso', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-04 14:34:44', '2019-11-05 04:51:15', NULL, '1965-03-15', 'Male', NULL, 'Amos John Alphonso', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('6451b38b2f3b7b6f54eaf0eed28bbaca', 'Dawn', 'fabianparamakuru@yahoo.com', 'dce9bf9c454543d200088280b4b49fad9ff698beefbe482fef760211ecb42ca8', 'a70d87810c6391809bcfed649a7caf54b2c17abe', '0125087485', '681025145190', NULL, 'Dawn Gabrielle Pereira', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-04 17:01:34', '2019-11-07 12:21:03', '13-3-18, Apartment Bustan Shamelin, Jalan 2/91A, Taman Shamelin Perkasa Cheras, Kuala Lumpur', '1968-10-25', 'Female', 'CIMB BANK BERHAD', 'Dawn Gabrielle Pereira', '706 568 1975', 'Honda Freed', '2012', NULL, 0, 0, 0, '0'),
('67b9a6d50b962624617fb04054937d3e', 'shereenlim1', 'shereenlim1@gmail.com', '7e1c166aa26745e5510eab802c4910dd312a31fdd838421ce8ad2fa7b32a1edc', 'b17ef2d3f78ae0545c4e866b45f75ff794d8f3b4', '0129155571', '6080093', NULL, 'Lim Chooi Chin', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 11:25:19', '2019-11-07 06:24:44', NULL, NULL, 'Female', NULL, 'Lim Chooi Chin', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('695e3f561feb654cd1cd8f7a47c75345', 'cklim', 'cklim5155@gmail.com', '9ff989fe5e83b72c3d9f2cee2e4d8dc5785b1175a22abf53ff6fd6cb51d45da2', '2866e008b763f18c84c60767f9760d79ed9b3749', '0124725952', '670124075155', NULL, 'Lim Cheng Keat', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 0, '2019-09-30 07:03:59', '2019-11-05 11:15:28', NULL, '1968-01-24', 'Male', 'MAYBANK', 'Lim Cheng Keat', '107116401077', NULL, NULL, NULL, 0, 0, 0, '0'),
('696c265f58499da739b52e5660035fa1', 'Jackyng', 'jackyng686@gmail.com', '14d5f6ac9ccbcfa77eef369c15983ef7e5d0e1770d2027fd6c35866f6b93b6ca', 'b4a5005584979e1fa3de3e020f990ffdc019a627', '0124286869', '721128075109', NULL, 'Ng Poh Hock', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 14:39:08', '2019-10-25 15:33:29', NULL, '1972-11-28', 'Male', NULL, 'Ng Poh Hock', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('6a38de7d5332758f156cdad51f16ef67', 'wawasan', 'wawasanutara123@gmail.com', 'ef7c428221494a3a98fc13711af37bc51184200ada28c3b74588f81565ea3b7f', 'f6379b01ff0c30c538e15f4d7f8f5faaf0cdd791', '0124281527', '740620075403', NULL, 'Ooi Teng Kooi', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-27 13:51:58', '2019-10-27 13:54:36', NULL, '1974-06-20', 'Male', NULL, 'Ooi Teng Kooi', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('6afc28fde2616be86a619428e274de7f', 'Dankor', 'dankor@gmail.com', '1e68d2bc4bbb54b8ce914adc87e4897ee271982d1b9b2238cbbde9353e34b2b0', '6e0a13ed1d7a0b64a50c7dd822aa2f83b11782bd', '0125302911', '701125025191', NULL, 'Kor Boo Huat', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 04:02:48', '2019-11-07 04:46:40', 'No.3, Lorong BLM 3/4, Bandar Laguna Merbok, 08000, Sungai Petani Kedah.', '1970-11-25', 'Male', 'MAYBANK', 'Kor Boo Huat', '102102016884', 'Mercedes-Benz C200', '2012', NULL, 0, 0, 0, '0'),
('6bd848a66b4fdf0ad68042aac36d9c93', 'chengkee', 'zabi9088@gmail.com', '018f0c31d2abfddcb5d3a8c1bd3ad466200bd3765276c8d7faca8b51c19ecbca', 'e6476b889296e9a70e126ecc60efdb284e21e26c', '019569555', '690915075811', NULL, 'Kee Eng Cheng', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 15:33:29', '2019-10-25 15:35:44', NULL, '1969-09-15', 'Male', NULL, 'Kee Eng Cheng', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('722405502b37aeeb2c2bcfcf63f3dbe7', 'Peterchen', 'maihoa8999@yahoo.com', '7f76d5375121ee99e8ceb06387a4cb3e20d27668348cde4a6f0bb44060754bde', '9f97c4b89ec0dae8e5ed062f94654bf2979a5667', '0194448236', '650514065181', NULL, 'Chen Yoon Fatt', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 08:41:22', '2019-10-21 08:44:05', NULL, '1965-05-14', 'Male', NULL, 'Chen Yoon Fatt', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('7673d904296328fe996510a9a7c504b0', 'Jwong', 'ongjw.ojw600@gmail.com', '7c8a98a8adf78fcfc98927556a67f1fb74b07c0db40d7cb74babdfdbcc69bba6', '910cd41e36caa9a2e1b4122a4819d28870d83d63', '01111728188', '980526075573', NULL, 'Ong Jeng Wei', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-19 16:37:59', '2019-10-24 05:13:02', '18 Persiaran BLM 3A Bandar Laguna Merbok 08000 Sg Pertani Kedah', '1998-05-26', 'Male', 'CIMB BANK BERHAD', 'Ong Jeng Wei', '7069915852', 'Axia', '2017', NULL, 6, 500, 500, '0'),
('773a0ae6095f0ba243ec16c3ea818ff6', 'Layyean', 'ah_how2007@yahoo.com', 'af4d22784f892ebeefdc4c7e65777f0dfb718f414041b1979bf9c180a9aba8d3', 'ca55432873a9ae887cc1bd67693ae1528659e49f', '0164222586', '670420075694', NULL, 'Chon Lay Yean', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 07:13:17', '2019-10-20 07:14:55', NULL, '1967-04-20', 'Female', NULL, 'Chon Lay Yean', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('78ae38b91ca93c0f76b1a5487376d02d', 'DCK', NULL, '287fb194e19c75afc5c40c544656b1c6bfb99ceb353f1990497b8c4abacbe936', '033426541db7c84dde2a3c454cc6b231f6f863fa', '0124725952', 'A000', NULL, 'DCK', 'f37b8ba91dd71fb9f5d4d1c7eed9c8bd6d5d79922418f4bbadbe039507f0a8d5', 'a3cdc109e08a9a4291ca96c378bf957bdca07a0e', NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-16 09:38:11', '2019-11-07 12:02:28', NULL, '1967-01-24', 'Male', 'PUBLIC BANK BERHAD', 'DCK', '3216072811', 'BMW', '2011', 18, 12, 300, 300, '99983200'),
('7d85a3e759a8e44d79b00749e33d5391', 'wendy', 'wendylimchewwen@gmail.com', '70daed670348592d737a594479333bcc05e214fbb4ea290b3b56d9fc2fdaef4e', 'c14b048f29c80ad1d152e9c264e33b5d1caa5a1f', '0124023181', '000512070134', NULL, 'Lim Chew Wen', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-19 15:49:54', '2019-10-19 15:55:21', NULL, '2000-05-12', 'Female', NULL, 'Lim Chew Wen', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('832d825bd80e28135e9a93580591149a', 'Jagan4313', 'jagan4313@yahoo.com.uk', 'e4b26887e7a1f85e0fcd697863108809d5c190e0577adb3d37812386f56a12f7', '527ebfe1e380d6fe9b40801bcb476073bfefb0aa', '0124865636', '560404075187', NULL, 'Jagan Mohan A/L Venugopal', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 11:29:22', '2019-11-05 03:19:14', NULL, '1956-04-04', 'Male', NULL, 'Jagan Mohan A/L Venugopal', NULL, NULL, NULL, NULL, 3, 350, 350, '0'),
('83ccbb881e19774f5d7a085a6003e245', 'michaelwong', 'michael@gg.cc', '8221e94a9ef93f8a286be0d2db6fbdcbfe1ae1890c1b57d2d4e7763034ceb2bf', 'd39d18e1bef4a1154994351e08bab2f19604687d', '1122334455', '12345', NULL, 'michaelwong', '9012633b766042fb9d82bed00036a7bcbc5b56a0fdba0b5cdd6a13bd8b4c7cb7', 'cb3e6899040084c7fe9b1b865a728086ab04af98', NULL, 1, 0, 1, 1, 0, 0, 0, '2019-10-16 09:51:52', '2019-11-07 10:07:11', NULL, NULL, NULL, 'BANK OF TOKYO-MITSUBISHI UFJ (M) BHD', 'michaelwong', '11223355225522', NULL, NULL, 15, 3, 350, 1350, '1800'),
('847c50938aaedc09b473f57ed6068075', 'Tommygoh', 'tommygoh523@gmail.com', '83ea071fc6904a79ee83f5a5a71541701d54d2874b435e3ee2ffdc184edeb379', 'dd40b05545c5e950c6db1ae16197b9769410d2d3', '0164117932', '590213075755', NULL, 'Goh Chin Teik', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 14:50:42', '2019-10-29 15:16:30', NULL, '1959-02-13', 'Male', NULL, 'Goh Chin Teik', NULL, NULL, NULL, NULL, 2, 300, 300, '0'),
('84f2e1c258036f9f6cdfa371320f15a8', 'Aina52', 'camposaina@yahoo.com', '49fd7463611b0e63e0c12efb59e9d5147b2bc8c74cebe6e3a7d84236348d9921', 'ed72a2dd9a2eaf9e8a8f32fc3ba50d76480c12f5', '0123416895', '520304105506', NULL, 'Aina Bhandari A/P R.C.Bhandari', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-04 14:29:21', '2019-11-04 14:40:57', NULL, '1952-03-04', 'Female', NULL, 'Aina Bhandari A/P R.C.Bhandari', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('86d0613407b61357a5bb1300832b86ff', 'Jacky181', 'jackylim3181@yahoo.com', '3aa642701434791c0712f96560222d9289f84beac1e586495d434521941ea3cb', '9fa6658447e740e04908ae4f94b0c2ebb5ed0f98', '0124803181', '690129075523', NULL, 'Lim Gim Chye', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-19 15:40:09', '2019-11-05 14:04:36', '22,Jalan Cermai. Taman Cermai. 13500 Pmtg Pauh.', '1969-01-29', 'Male', 'AMBANK BERHAD', 'Lim Gim Chye', '020 201 500 3963', 'Toyota Vellfire', '2015', NULL, 6, 500, 500, '0'),
('876fd5c039eb5a7006d65f550941a81e', 'suiyuan', 'ah_how2009@yahoo.com', '0f5ae0cc0a2399224fce6a4ced977bc14d52bd99d8546b1008e73f6690b8619d', 'c1af1c5e1deb5f1c0be47381b53240bced0cb292', '0164222586', '000424070070', NULL, 'Chow Sui Yuan', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 07:23:48', '2019-10-20 07:25:42', NULL, '2000-04-24', 'Female', NULL, 'Chow Sui Yuan', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('887ee291da411b05c8b8ef7c760ec19b', 'jackychen', 'jackychenyw@gmail.com', 'dfa1b9cd49c5c57ee8d12b52c5d84ac2cb8a3b0cb55ca74550d8b551a5904af1', '403839439286c9b7f8d4369c3472f34e2496e1c6', '01135143931', '620424065041', NULL, 'Chen Yoong Wah', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 07:54:11', '2019-10-21 08:41:22', NULL, '1962-04-24', NULL, NULL, 'Chen Yoong Wah', NULL, NULL, NULL, NULL, 2, 300, 300, '0'),
('8ca2402c6c3f7643a93af5eeea795cdd', 'Josieloh', 'loh.s.h.2263@gmail.com', '1896c63d2ea897840794c026b8373078e131d78f62e0c015c9135bb31ecae370', '0d2d25b55624b28c114f0d42f76e551d6496c108', '0164732263', '760126095044', NULL, 'Loh Siew Heong', '6a3be240c940d29a1d3999460212bc34cb1bcb1351b7379d53b95b428d8be701', 'd953e59551873059af66a7ae28c312b7888ac2a4', NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 16:09:38', '2019-11-06 04:33:07', '123', '1976-01-26', 'Female', 'MAYBANK', 'Loh Siew Heong', '557241061451', 'honda', '2014', NULL, 4, 400, 0, '0'),
('8d335b3a10125e39ce46d611d88b2b4a', 'DCK1', NULL, '97d922f590cdb48b2d73c737c5c4946ea77e32d6c9cfbe5afdcd45c4c477a4eb', '0452f7c95496abe90cbb92b9b76be57e0ffbf658', NULL, 'A001', NULL, NULL, '147cc02edb5c083cf207c1fb3b0abb9a174357c83791a0ad68890d8acf8b7930', '946fe0c346d29c4b21cb4d7135876666da0a4f51', NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-16 09:41:39', '2019-11-05 09:53:44', NULL, NULL, NULL, 'PUBLIC BANK BERHAD', NULL, '3216072811', NULL, NULL, NULL, 1, 150, 0, '150'),
('8db2bccf185d95140389ea22d7a25a7d', 'Chyrie', 'chyriechua00@hotmail.com', 'a1e37fca583c955d70de8702309ab314977f9f1c46a8bf2164e47d6c835ce41b', '2538ea9bc1464cf71647c3390187e21f108fe620', '0149456724', '921123075472', NULL, 'Chua Hsu Yann', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 05:08:43', '2019-11-07 05:37:53', NULL, '1992-11-23', 'Female', NULL, 'Chua Hsu Yann', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('8ece43cdc970996fbdca121a9194815a', 'Leo', 'lizhitian08@gmail.com', 'a1854667e857c5e6c992ee9836b6be01a0f6965ff63a219d7bb8d479d70449ad', '9a96e8af0fea134726f344045d09187cf036cb59', '0149994308', 'EE1285848', NULL, 'Li Zhitian', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 16:42:02', '2019-10-21 08:21:24', NULL, '1977-05-03', 'Male', NULL, 'Li Zhitian', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('8f2d875cf8c41c81548dacb37497c7cf', 'maihoa', 'maihoa8998@yahoo.com', '3f8d39d0c398b38a12aea2d9a91551cd75bb7378f38eb45fe3bfd1a794a56915', '8e3589c96f20180a308ac6e08fb3e96175f4b629', '0178998319', '720101685068', NULL, 'Nguyen Mai Hoa', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 07:26:38', '2019-11-06 01:58:21', NULL, '1972-01-01', 'Female', NULL, 'Nguyen Mai Hoa', NULL, NULL, NULL, NULL, 4, 400, 400, '0'),
('8fa12e6b81af70b499f5f1e299087191', 'Fabian', 'fabianthh@yahoo.com', '1441c3ec5de0604bac883b5a94247eaa45e846ac864d1fb5209e3b621af665d6', '9949aaddd02957ce8fad94cf6f25867058358891', '0194452972', '680311075301', NULL, 'Tan Heng Hean', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 08:49:28', '2019-10-24 08:51:20', NULL, '1968-03-11', 'Male', NULL, 'Tan Heng Hean', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('91ddb6bea21b3218bd7c52c8a13e4cbc', 'galaxykc', 'eason19730@yahoo.com', 'bc21a48629a331d9151f2bce1761479cd064dbd4782e1fed71584331aa068121', 'fefc2449afbf27f0786f9489237d397f3b97b023', '0194135566', '731009075519', NULL, 'Tan Kuan Chye', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 07:29:49', '2019-10-20 07:31:30', NULL, '1973-10-09', 'Male', NULL, 'Tan Kuan Chye', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('91e134c76c2134f4ee387275bfe17605', 'FP', 'fabianparamakuru@gmail.com', '5d8b51366145b1d1f1261747c22912871b963aac61b580e8745b8a21babea066', '3e7078188622008f4439343247652bd280850ada', '0125087485', '660119086053', NULL, 'Fabian Paramakuru', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 11:42:42', '2019-11-07 12:03:25', '13-3-18, Apartment Bustan Shamelin, Jalan 2/91A, Taman Shamelin Perkasa Cheras, Kuala Lumpur', '1966-01-19', 'Male', 'RHB BANK BERHAD', 'Fabian Paramakuru', '1-12095-0028892-0', 'MYVI EZ', '2011', NULL, 1, 150, 150, '0'),
('91fa223cd25fec9982d166cf551639d6', 'DCK2', NULL, 'e6b8f88cb997d9b8c339e9afdc5095d5195865ed81e96ae688dbbca808bea3cd', '7a7975add5169aabe2daa807abe645177c08a61c', NULL, 'A002', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-16 09:42:54', '2019-11-05 09:54:23', NULL, NULL, NULL, 'PUBLIC BANK BERHAD', NULL, '3216072811', NULL, NULL, NULL, 1, 150, 150, '0'),
('922d5d256639f7520ba01af3488a3e90', 'Girliechua', 'clyue2000@yahoo.com', '946f9dafe0863e2013dec2998e7d13b90f2dc672c02f2edbb11a1d48875945f5', '7a18daf09e737f9bbec7c8cb22b6781c7f76890c', '0173954022', '620317075586', NULL, 'Chua Lay Guat', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 13:13:13', '2019-11-07 12:05:14', NULL, '1962-03-17', 'Female', NULL, 'Chua Lay Guat', NULL, NULL, NULL, NULL, 5, 450, 450, '0'),
('933d96d810218e875730b90af74b016d', 'Chitra', 'chitra@taipanfreight', '659b3dcc137a9317e1c3c0d7e0da249b729cd0512b9de4cfcc53cbbe923e98a2', '1390cf76e13e57845fe2e015991266f0dc4547e6', '0164234235', '620723075012', NULL, 'Chitra A/P Shanmugam', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 07:44:30', '2019-11-08 01:30:24', '2-G-6 MUTIARA VIEW, LRG DELIMA 13, ISLAND GLADES, 11700 PENANG', '1962-07-23', 'Female', 'CIMB BANK BERHAD', 'Chitra A/P Shanmugam', '7021413645', 'MYVI 1.3 (Purple)', '2012', NULL, 0, 0, 0, '0'),
('95bcdb2d0ee44e646cba4b5e9e27472c', 'Albert', 'albertgsc@gmail.com', 'ec86c2c4a1783317e08fe9be6447b5cdf98e2add106f990b2281c84e0a1686d1', '0641656d357261b579cd94667b61cd74f356c621', '0124989272', '641102095005', NULL, 'Gan Sing Chaw', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 08:21:24', '2019-10-21 08:23:40', NULL, '1964-11-02', 'Male', NULL, 'Gan Sing Chaw', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('973c3b0dbaffaab3dde7402566e2dfd6', 'Allinto1', 'josephkoay@yahoo.com', 'e76bd05ce3630ebe2ed59d87c2e165f404c1d37a7c9b69537cde6fd928d16beb', '8ce4fa2a720afd3e8661e498083b66948fe0a30e', '0124778232', '680125075199', NULL, 'Koay Peng Hooi', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 08:34:44', '2019-10-25 08:51:54', NULL, '1968-01-25', 'Male', NULL, 'Koay Peng Kooi', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('994872e4a1d63462e47de647c57ea264', 'admin', NULL, 'fad60f404b8d6ac76c78f5b89b8f3f61d644ceddc80bdb60d6eab116ac98e1da', 'ebdd75696f849850a95d47ae9e40c029e711b232', NULL, '012012', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, '2019-10-18 03:12:32', '2019-10-29 07:48:02', NULL, NULL, NULL, 'MAYBANK ISLAMIC BERHAD', 'adminnoacc', '001100220033', NULL, NULL, 12, 0, 0, 0, '0'),
('9de7598ef605823ce722f226c41ad5ec', 'Kshslim', 'kshslim@gmail.com', 'b3cf5cb4fe6306cd9948b5afac1866e23197993d1fb80496e0af500f06e19f4b', '6003e227a992e46916869340a016fea859c914d6', '0164150077', '630824076013', NULL, 'Lim Khong Soon', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 12:05:14', '2019-11-07 12:06:34', NULL, '1963-08-24', 'Female', NULL, 'Lim Khong Soon', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('9f84e5ed3bd30d6a1cde40ab2cca7f30', 'Cam', 'cam33@gmail.com', '05e39cbaba6c07037422a2f6c9b0e68642f5f896924bd589cde6f11cd1dd3261', '16e1f1789c58923bd57237cf189a679ff24d0b1b', '0164228133', '601006086014', NULL, 'Lee Yin Kwan', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 05:00:00', '2019-11-07 05:36:12', NULL, '1960-10-06', 'Female', NULL, 'Lee Yin Kwan', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('a30ac6b232b13c4b187ec873ae16f1a1', 'Teik', 'cklim5156@gmail.com', 'b475bbf752c49f7ada27175d68001edd33e358e23dfaf1e059b4e2666eff3542', '7e70d4aaaa81afa8315ca124e10bf9337889569e', '0123352086', '580310075029', NULL, 'Lim Cheng Teik', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-19 17:10:46', '2019-10-19 17:14:09', NULL, '1958-03-10', 'Male', NULL, 'Lim Cheng Teik', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('a53112e56048b34a46060f212a8008d8', 'kccheah', 'cheahkhianchews@gmail.com', '8ee41f6fe22227bcf6572522d6aae4fc42dbe09c0d7575148bfede88693b6a06', '3ae58bcf3431819307a886a5107549e1f90a69d4', '0194470623', '670519075117', NULL, 'Cheah Khiam Chew', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-29 10:36:28', '2019-10-29 10:46:07', NULL, '1967-05-19', 'Male', NULL, 'Cheah Khiam Chew', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('a55ebdd5812ff91eba1bffd86089834e', 'ksaras2525', 'ksaras@ksttraining.com', '415a74936c4dac78d6241f0801ed4cca1f7ae676d996cbf7ecb1b600a01f0c42', '56a4c1819ec48d12d416623a5cadc0f2ca0fab54', '0124012525', '590725076022', NULL, 'Saraswathy Thevi A/P N.Kaliappan', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 05:37:11', '2019-10-25 11:42:42', NULL, '1959-07-25', 'Female', NULL, 'Saraswathy Thevi A/P N.Kaliappan', NULL, NULL, NULL, NULL, 6, 500, 500, '0'),
('a7a9de836484b55e3bb06d568a722400', 'DCK5', NULL, '40b3f933b86eb0b1c390b8cb6543c5fc11b31266d7f06148bd95dee3debdec25', '2d1f957beb8ec0a6be783881e282253021327251', NULL, 'A005', NULL, NULL, '15d144e124b4c2a7b9474203e4890da3aa1112900653b44594fb9af28257ae19', '468713d10dae4569a444377107cf0ff069e5a7a7', NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-16 09:43:23', '2019-11-05 09:56:44', NULL, NULL, NULL, 'PUBLIC BANK BERHAD', NULL, '3216072811', NULL, NULL, NULL, 2, 300, 300, '0'),
('a9e242dc63ba85f2181e621b9085262b', 'chiayiting91', 'micha_91@live.com.my', '2e810f7b7f5de3b6b65a244773db79053375c870cd8d13ebab27e4c39a4cc352', '41d67a1a94ba61f44c6051c63611249bb36a03d0', '0165152751', '911208085726', NULL, 'Chia Yi Ting', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 15:57:45', '2019-10-20 15:59:33', NULL, '1991-12-08', 'Female', NULL, 'Chia Yi Ting', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('ac94e0e345ff3974f98e89820a36a101', 'kiewkiew', 'phaikkiew1963@gmail.com', '69b7ac2d9fdf15130246bfc486836014b1263827e819c18393ab23b7a43262b8', '3c5930b17b40d424f5fabdd26d8dd75d11ffaa6b', '0183912138', '631013076110', NULL, 'Tan Phaik Kiew', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 08:04:01', '2019-10-21 08:06:18', NULL, '1963-10-13', 'Female', NULL, 'Tan Phaik Kiew', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('ad2e06a7aa2b073ec0121dfe50fad30b', 'Evianlim', 'evianlim@gmail.com', 'b86680856384ae64e919451beff8d05832456e96a941508c77e2e275c55bc594', 'cc7787015204a583e16eb8b488d19d5abc05389e', '0184644482', '851231075604', NULL, 'Lim Lee Ling', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 04:19:09', '2019-10-24 04:25:36', NULL, '1985-12-31', 'Female', NULL, 'Lim Lee Ling', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('aec0b0afe43de960cc4db7b8c38cc4bf', '1121188', 'desmondlyl@gmail.com', '4c2bdacad238b2717c69765117e50c39b56b44ef437e7bf6edb71008ea01a526', '985b3bb440a377a1b5d603ff12759aff885552e7', '0164110239', '631225075419', NULL, 'Lee Yip Leong', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 06:12:16', '2019-10-27 13:51:58', NULL, '1963-12-25', 'Male', NULL, 'Lee Yip Leong', NULL, NULL, NULL, NULL, 2, 300, 300, '0'),
('afd9f21cc7935e7c1a8b6ef1032d4272', 'super01', 'mustaripro96@gmail.com', '00fd10cfe2167b8ff58b784496f22a1d98f79996aa5e3736358421596a80c91a', 'e60ef4e3b1e8101437105be026861877a48486de', 'mustaripro96@gmail.c', '123321', NULL, 'super01', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 09:41:06', '2019-11-07 10:07:11', '01155670433', NULL, 'Female', 'BANGKOK BANK BERHAD', 'super01', '777775555511117', 'ff', '1234', NULL, 1, 150, 150, '900'),
('b535adb1e7e856a84134d9cbdb969e8a', 'Pinqin0426', 'benkooi0426@gmail.com', '5a63c10c71d8760986d716bfdaa0d69ebc44c1a4b0d8b2c38330eec718c505c3', 'e1687c6a8e16a6f3da11ecad1f714c826d6f6d62', '0125487001', '000426070365', NULL, 'Kooi Pin Qin', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 08:46:28', '2019-10-20 08:49:09', NULL, '2000-04-26', 'Male', NULL, 'Kooi Pin Qin', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('b54ce303811b0a3ca98f21e95943f385', '10004292', 'klh888@gmail.com', 'e8955ee795be1ac84ae0d9b6773671c6529b097a0308f32e06f55adbd6af7695', 'b58698bc90bd062b25985b8b5844ddcf3df419a6', '0164204604', '630410075619', NULL, 'Khaw Look Hing', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 06:21:39', '2019-10-24 06:23:29', NULL, '1963-04-10', 'Male', NULL, 'Khaw Look Hing', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('b8db4691eb744b346aaa318375ffd9b3', 'adrianlsw', 'adriansw_lee@hotmail.com', 'fc6d0742fa2d8d48136441fed23eb1e4d38965d244c4681707749ff0648d5beb', '9b3abc73a8ed1b03ebb727837620e3d0f18e13b3', '0122176042', '880808565679', NULL, 'Adrian Lee Szion Wye', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 13:27:53', '2019-10-25 13:42:42', NULL, '1988-08-08', 'Male', NULL, 'Adrian Lee Szion Wye', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('bb97e777595704a90693bc6a0b0a7b92', 'Mike', 'mikejc1963@gmail.com', 'a351818e3f355e026c12b14e731414ad1d7c802353aed796274ebd7023799b50', 'ea7bbba0c9abdafef6a762a551f6c757dbf0e61a', '0165214333', '630407075845', NULL, 'Tan Jin Chye', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 16:32:55', '2019-10-20 17:07:22', NULL, '1963-04-07', 'Male', NULL, 'Tan Jin Chye', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('c067bb46a0986d11d4f4541f62adbd90', 'Kumaran', 'kumaranalwar22@gmail.com', 'e26b79a0ba6df8a64c8d3f48ab57aa717c87439f1f79387fb6746f2b109c73f0', '1e29f5dc41b03ebe6cbbe9fcea561aae0812710a', '0189797443', '721122075749', NULL, 'Kumaranalwar', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-05 03:14:29', '2019-11-05 03:24:24', NULL, '1972-11-22', 'Male', NULL, 'Kumaranalwar', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('c075645287643d2d5a0a590cbabe84bb', 'cschang', 'cs42.chang@gmail.com', '04309396e989e4dc2fc21220692e36a468e3d070990e8833d2dc19d93ba64a09', '9c128a317ccc233d5f13eb7173655593d6aafcf4', '0124780695', '540109075135', NULL, 'Chang Chee Seng', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 07:01:56', '2019-10-24 08:59:05', NULL, '1954-01-09', 'Male', NULL, 'Chang Chee Seng', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('c3aeddda183272740d139fea3c7f38c6', 'Thialan', 'thialan.ohhira@gmail.com', '2466c3b8e71be24b3ab9543f16d1ab79a8ee112bec0b89955b908284acf143b9', 'da43341fb9d4403716222c13ac06b3e40cf6d5f3', '0193362197', '550921086627', NULL, 'Thialan Letchumanan', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-05 03:19:13', '2019-11-05 03:39:37', NULL, '1955-09-21', 'Male', NULL, 'Thialan Letchumanan', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('c4bf057e62874f1d0ac08a976fbd89f8', 'Ozzyben', 'ozzyben89@yahoo.com', '84b4e1add31ae5537be0ff87ed3eaaf9bffa4ff0cb3b521ee7d572aaf57c8c38', '9f2ff8a6a5007516b77db9421e635caeb9616903', '0193398636', '761019087123', NULL, 'Oswald Benjamin A/L Balasingam', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-04 14:37:00', '2019-11-04 14:44:55', NULL, '1976-10-19', 'Male', NULL, 'Oswald Benjamin A/L Balasingam', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('c4eda98f92ee6a1129ca16d1a80aef3f', 'Jun96', 'jun96@gmail.com', 'a2170109c8b34abf77068c1803a05eb1a0a443a301e906bc12bc1b64efb55b0c', '17a9149e78a25e563994f0a0990270e6cc31fc77', '01111162188', '960712075560', NULL, 'Ong Jun Xin', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 05:13:02', '2019-10-24 05:16:51', NULL, '1996-07-12', 'Female', NULL, 'Ong Jun Xin', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('ca13c78fe860db1066698e82bd2a92cf', 'Praveen', 'praveen.huawei@gmail.com', 'c4decb410bbc1a9ff264a62d6d0966e6304ef9ede416b6375186e20724720f4c', '590b8558c129fc35dfde568ce9e03bab633fa14c', '0172122251', '860730335305', NULL, 'Praveen Gobynathan', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-04 14:26:26', '2019-11-07 11:31:29', NULL, '1986-07-30', 'Male', 'MAYBANK', 'Praveen Gobynathan', '112205118150', 'Mazda 3', '2010', NULL, 0, 0, 0, '0'),
('ca94f2b4e70886f87fa5a601c38ea66f', 'Haisong', 'limhaisong@gmail.com', '6b4ad671077ef31a8b362040406cdb77b0a8a217a3ac0540b6010df9d474281a', '34e9b2f27bef1481c2b2d46e83855f4967d42115', '0164295123', '510813075123', NULL, 'Lim Hai Song', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-29 10:46:07', '2019-10-29 10:51:27', NULL, '1951-08-13', 'Male', NULL, 'Lim Hai Song', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('cc6807bf135b2fc61c4c20f545cf0ddc', 'teestt', NULL, 'a6a54a6001b1a3fd1567ec5f839a2dd453ed90d07149b4ad44d336fce369d50b', 'bb8d5a00228448a63967f30024b0d5e9b3f075c0', NULL, '11', NULL, 'asdadsd', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 09:43:37', '2019-11-07 09:43:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('d118196bb40ba1b4d58201db356ff390', 'Zz', NULL, 'b1caa529591edacb458c9faf19a203278c1b851a3020a5181fcfe73e0b273d49', 'be40b725922a1fbf55fc64c655e21a701c486b21', NULL, '369', NULL, 'Zz', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-07 09:48:15', '2019-11-07 09:48:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('d42f85e1d6095ab0dddd68d48ffe6035', 'skng', 'skng@gmail.com', '150a31cc6f55945a94906b956c90b3d1b5814c04778aaf544f0c33555a10c316', 'edac5cb8744abac55acc16977e5efaed7a0e5b1d', '0124780695', '551106025224', NULL, 'Ng Siew Kah', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 08:59:05', '2019-10-24 09:01:40', NULL, '1955-11-06', 'Female', NULL, 'Ng Siew Kah', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('d430265249264f86e815b952588aad77', 'admindck', NULL, '48575e6559993aa35d98a893a62029f6c855f7b6d17930e4fe37bbfa48de0057', '5e29f9c1c505e3e6c954240573a4c4d15c5acf93', NULL, 'AdminDCK000', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, '2019-10-17 01:18:38', '2019-10-29 07:19:01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('d68160bb964940925c065915952f9004', 'Leena', 'leenabellina@gmail.com', '6664905ad7c782e1c21cc09bc47f295711be774abca1ac6733aa837eee8970ac', '2dd474a0328748c6dfb2c836e2c5bbdff5767fd6', '0125678763', '820824075106', NULL, 'Maslina Binti Ali Edilarie', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 09:33:36', '2019-10-24 09:47:23', NULL, '1982-08-24', 'Female', NULL, 'Maslina Binti Ali Edilarie', NULL, NULL, NULL, NULL, 2, 300, 300, '0'),
('d6ae09e2d7ddba5c4f3407296061d2d6', 'David Chua', 'chuacts@gmail.com', '04d895fab4c13d74b408a072b84dc691507af4735644015c0976fdc6b2942e74', '89de8f68626ec754b3dbb6cccf4c693b63afe4ef', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 1, 0, 0, 0, '2019-08-08 09:49:11', '2019-10-16 09:49:50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('da34e98545ad21d7adf4e9b942e1cdee', 'Victorz', 'bhteoh@gmail.com', '94db50e1489fe0910de7a58da700b3583c6e3ee8ef850b41f5d12f463ef2ad7c', '46c038454ad4e90699e0b7002078e08f0c569b4e', '0174134898', '660114075451', NULL, 'Teoh Beng Hooi', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-25 13:42:42', '2019-10-25 13:46:17', NULL, '1966-01-14', 'Male', NULL, 'Teoh Beng Hooi', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('db234ab854958eefcb2f1cfdbf064725', 'vincentpeh', 'vincentpeh@gmail.com', 'ac3f0aaa76e30ca0094c1037abceacbf7b5d0b470e56170ed13dd1f6f4780711', '0f6b6c5c5b827e3a52749ec842a53188acaa5858', '01136641286', '961114065201', NULL, 'Peh Vin Cent', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 11:33:43', '2019-10-21 11:36:13', NULL, '1996-11-14', 'Male', NULL, 'Peh Vin Cent', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('de549b1d8c5fa5aa6e619f8a65f5e710', 'meifang', 'meifang@gmail.com', 'b79463bc9e8071969f1bb8aaa03d161021f0211121857c7d28e7a7fc0beb217a', '945d217f5f5d44d03ece01f37012d20a944477a6', '0165046724', '020206070396', NULL, 'Ng Mei Fang', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 11:25:03', '2019-10-21 11:28:11', NULL, '2002-02-06', 'Female', NULL, 'Ng Mei Fang', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('e0ba42f66f1e63ad790906447b5df998', 'skliew9', 'skliew9@yahoo.com', 'ad34cb2b54874eaaa8119b7d369fda282ce428548aead31a5ce26a3f5a3709a7', '24a7f0261ce5aa38639d117142fc25115122e0a0', '0174433202', '570215105729', NULL, 'Liew Seng Kee', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-27 13:58:38', '2019-10-27 14:00:37', NULL, '1957-02-15', 'Male', NULL, 'Liew Seng Kee', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('e516b51931fb08b4612433fb51005c56', 'meiyong', 'ah_how2006@yahoo.com', 'cd216dd555d2364f02a0e2ac58e30c5a47d870e51be97f63688594150c8f68a2', 'ea923e6726580d9b53adfb8d06a258e972657a86', '0164222586', '480329025250', NULL, 'Ng Mei Yong', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 07:07:11', '2019-10-20 07:09:34', NULL, '1948-03-29', 'Female', NULL, 'Ng Mei Yong', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('e57eba868ce93e56f3f31196f0e8caa0', 'Anson', 'jackylim3180@yahoo.com', '60a345a11e53aaaaed77262fbf52e684b22568251b181ea348e34c8cbe1527c9', 'df19419a8b0fe1691af7e1dda3437c3ab60183a2', '0124541524', '910707075229', NULL, 'Ng Wen Jun', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 15:49:37', '2019-10-20 15:51:59', NULL, '1991-07-07', 'Male', NULL, 'Ng Wen Jun', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('e5a4922a94ef5db728b20f1d2fc0c011', 'Lee', 'chengenglee8@gmail.com', '3b93900aa829d661d91652bb6de95cbb9c11975508a9cbc90dea41095a927954', '83723fba73dec100946a5e98cb63faacd385ffb0', '0164221822', '670517075009', NULL, 'Lee Cheng Eng', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-28 10:56:13', '2019-10-28 10:58:13', NULL, '1967-05-17', 'Male', NULL, 'Lee Cheng Eng', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('e7f97247d154c2344218034b94b466c1', 'ktchew', 'ktchewww75@gmail.com', 'f591bf7796d4dcf5f94494e6d10c7f0a43de9bf105905714c0cf049857004a62', '4cd3dfa1dc00370e6fd349245b8c00ce0a0409d5', '0164839252', '750619075149', NULL, 'Chew Kong Tat', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-20 16:26:08', '2019-10-20 16:28:00', NULL, '1975-06-19', 'Male', NULL, 'Chew Kong Tat', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('e7ffbcf4843ef7f63be105adcaa91e43', 'hebaodan123', '1309166503@qq.com', '1c68e9fe742f122a91154245041e13a6aaca81384cbe1509cdbacd367185077c', '965aabff16a9fc599381e166e2c5070d3f12196f', '0166495148', 'EB7156070', NULL, 'He Lirong', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-21 12:01:57', '2019-10-21 12:04:58', NULL, NULL, 'Male', NULL, 'He Lirong', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('ec9abee186248eab79c4e257a1a8c4c8', 'Abi', 'abi@gmail.com', 'bc50f1b77fee9573d53e971505883cf492d0366f002dae8ceb7fa46cd277816d', 'ce3a2aec40b62cef24ff5cbe210eba39ec842165', NULL, '771013025043', NULL, 'Abitullah Bin Daunlat', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 09:44:15', '2019-10-24 09:54:07', NULL, '1977-10-13', 'Male', NULL, 'Abitullah Bin Danlat', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('f15107a4ec3d75eebc75e1b912dc8d63', 'DCK4', NULL, '4ea583a09e5241f5a00153f9333e5932c24e1433fa789b2b1b1d01e8f13fe3ff', 'fbc30c8031212930163859c2bed8caf938981f1d', NULL, 'A004', NULL, NULL, '7885e1a62d0797ca35bb201c9e1634ae322e0fe13b6a524c210abde7f3cd3283', '1ce69f50d1bef063a1c9d2166eb545714310bbe3', NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-16 09:43:16', '2019-11-05 09:56:11', NULL, NULL, NULL, 'PUBLIC BANK BERHAD', NULL, '3216072811', NULL, NULL, NULL, 1, 150, 150, '0'),
('f178369e2183b1d97704df861eced6f7', 'Spike07', 'spikeselvanayagam@gmail.com', 'eef0e76816e787ef789fd0b7e645566877b76505f7356168796f8040a7e1815d', '4ffe365f291bd0c22ce937ed2c287378e44a436e', '0167052556', '620607105085', NULL, 'Selvanagam A/L Nadarajah', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-11-04 14:32:09', '2019-11-04 15:49:32', NULL, '1962-06-07', 'Male', NULL, 'Selvanagam A/L Nadarajah', NULL, NULL, NULL, NULL, 1, 150, 150, '0'),
('f27cdb430d061807a0e4f4993d7af9df', 'DCK6', NULL, 'f575e846d3ee1e52a81b3449424273766a2a66886ffba3a8795345098c08c230', '0d30c0476815b6f3ff27e967a0761bacd6236d22', NULL, 'A006', NULL, NULL, '9d8ee4b9baca0bb0244edef56ebb815567fb8224a825640d9e4310a024b56ebe', 'c5e7fde599ba74acbe4a5a5be779e8fcec1fd11a', NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-16 09:43:31', '2019-11-05 09:57:49', NULL, NULL, NULL, 'PUBLIC BANK BERHAD', NULL, '3216072811', NULL, NULL, NULL, 1, 150, 150, '0'),
('f5b67cde35a74ecb0646b7ef4f90cdfa', 'Quak', 'luxhousingsystem@yahoo.com', '8d006d01980647a22f588d532493920fef2b562f4c2e079caf89ce86ad9ff01b', '9716055d134f6ad37178367b6d9d883a06ac1251', '0194378883', '690501075627', NULL, 'Quak Chew Seng', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 06:41:07', '2019-10-24 06:43:28', NULL, '1969-05-01', 'Male', NULL, 'Quak Chew Seng', NULL, NULL, NULL, NULL, 0, 0, 0, '0'),
('f9b60b8d0eb3c59b2ab4b36e3ddbcd61', 'DCK3', NULL, '7f68ad5add2df64748c1e6f77f75221464c9ec8e88c5f586fbe4cbd43a6eb3ed', '66b1d8bed6e7e31941624634e9a9055233fb5f0c', NULL, 'A003', NULL, NULL, '32996d1ff9a10b1392342e7eb3d4d2c87d7e239c206b451f91f87ab620b71800', '43f172a29195a4d8a723055dd8a01ff101049446', NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-16 09:43:07', '2019-11-05 09:55:16', NULL, NULL, NULL, 'PUBLIC BANK BERHAD', NULL, '3216072811', NULL, NULL, NULL, 1, 150, 150, '0'),
('fa06dd8c1aaeb0e644dfdf6ab9538cac', 'Jalil', 'jalil@gmail.com', '34d67d13d24c5401473c265021c93cb9d99445f5a4f2727c88a642fb6a7be66f', 'b5e9908a71317d09d8220c3c90306c20a170935d', '0196835422', '571108105349', NULL, 'Jalaludin Bin Mohd Mokhtar', NULL, NULL, NULL, 1, 0, 1, 1, 0, 0, 1, '2019-10-24 09:47:23', '2019-10-24 09:50:02', NULL, '1957-11-08', 'Male', NULL, 'Jalaludin Bin Mohd Moktar', NULL, NULL, NULL, NULL, 0, 0, 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `uid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `withdrawal_number` int(255) NOT NULL,
  `withdrawal_status` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `contact` int(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `amount` int(255) DEFAULT NULL,
  `final_amount` int(255) DEFAULT NULL,
  `withdrawal_method` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `withdrawal_amount` int(255) DEFAULT NULL,
  `withdrawal_note` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `bank_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `acc_number` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `point` int(255) DEFAULT NULL,
  `owner` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `receipt` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `withdrawal`
--

INSERT INTO `withdrawal` (`uid`, `withdrawal_number`, `withdrawal_status`, `contact`, `date_created`, `date_updated`, `amount`, `final_amount`, `withdrawal_method`, `withdrawal_amount`, `withdrawal_note`, `username`, `bank_name`, `acc_number`, `point`, `owner`, `receipt`, `name`) VALUES
('2402a716d533bd79d3d764be9c1d645a', 22, 'PENDING', 184000170, '2019-11-05 02:27:09', '2019-11-05 02:27:09', 390, 400, NULL, NULL, NULL, 'Lim Eng Hong', 'MAYBANK', '157175178662', NULL, 'Windlim', NULL, NULL),
('83ccbb881e19774f5d7a085a6003e245', 23, 'PENDING', 1122334455, '2019-11-05 09:49:21', '2019-11-05 09:49:21', 490, 2850, NULL, NULL, NULL, 'michaelwong', 'BANK OF TOKYO-MITSUBISHI UFJ (M) BHD', '11223355225522', NULL, 'michaelwong', NULL, NULL),
('83ccbb881e19774f5d7a085a6003e245', 24, 'PENDING', 1122334455, '2019-11-05 09:49:58', '2019-11-05 09:49:58', 490, 2350, NULL, NULL, NULL, 'michaelwong', 'BANK OF TOKYO-MITSUBISHI UFJ (M) BHD', '11223355225522', NULL, 'michaelwong', NULL, NULL),
('0c396af354a0bda95e6114a77ecc4cfa', 25, 'PENDING', 145212020, '2019-11-06 04:29:44', '2019-11-06 04:29:44', 490, 500, NULL, NULL, NULL, 'Lim Shuenn Chi', 'AMBANK BERHAD', '820070001653', NULL, 'shuennchi2020', NULL, NULL),
('8ca2402c6c3f7643a93af5eeea795cdd', 26, 'PENDING', 164732263, '2019-11-06 04:33:07', '2019-11-06 04:33:07', 390, 400, NULL, NULL, NULL, 'Loh Siew Heong', 'MAYBANK', '557241061451', NULL, 'Josieloh', NULL, NULL),
('197c08e85e66915dc9aeca5759cce4ec', 27, 'PENDING', 164222586, '2019-11-07 05:20:45', '2019-11-07 05:20:45', 490, 500, NULL, NULL, NULL, 'Chow Chean How', 'HONG LEONG BANK BERHAD', '11900006309', NULL, 'cchow', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_privileges`
--
ALTER TABLE `admin_privileges`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `adminUidAdminPrivileges_relateTo_userId` (`admin_uid`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`announce_id`);

--
-- Indexes for table `bonus`
--
ALTER TABLE `bonus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdBonus_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdBonus_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdBonus_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `cash_to_point`
--
ALTER TABLE `cash_to_point`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cashToPointUid_to_user` (`uid`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactionTypeIdGroupCommission_relateTo_transactionTypeId` (`transaction_type_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `money_type`
--
ALTER TABLE `money_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productIdTransactionHistory_relateTo_productId` (`product_id`),
  ADD KEY `orderIdProductOrders_relateTo_orderId` (`order_id`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `referralIdReferralHistory_relateTo_userId` (`referral_id`),
  ADD KEY `referrerIdReferralHistory_relateTo_userId` (`referrer_id`),
  ADD KEY `topReferrerIdReferralHistory_relateTo_userId` (`top_referrer_id`);

--
-- Indexes for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uidTransactionHistory_relateTo_userId` (`uid`),
  ADD KEY `targetUidTransactionHistory_relateTo_userId` (`target_uid`),
  ADD KEY `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` (`transaction_type_id`),
  ADD KEY `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` (`money_type_id`),
  ADD KEY `sourceTransactionIdTransactionHistory_relateTo_self` (`source_transaction_id`),
  ADD KEY `orderIdTransactionHistory_relateTo_orderId` (`order_id`);

--
-- Indexes for table `transaction_type`
--
ALTER TABLE `transaction_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_point`
--
ALTER TABLE `transfer_point`
  ADD PRIMARY KEY (`id`),
  ADD KEY `SendUid` (`send_uid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `ic_no` (`ic_no`),
  ADD KEY `countryIdUser_relateTo_countryId` (`country_id`);

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`withdrawal_number`),
  ADD KEY `withdrawalId_to_user` (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_privileges`
--
ALTER TABLE `admin_privileges`
  MODIFY `admin_id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `announce_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `bonus`
--
ALTER TABLE `bonus`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=229;

--
-- AUTO_INCREMENT for table `cash_to_point`
--
ALTER TABLE `cash_to_point`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;

--
-- AUTO_INCREMENT for table `group_commission`
--
ALTER TABLE `group_commission`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `pid` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `money_type`
--
ALTER TABLE `money_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_orders`
--
ALTER TABLE `product_orders`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `referral_history`
--
ALTER TABLE `referral_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=229;

--
-- AUTO_INCREMENT for table `transaction_history`
--
ALTER TABLE `transaction_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transaction_type`
--
ALTER TABLE `transaction_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transfer_point`
--
ALTER TABLE `transfer_point`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `withdrawal_number` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_privileges`
--
ALTER TABLE `admin_privileges`
  ADD CONSTRAINT `adminUidAdminPrivileges_relateTo_userId` FOREIGN KEY (`admin_uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `bonus`
--
ALTER TABLE `bonus`
  ADD CONSTRAINT `referralIdBonus_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdBonus_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdBonus_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `cash_to_point`
--
ALTER TABLE `cash_to_point`
  ADD CONSTRAINT `cashToPointUid_to_user` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`);

--
-- Constraints for table `group_commission`
--
ALTER TABLE `group_commission`
  ADD CONSTRAINT `transactionTypeIdGroupCommission_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `product_orders`
--
ALTER TABLE `product_orders`
  ADD CONSTRAINT `orderIdProductOrders_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `productIdTransactionHistory_relateTo_productId` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `referral_history`
--
ALTER TABLE `referral_history`
  ADD CONSTRAINT `referralIdReferralHistory_relateTo_userId` FOREIGN KEY (`referral_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `referrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `topReferrerIdReferralHistory_relateTo_userId` FOREIGN KEY (`top_referrer_id`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD CONSTRAINT `moneyTypeIdTransactionHistory_relateTo_moneyTypeId` FOREIGN KEY (`money_type_id`) REFERENCES `money_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orderIdTransactionHistory_relateTo_orderId` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `sourceTransactionIdTransactionHistory_relateTo_self` FOREIGN KEY (`source_transaction_id`) REFERENCES `transaction_history` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `targetUidTransactionHistory_relateTo_userId` FOREIGN KEY (`target_uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE,
  ADD CONSTRAINT `transactionTypeIdTransactionHistory_relateTo_transactionTypeId` FOREIGN KEY (`transaction_type_id`) REFERENCES `transaction_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `uidTransactionHistory_relateTo_userId` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `transfer_point`
--
ALTER TABLE `transfer_point`
  ADD CONSTRAINT `SendUid` FOREIGN KEY (`send_uid`) REFERENCES `user` (`uid`) ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `countryIdUser_relateTo_countryId` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD CONSTRAINT `withdrawalId_to_user` FOREIGN KEY (`uid`) REFERENCES `user` (`uid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
