<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Withdrawal.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/Rate.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';


$uid = $_SESSION['uid'];


// echo 'aaa';
// echo $_POST['shipping_method'];
// echo '<br/>';
// echo 'bbb';
// echo $_POST['order_id'];
$conn = connDB();
$withdrawRate = getRate($conn," WHERE id = ? ",array("id"),array(1),"i");
$rateDetails = $withdrawRate[0];

$withdrawalRows = getWithdrawReq($conn," WHERE withdrawal_number = ? ",array("withdrawal_number"),array($_POST["withdrawal_number"]),"s");
$withdrawalDetails = $withdrawalRows[0];

$newuid = $withdrawalDetails->getUid();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($newuid),"s");
$userDetails = $userRows[0];

date_default_timezone_set("Asia/Kuala_Lumpur");
$date = date("Y-m-d H:i:s");
// echo $date;

if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $newuid = $withdrawalDetails->getUid();

        $withdrawalStatus = rewrite($_POST["withdrawal_status"]);//accepted
        $withdrawalStatus1 = rewrite($_POST["withdrawal_status1"]);//rejected
        $withdrawalMethod = rewrite($_POST["withdrawal_method"]);
        $withdrawalAmount = rewrite($_POST["withdrawal_accepted"]);//admin keyin amount
        $withdrawalNote = rewrite($_POST["withdrawal_note"]);//admin note to user
        $withdrawalReceipt = rewrite($_POST["uploadfile"]);
        $withdrawalNumber = rewrite($_POST["withdrawal_number"]);

        $userMoney = $userDetails -> getWithdrawAmount();
        $username = $withdrawalDetails->getUsername();
        $charges = $rateDetails -> getChargesWithdraw();
        $MoneyAfterCharge = $withdrawAmount - $charges;
        //$MoneyAfterCharge = $withdrawAmount + $charges;

        $name = $_FILES['file']['name'];
        $target_dir = "../images/";
        $target_file = $target_dir . basename($_FILES["file"]["name"]);
        //$image_text = mysqli_real_escape_string($conn, $_POST['number']);


        // Select file type
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Valid file extensions
        $extensions_arr = array("jpg","jpeg","png","gif");

        if( in_array($imageFileType,$extensions_arr) ){


         move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$name);
}


        if ($MoneyAfterCharge < $userMoney && $userMoney >= 0) {
          if ($withdrawalStatus) {
            $userMoneyTotal = $userMoney - $withdrawalAmount - $charges; //cant get 0 but can get -ve
            // $userMoneyTotal = $userMoney - $withdrawalAmount;
            // $withdrawalAmount-= $charges;
            if(isset($_POST['withdrawal_number']))
            {
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";

                if(!$userMoneyTotal)
                {   $money = 0;
                    array_push($tableName,"final_amount");
                    array_push($tableValue,$money);
                    $stringType .=  "s";
                }

                array_push($tableValue,$newuid);
                $stringType .=  "s";
                $withdrawUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($withdrawUpdated)
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../adminWithdrawal.php?type=1');
                }
                else
                {
                    echo "fail";

                }
            }
            else
            {
                echo "dunno";

            }
          }else {
            echo "string";
          }

            // $userMoney = $userMoney - $withdrawalAmount - $charges;
        }else {
          echo "error";
        }

        if(isset($_POST['withdrawal_number']))
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            // //echo "save to database";
            if($userMoneyTotal)
            {
                array_push($tableName,"final_amount");
                array_push($tableValue,$userMoneyTotal);
                $stringType .=  "i";
            }
            if($withdrawalStatus)
            {
                array_push($tableName,"withdrawal_status");
                array_push($tableValue,$withdrawalStatus);
                $stringType .=  "s";
            }if($withdrawalStatus1)
            {
                array_push($tableName,"withdrawal_status");
                array_push($tableValue,$withdrawalStatus1);
                $stringType .=  "s";
            }
            if($withdrawalMethod)
            {
                array_push($tableName,"withdrawal_method");
                array_push($tableValue,$withdrawalMethod);
                $stringType .=  "s";
            }
            if($withdrawalAmount)
            {
                array_push($tableName,"withdrawal_amount");
                array_push($tableValue,$withdrawalAmount);
                $stringType .=  "i";
            }
            if($withdrawalNote)
            {
                array_push($tableName,"withdrawal_note");
                array_push($tableValue,$withdrawalNote);
                $stringType .=  "s";
            }

            if($name)
            {

              move_uploaded_file($_FILES['file']['tmp_name'],$target_file);

                array_push($tableName,"receipt");
                array_push($tableValue,$name);
                $stringType .=  "s";
            //}
          }
                //declaring variables


            array_push($tableValue,$withdrawalNumber);
            $stringType .=  "i";
            $withdrawUpdated = updateDynamicData($conn,"withdrawal"," WHERE withdrawal_number = ? ",$tableName,$tableValue,$stringType);

            if($withdrawUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminWithdrawal.php?type=1');
            }
            else
            {
                echo "fail";

            }
        }
        else
        {
            echo "dunno";

        }

        if(isset($_POST['withdrawal_number']))
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($userMoneyTotal)
            {
                array_push($tableName,"final_amount");
                array_push($tableValue,$userMoneyTotal);
                $stringType .=  "s";
            }
            // if(!$userMoneyTotal)
            // {   $money = 0;
            //     array_push($tableName,"final_amount");
            //     array_push($tableValue,$money);
            //     $stringType .=  "s";
            // }

            array_push($tableValue,$newuid);
            $stringType .=  "s";
            $withdrawUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($withdrawUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminWithdrawal.php?type=1');
            }
            else
            {
                echo "fail";

            }
        }
        else
        {
            echo "dunno";

        }
        // if($_SERVER['REQUEST_METHOD'] == 'POST')
        //     {
        //         $conn = connDB();

                //$newuid = $withdrawalDetails->getUid();

              //  $withdrawalStatus = rewrite($_POST["file"]);//accepted
              //  $withdrawal = rewrite($_POST["file"]);//accepted




      //  }


        //  if (isset($_POST['withdrawal_number'])) {
        //   if (getimagesize($_FILES['image']['tmp_name'])) {
        //     echo "plss select an image";
        //   }else {
        //     $image = addslashes($_FILES['image']['tmp_name']);
        //       $name = addslashes($_FILES['image']['name']);
        //       $image = file_get_contents($image);
        //       $image = base64_encode($image);
        //       saveimage($name,$image);
        //   }
        // } function saveimage(){
        //   $sql = "insert into withdrawal (name,receipt) values ('$name','$image')";
        //   $result = mysqli_query($sql,$conn);
        //   if($result){
        //   echo "success";
        // }else {
        //   echo "fail";
        // } }

    }
else
{
    header('Location: ../oilxag/adminWithdrawal.php');
}

?>
