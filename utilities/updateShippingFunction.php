<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $shipping_method = rewrite($_POST["shipping_method"]);
        $tracking_number = rewrite($_POST["tracking_number"]);
        $shipping_date = rewrite($_POST["shipping_date"]);
        $shipping_status = rewrite($_POST["shipping_status"]);
        $order_id = rewrite($_POST["order_id"]);

        if(isset($_POST['order_id']))
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($shipping_method)
            {
                array_push($tableName,"shipping_method");
                array_push($tableValue,$shipping_method);
                $stringType .=  "s";
            }     
            if($tracking_number)
            {
                array_push($tableName,"tracking_number");
                array_push($tableValue,$tracking_number);
                $stringType .=  "s";
            }     
            if($shipping_date)
            {
                array_push($tableName,"shipping_date");
                array_push($tableValue,$shipping_date);
                $stringType .=  "s";
            }  
            if($shipping_status)
            {
                array_push($tableName,"shipping_status");
                array_push($tableValue,$shipping_status);
                $stringType .=  "s";
            }  

            array_push($tableValue,$order_id);
            $stringType .=  "s";
            $orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            
            if($orderUpdated)
            {
                // $_SESSION['messageType'] = 1;
                // header('Location: ../adminShipping.php?type=1');
                header('Location: ../adminShipping.php');
            }
            else
            {

            }
        }
        else
        {

        }

    }
else 
{

}

$conn->close();
?>