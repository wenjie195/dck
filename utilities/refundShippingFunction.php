<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Product.php';
require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

// echo 'aaa';
// echo $_POST['shipping_method'];
// echo '<br/>';
// echo 'bbb';
// echo $_POST['order_id'];

date_default_timezone_set("Asia/Kuala_Lumpur");
$date = date("Y-m-d H:i:s"); 
// echo $date;

if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $refund_method = rewrite($_POST["insert_refund_method"]);
        $refund_amount = rewrite($_POST["insert_refund_amount"]);
        $refund_note = rewrite($_POST["insert_refund_note"]);
        $refund_reason = rewrite($_POST["insert_refund_reason"]);
        $shipping_date = $date;
        $shipping_status = rewrite($_POST["shipping_status"]);
        // $shipping_method = rewrite($_POST["shipping_method"]);
        // $tracking_number = rewrite($_POST["tracking_number"]);
        $order_id = rewrite($_POST["order_id"]);

        // echo $order_id."<br>";
        // echo $shipping_method."<br>";

        if(isset($_POST['order_id']))
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($refund_method)
            {
                array_push($tableName,"refund_method");
                array_push($tableValue,$refund_method);
                $stringType .=  "s";
            }     
            if($refund_amount)
            {
                array_push($tableName,"refund_amount");
                array_push($tableValue,$refund_amount);
                $stringType .=  "s";
            }  
            if($refund_note)
            {
                array_push($tableName,"refund_note");
                array_push($tableValue,$refund_note);
                $stringType .=  "s";
            }  
            if($refund_reason)
            {
                array_push($tableName,"refund_reason");
                array_push($tableValue,$refund_reason);
                $stringType .=  "s";
            }  
            if($shipping_date)
            {
                array_push($tableName,"shipping_date");
                array_push($tableValue,$shipping_date);
                $stringType .=  "s";
            }  
            if($shipping_status)
            {
                array_push($tableName,"shipping_status");
                array_push($tableValue,$shipping_status);
                $stringType .=  "s";
            }  
            // if($shipping_method)
            // {
            //     array_push($tableName,"shipping_method");
            //     array_push($tableValue,$shipping_method);
            //     $stringType .=  "s";
            // }  
            // if($tracking_number)
            // {
            //     array_push($tableName,"tracking_number");
            //     array_push($tableValue,$tracking_number);
            //     $stringType .=  "s";
            // }  
          
            array_push($tableValue,$order_id);
            $stringType .=  "s";
            $orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            
            if($orderUpdated)
            {
                // echo "<br>";
                // echo $_POST['order_id']."<br>";
                // echo $refund_method."<br>";
                // echo $refund_amount."<br>";
                // echo $refund_note."<br>";
                // echo $refund_reason."<br>";
                // echo $shipping_date."<br>";
                // echo $shipping_status."<br>";
                // echo $shipping_method."<br>";
                // echo $tracking_number."<br>";
                // echo "success";
                $_SESSION['messageType'] = 1;
                header('Location: ../adminShipping.php?type=1');
            }
            else
            {
                // echo "fail";
                $_SESSION['messageType'] = 1;
                header('Location: ../adminShipping.php?type=2');
            }
        }
        else
        {
            // echo "dunno";
            $_SESSION['messageType'] = 1;
            header('Location: ../adminShipping.php?type=3');
        }

    }
else 
{
    header('Location: ../adminShipping.php');
}

?>