<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/withdrawal.php';
require_once dirname(__FILE__) . '/../classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/mailerFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

$conn = connDB();

$uid = $_SESSION['uid'];
$getWho = getWholeDownlineTree($conn, $_SESSION['uid'],false);
    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
        $userDetails = $userRows[0];
        $sendName = $userDetails->getUsername();


        // $update_money = $userDetails -> getWithdrawAmount();
        $currentUsernameSearch = $_POST["username"];
        $existingIC = $userDetails -> getIcNo();
        $currentPoint = $userDetails -> getUserPoint();
        $transferAmount = $_POST["transferAmount"];

        $referralHistorys = getReferralHistory($conn,"WHERE referral_name = ? ",array("referral_name"),array($currentUsernameSearch),"s");
        //$userRows11 = getUser($conn," WHERE uid = ? ",array("uid"),array($referralHistory[0]->getReferralId()),"s");
        if ($referralHistorys) {



        $getTopReferrerReceiver = $referralHistorys[0]->getTopReferrerId();
        $getReferralIdReceiver = $referralHistorys[0]->getReferralId();
        $userReceiverDetails = getUser($conn,"WHERE uid = ? ",array("uid"),array($getReferralIdReceiver),"s");
         if ($userReceiverDetails) {
          $userPoint = $userReceiverDetails[0]->getUserPoint();
        }

        $referralHistoryII = getReferralHistory($conn,"WHERE referral_id = ? ",array("referral_id"),array($uid),"s");
        if ($referralHistoryII) {
          $getTopReferrerSender = $referralHistoryII[0]->getTopReferrerId();
          $getNameSender = $referralHistoryII[0]->getReferralName();
        }
        $referralHistoryAdmin = getReferralHistory($conn,"WHERE top_referrer_id = ? ",array("top_referrer_id"),array($uid),"s");
        if ($referralHistoryAdmin) {
            $getUidAdmin = $referralHistoryAdmin[0]->getTopReferrerId();
        }

        $currentEnterEpin = $_POST["withdraw_epin_convert"];
        $dbEpin =  $userDetails->getEpin();
        $dbSaltEpin =  $userDetails->getSaltEpin();
        $newEpin_hashed = hash('sha256',$currentEnterEpin);
        $status = 'RECEIVED';
        $newEpin_hashed_salt = hash('sha256', $dbSaltEpin .   $newEpin_hashed);

        if ($transferAmount <= $currentPoint &&  $currentPoint > 0) {
          if ($dbEpin == $newEpin_hashed_salt) {
              if ($getTopReferrerSender == $getTopReferrerReceiver || $getUidAdmin==$getTopReferrerReceiver) {
                if ($getNameSender != $currentUsernameSearch) {
                $pointNow = $currentPoint - $transferAmount;
                $pointBeingTransfer = $userPoint + $transferAmount;
                if (NewWithdraw($conn,$uid,$sendName,$transferAmount,$currentUsernameSearch,$getReferralIdReceiver,$status)) {
                          $_SESSION['messageType'] = 1;
                          header('Location: ../profile.php?type=1');
                      }
                      else
                      {
                          $_SESSION['messageType'] = 2;
                          header('Location: ../profile.php?type=1');
                      }
                $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
                if(!$user)
                {
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($pointNow)
                    {
                        array_push($tableName,"point");
                        array_push($tableValue,$pointNow);
                        $stringType .=  "i";
                    }
                    if(!$pointNow)
                    {   $point = 0;
                        array_push($tableName,"point");
                        array_push($tableValue,$point);
                        $stringType .=  "i";
                    }
                    if($finalMoney)
                    {
                        array_push($tableName,"final_amount");
                        array_push($tableValue,$finalMoney);
                        $stringType .=  "i";
                    }

                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($passwordUpdated)
                    {
                        // echo "success";
                        // $_SESSION['messageType'] = 1;
                        // header('Location: ../wallet.php?type=1');
                        // header('Location: ../editprofile.php?type=3');
                    }
                    else
                    {
                        // echo "fail";
                        // $_SESSION['messageType'] = 3;
                        // header('Location: ../wallet.php?type=3');
                    }
                }
                else
                {
                    //echo "";
                    // $_SESSION['messageType'] = 2;
                    // header('Location: ../wallet.php?type=2');
                }
                $_SESSION['messageType'] = 1;
                header('Location: ../profile.php?type=8');

              }else {
                 $_SESSION['messageType'] = 2;
                header('Location: ../profile.php?type=3');
                // header('Location: ../wallet.php?type=7');
              }

              }else {
                 $_SESSION['messageType'] = 2;
                header('Location: ../profile.php?type=2');
                // header('Location: ../wallet.php?type=7');
              }


          }else {
            $_SESSION['messageType'] = 1;
            header('Location: ../profile.php?type=3');
          }

        }else {
          $_SESSION['messageType'] = 1;
          header('Location: ../profile.php?type=9');
        }

      }else {
        $_SESSION['messageType'] = 2;
       header('Location: ../profile.php?type=4');
      }


        // $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
        // if(!$user)
        // {
        //     $tableName = array();
        //     $tableValue =  array();
        //     $stringType =  "";
        //     //echo "save to database";
        //     if($pointNow)
        //     {
        //         array_push($tableName,"point");
        //         array_push($tableValue,$pointNow);
        //         $stringType .=  "i";
        //     }
        //     // if(!$pointNow)
        //     // {   $point = 0;
        //     //     array_push($tableName,"point");
        //     //     array_push($tableValue,$point);
        //     //     $stringType .=  "i";
        //     // }
        //     if($finalMoney)
        //     {
        //         array_push($tableName,"final_amount");
        //         array_push($tableValue,$finalMoney);
        //         $stringType .=  "i";
        //     }
        //
        //     array_push($tableValue,$uid);
        //     $stringType .=  "s";
        //     $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        //     if($passwordUpdated)
        //     {
        //         // echo "success";
        //         // $_SESSION['messageType'] = 1;
        //         // header('Location: ../wallet.php?type=1');
        //         // header('Location: ../editprofile.php?type=3');
        //     }
        //     else
        //     {
        //         // echo "fail";
        //         // $_SESSION['messageType'] = 3;
        //         // header('Location: ../wallet.php?type=3');
        //     }
        // }
        // else
        // {
        //     //echo "";
        //     // $_SESSION['messageType'] = 2;
        //     // header('Location: ../wallet.php?type=2');
        // }

        $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
        if(!$user)
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($pointBeingTransfer)
            {
                array_push($tableName,"point");
                array_push($tableValue,$pointBeingTransfer);
                $stringType .=  "s";
            }


            array_push($tableValue,$currentUsernameSearch);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE username = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                // echo "success";
                // $_SESSION['messageType'] = 1;
                // header('Location: ../wallet.php?type=1');
                // header('Location: ../editprofile.php?type=3');
            }
            else
            {
                // echo "fail";
                // $_SESSION['messageType'] = 3;
                // header('Location: ../wallet.php?type=3');
            }
        }
        else
        {
            //echo "";
            // $_SESSION['messageType'] = 2;
            // header('Location: ../wallet.php?type=2');
        }

      //}

    }
else
{
    //header('Location: ../editprofile.php');
    header('Location: ../index.php');
}

function NewWithdraw($conn,$uid,$sendName,$transferAmount,$currentUsernameSearch,$getReferralIdReceiver,$status){

     if(insertDynamicData($conn,"transfer_point",array("send_uid","send_name","amount","receive_name","receive_uid","status"),
         array($uid,$sendName,$transferAmount,$currentUsernameSearch,$getReferralIdReceiver,$status),"ssisss") === null){

          return false;
     }else{
     }

    return true;
 }
?>
