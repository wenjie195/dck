<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/GroupCommission.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $register_uid = md5(uniqid());
    $register_username = rewrite($_POST['register_username']);



    $register_email_user = null;
    if(isset($_POST['register_email_user']))
    {
         $register_email_user = rewrite($_POST['register_email_user']);
         $register_email_user = filter_var($register_email_user, FILTER_SANITIZE_EMAIL);
    }

    $register_ic_no = rewrite($_POST['register_ic_no']);
    $register_password = $_POST['register_password'];
    $register_password_validation = strlen($register_password);
    $register_retype_password = $_POST['register_retype_password'];
    // $register_email_referrer = $_POST['register_email_referrer'];

    // $register_email_referrer = null;
    // if(isset($_POST['register_email_user']))
    // {
    //      $register_email_referrer = rewrite($_POST['register_email_referrer']);
    //      $register_email_referrer = filter_var($register_email_referrer, FILTER_SANITIZE_EMAIL);
    // }

    $register_ic_referrer = $_POST['register_ic_referrer'];

}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

//$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/addReferee.php" />
    <meta property="og:title" content="Add Referee | DCK Supreme" />
    <title>Add Referee | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/referee.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<div class="yellow-body padding-from-menu same-padding">

	<h1 class="username">ADD NEW REFEREE</h1>
  <h3>Point : <?php echo $userRefereeLimit = $userDetails->getUserPoint(); ?>Pts &emsp; Bonus : RM<?php echo $userRefereeLimit = $userDetails->getBonus(); ?></h3>


<?php

$uid = $_SESSION['uid'];

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$userRefereeLimit = $userDetails->getUserPoint();

  if ($userRefereeLimit >=300) {


?>
    <form  class="edit-profile-div2" onsubmit="return registerNewMemberAccountValidation();" action="utilities/addNewRefereeFunction.php" method="POST">
        <table class="edit-profile-table password-table">
        	<tr class="profile-tr">
            	<td class="">Username</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input class="clean edit-profile-input" type="text" placeholder="Username" id="register_username" name="register_username">
                </td>
            </tr>
            <tr class="profile-tr">
              	<td class="">Fullname</td>
                  <td class="profile-td2">:</td>
                  <td class="profile-td3">
                      <input class="clean edit-profile-input" type="text" placeholder="Fullname" id="register_fullname" name="register_fullname">
                  </td>
              </tr>
        	<tr class="profile-tr">
            	<td>IC No.</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required class="clean edit-profile-input" type="text" placeholder="IC Number" id="register_ic_no" name="register_ic_no">
                </td>
            </tr>
            <tr class="profile-tr">
            	<td>Email</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input class="clean edit-profile-input" type="text" placeholder="Email" id="register_email_user" name="register_email_user">
                </td>
            </tr>
        	<!-- <tr class="profile-tr">
            	<td>Referrer's Email</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input class="clean edit-profile-input" type="email" placeholder="Referrer`s Email" id="register_email_referrer" name="register_email_referrer"
                        value="<?php //echo $userDetails->getEmail();?>">
                </td>
            </tr>     -->

            <tr class="profile-tr">
            	<td>Referrer's Username.</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3">
                    <input required class="clean edit-profile-input" type="text" placeholder="Referrer`s IC No." id="register_username_referrer" name="register_username_referrer"
                        value="<?php echo $userDetails->getUsername();?>">
                </td>
            </tr>

          </table>

        <input required class="login-input password-input clean" type="hidden" id="register_password" name="register_password" value="123321">
        <input required class="login-input password-input clean" type="hidden" id="register_retype_password" name="register_retype_password" value="123321">

        <div class="clear"></div>

        <button class="confirm-btn text-center white-text clean black-button"name="refereeButton">ADD REFEREE</button>
    </form>
  <?php }else { ?>
    <center>  <div class= "width100 oveflow">
        <div class="width20">
            <div class="white50div">
        <?php echo "*You Can't Register New Referee Because Your Current Point Is Less Than 300Pts." ?>
      </div>
  </div>
</div></center>
<?php  } ?>
</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this username ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Register Successfully!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "The Referrer Point Below 300Pts !";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "Wrong email format.";
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
if(isset($_GET['promptError']))
{
    $messageType = null;

    if($_GET['promptError'] == 1)
    {
        $messageType = "Error registering new account.The account already exist";
    }
    else if($_GET['promptError'] == 2)
    {
        $messageType = "Error assigning referral relationship. Please register again.";
    }
    echo '
    <script>
        putNoticeJavascript("Notice !! ","'.$messageType.'");
    </script>
    ';
}
?>

<script>
  function checkIfVariableIsNullOrEmptyString(field,isValidate)
  {
    if(field == null || field == "" || field.length == 0)
    {
      isValidate += 1;
      return isValidate;
    }
    else
    {
      return isValidate;
    }
  }
  function registerNewMemberAccountValidation()
  {
    // First Level Validation
    let isValidatedAndCanProceedToNextLevel = 0;

    let register_username = $('#register_username').val();
    let register_ic_no = $('#register_ic_no').val();
    let register_password = $('#register_password').val();
    let register_retype_password = $('#register_retype_password').val();
    // let register_email_referrer = $('#register_email_referrer').val();

    // console.log('Initial Counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_username,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_ic_no,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);


    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_password,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    isValidatedAndCanProceedToNextLevel = checkIfVariableIsNullOrEmptyString(register_retype_password,isValidatedAndCanProceedToNextLevel);
    // console.log('counter = '+isValidatedAndCanProceedToNextLevel);

    // Second Level Validation
    if(isValidatedAndCanProceedToNextLevel == 0)
    {
      // alert('can continue');
      if(register_password.length >= 6)
      {
        if(register_password == register_retype_password)
        {
        //   if(register_email_referrer == null || register_email_referrer == "" || register_email_referrer.length == 0)
        //   {

        //   }
        //   else
        //   {
        //     if(emailIsValid (register_email_referrer))
        //     {

        //     }
        //     else
        //     {
        //       alert('Referrer`s email is not valid ! Please try again ! ');
        //       event.preventDefault();
        //     }
        //   }
        }
        else
        {
          alert('Password does not match ! Please try again ! ');
          event.preventDefault();
        }
      }
      else
      {
        alert('Password must be more than 5 ! Please try again ! ');
        event.preventDefault();
      }
    }
    else
    {
      alert('Please enter all fields required ! ');
      event.preventDefault();
    }bbbbbbb
  }
  function emailIsValid (email)
  {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
  }
</script>

</body>
</html>
