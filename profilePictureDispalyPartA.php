<style>
.imagetj
{
  opacity: 1;
  display: block;
  width: 98%;
  height:98%;
  transition: .5s ease;
  backface-visibility: hidden;
}
.edit-profile-div-smalltj
{
    margin-left: auto;
    margin-right: auto;	
    width: 200px;
	height:200px;
}
</style>

<div class="left-profile-div"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": ".9"}'>
        <?php
        if($userProPic == "")
        {
        ?>
            <div class="profile-div">
                <a href="uploadProfilePicture.php">
                    <button class="edit-profile-pic-btn text-center white-text clean"><img src="img/camera.png" class="camera-icon" alt="Update Profile Picture" title="Update Profile Picture"> UPDATE</button>
                </a>
            </div>
            <p class="edit-profile-p text-center width100">
                <a href="editProfile.php" class="profile-a"><img src="img/edit-profile.png" class="edit-profile-icon" alt="Edit Profile" title="Edit Profile"> Edit Profile</a>
            </p>  
        <?php
        }
        else
        { ?>
           <div class="edit-profile-div-smalltj text-center white-text clean">
                <a href="uploadProfilePicture.php">
                    <img src="upload/<?php echo $userProPic->getFilename();?>" style="width:100%" class="imagetj camera-icon overwrite-imagetj" alt="Update Profile Picture" title="Update Profile Picture">
                	<button class="edit-profile-pic-btn text-center white-text clean ow-update-pic-btn"><img src="img/camera.png" class="camera-icon" alt="Update Profile Picture" title="Update Profile Picture"> UPDATE</button>
                </a>
            </div>
            <p class="edit-profile-p text-center width100">
                <a href="editProfile.php" class="profile-a"><img src="img/edit-profile.png" class="edit-profile-icon" alt="Edit Profile" title="Edit Profile"> Edit Profile</a>
            </p> 
        <?php
        }
        ?>   
</div>