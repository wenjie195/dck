<?php


require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';

$conn = connDB();
$post = array();
$start_date = date("Y-m-d");
$end_date = date("Y-m-d");
$referrername = "";
$referralname = "";
$registerDownlineNo  = "";
$amount  = "";

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //todo create table for transaction_history with at least a column for quantity(in order table),product_id(in order table), order_id, status (others can refer to btcw's), target_uid, trigger_transaction_id
    //todo create table for order and product_order
    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

        echo " this: $productId total: $quantity";
    }
}


if (isset($_GET["start_date"]) && isset($_GET["end_date"]) && isset($_GET["referrer_name"]) && isset($_GET["referral_name"]))
{
	$start_date = $_GET["start_date"];
	$end_date = $_GET["end_date"];
	$referrername = $_GET["referrer_name"];
  $referralname = $_GET["referral_name"];
  $registerDownlineNo = $_GET["register_downline_no"];
  $amount = $_GET["amount"];
	$post = $_GET;
}

$products = getProduct($conn);
$list = GetList($post, $conn);

//echo json_encode($list);//exit;

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($_SESSION['uid']),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function GetList($post, $conn)
{
	$sql = "SELECT referrer_name,referral_name,date_created, ";
	$sql .= "register_downline_no,amount ";
//$sql .= "CASE WHEN record_type_id = '1' THEN 'Direct Sponsor' ";
//	$sql .= "WHEN record_type_id = '2' THEN 'Sales Commission' ";
//	$sql .= "ELSE 'Annual Bonus' END AS 'type' ";
	$sql .= "FROM bonus ";
	$sql .= "WHERE id > '0' ";

  if (isset($post["reset"])) {
    if (isset($post["start_date"]) && strlen($post["start_date"]) < 0)
    {
      $sql .= "AND DATE(date_created) >= '" . $post["start_date"] . "' ";
    }

    if (isset($post["end_date"]) && strlen($post["end_date"]) < 0)
    {
      $sql .= "AND DATE(date_created) <= '" . $post["end_date"] . "' ";
    }

    if (isset($post["referrer_name"]) && strlen($post["referrer_name"]) < 0)
  	{
      $sql .= "AND referrer_name LIKE '%" . $post["referrer_name"] . "%' ";
  	}
    if (isset($post["referral_name"]) && strlen($post["referral_name"]) < 0)
  	{
      $sql .= "AND referral_name LIKE '%" . $post["referral_name"] . "%' ";
  	}
    if (isset($post["register_downline_no"]) && strlen($post["register_downline_no"]) < 0)
  	{
      $sql .= "AND register_downline_no LIKE '%" . $post["register_downline_no"] . "%' ";
  	}
    if (isset($post["amount"]) && strlen($post["amount"]) < 0)
  	{
      $sql .= "AND amount LIKE '%" . $post["amount"] . "%' ";
  	}


}else {


	if (isset($post["start_date"]))
	{
		$sql .= "AND DATE(date_created) >= '" . $post["start_date"] . "' ";
	}

	if (isset($post["end_date"]))
	{
		$sql .= "AND DATE(date_created) <= '" . $post["end_date"] . "' ";
	}

	if (isset($post["referrer_name"]) && strlen($post["referrer_name"]) > 0)
	{
    $sql .= "AND referrer_name LIKE '%" . $post["referrer_name"] . "%' ";
	}
  if (isset($post["referral_name"]) && strlen($post["referral_name"]) > 0)
	{
    $sql .= "AND referral_name LIKE '%" . $post["referral_name"] . "%' ";
	}
  if (isset($post["register_downline_no"]) && strlen($post["register_downline_no"]) > 0)
	{
    $sql .= "AND register_downline_no LIKE '%" . $post["register_downline_no"] . "%' ";
	}
  if (isset($post["amount"]) && strlen($post["amount"]) > 0)
	{
    $sql .= "AND amount LIKE '%" . $post["amount"] . "%' ";
	}
}
	$sql .= "ORDER BY date_created ASC ";
	//echo $sql;exit;

	$result = $conn->query($sql);
	$output = array();

	if ($result->num_rows > 0)
	{
		// output data of each row
		while($row = $result->fetch_assoc())
		{
			$output[] = $row;
		}
	}

	return $output;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/adminBonusReport.php" />
    <meta property="og:title" content="Member | DCK Supreme" />
    <title>Bonus Report | DCK Supreme</title>
    <meta property="og:description" content="DCK® Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK® Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/adminBonusReport.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php //include 'header-admin.php'; ?>
<?php include 'header-sherry.php'; ?>


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!--<form method="POST">-->
    <?php
//    if(isset($_POST['product-list-quantity-input'])){
//        createProductList($products,$_POST['product-list-quantity-input']);
//    }else{
//        createProductList($products);
//    }

    ?>
<!--    <button type="submit" name="addToCartButton" id="addToCartButton" >Add to cart</button>-->
<!--</form>-->
<div class="yellow-body padding-from-menu same-padding">
	<h1 class="h1-title h1-before-border shipping-h1">Bonus Report</h1>
    <div class="clear"></div>
	<div class="search-container0 payout-search">
		<form action="adminBonusReport.php" type="post">
            <div class="shipping-input clean smaller-text2">
                <p>Name</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" name="referrer_name" placeholder="Name"  value="<?php //echo $referrername; ?>">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p>Downline Name</p>
                <input class="shipping-input2 clean normal-input same-height-with-date" type="text" name="referral_name" placeholder="Downline Name"  value="<?php //echo $referralname; ?>">
            </div>
            <div class="shipping-input clean smaller-text2">
                <p>Start Date</p>
                <input class="shipping-input2 clean normal-input" name="start_date" type="date" value="<?php echo $start_date; ?>">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>End Date</p>
                <input class="shipping-input2 clean normal-input" name="end_date" type="date" value="<?php echo $end_date; ?>">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>Registered Downline Number</p>
                <input class="shipping-input2 clean normal-input" name="register_downline_no" type="number" placeholder="Registered Downline Number" value="<?php //echo $registerDownlineNo; ?>">
            </div>
            <div class="shipping-input clean smaller-text2 middle-shipping-div second-shipping">
                <p>Bonus</p>
                <input class="shipping-input2 clean normal-input" name="amount" type="number" placeholder="Bonus" value="<?php //echo $amount; ?>">
            </div>



            <button type="submit" class="clean black-button shipping-search-btn second-shipping same-height-with-date2">Search</button>
            <button type="submit" name="reset" class="clean black-button shipping-search-btn second-shipping same-height-with-date2">Reset</button>
			</form>
    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
    	<div class="overflow-scroll-div">
            <table class="shipping-table">
              <thead>
                  <tr>
                      <th>NO.</th>
                      <th>NAME</th>
                      <th>REGISTERED DOWNLINE NUMBER</th>
                      <th>DOWNLINE NAME</th>
                      <th>JOINED DATE</th>
                      <th>BONUS</th>
                  </tr>
              </thead>
                <tbody>
								<?php if ($list): ?>
                  <?php $index=0; ?>
									<?php foreach ($list AS $ls):
                     $index++;?>
										<tr>
											<td><?php echo $index; ?></td>
											<td><?php echo $ls["referrer_name"]; ?></td>
                      <td><?php echo $ls["register_downline_no"]; ?></td>
                      <td><?php echo $ls["referral_name"]; ?></td>
                      <td><?php echo $ls["date_created"]; ?></td>
											<td><?php echo $ls["amount"]; ?></td>
										</tr>
									<?php endforeach; ?>
								<?php else: ?>
									<tr>
										<td colspan="5">No result</td>
								</tr>
								<?php endif; ?>
                  </tbody>
            </table>
        </div>
    </div>
    <div class="clear"></div>
</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'jsAdmin.php'; ?>

</body>
</html>
