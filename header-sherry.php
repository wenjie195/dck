    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="left-logo-div float-left hidden-logo-padding">
                <a href="index.php"><img src="img/dck.png" class="logo-img" alt="DCK" title="DCK"></a>
            </div>
            <?php 
            if(isset($_SESSION['usertype_level']) && $_SESSION['usertype_level'] == 1)
            {
                ?>
                 <div class="middle-menu-div float-left hide">
                    <a href="index.php#about" class="menu-padding white-to-yellow">About</a>
                    <a href="index.php#products" class="menu-padding white-to-yellow">Products</a>
                    <a href="index.php#contact" class="menu-padding white-to-yellow">Contact Us</a>
                    <a href="faq.php" class="menu-padding white-to-yellow">FAQ</a>
                </div>

                <?php 
                if(isset($_SESSION['uid']))
                {
                    // echo count($_SESSION);
                    ?>
                    <div class="right-menu-div float-right" id="after-login-menu">
                        <a href="cart.php"  class="menu-padding hide"><img src="img/cart.png" class="cart-img" alt="cart" title="cart"><div class="dot-div"></div></a>
                        <a href="announcement.php"  class="menu-padding"><img src="img/notifcation.png" class="cart-img user-notification" alt="Notification" title="Notification"><div class="dot-div hide"></div></a>
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/white-user.png" class="cart-img" alt="profile" title="profile"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a> 
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a"><img src="img/user-black.png" class="cart-img dropdown-img2" alt="Profile" title="Profile"> My Profile</a></p>
                                <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a"><img src="img/edit-profile.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> Edit Profile</a></p>
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a"><img src="img/password-d.png" class="cart-img dropdown-img2" alt="Password" title="Password"> Edit Password</a></p>
                                <p class="dropdown-p"><a href="addePin.php"  class="menu-padding dropdown-a"><img src="img/e-pin.png" class="cart-img dropdown-img2" alt="E-Pin" title="E-Pin"> E-Pin</a></p>
                                <p class="dropdown-p"><a href="referee.php" class="menu-padding dropdown-a"><img src="img/user-group.png" class="cart-img dropdown-img2" alt="My Referee" title="My Referee"> My Referee</a></p>
                                <p class="dropdown-p"><a href="addReferee.php" class="menu-padding dropdown-a"><img src="img/add-referee.png" class="cart-img dropdown-img2" alt="Add Referee" title="Add Referee"> Add Referee</a></p>
                            </div>  
                        </div>    
                        <!-- <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/cart.png" class="cart-img" alt="Purchase" title="Purchase"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a> 
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="product.php"  class="menu-padding dropdown-a"><img src="img/bottle.png" class="cart-img dropdown-img2" alt="Purchase Product" title="Purchase Product"> Purchase Product</a></p>
                                <p class="dropdown-p"><a href="purchaseHistory.php"  class="menu-padding dropdown-a"><img src="img/verify-payment.png" class="cart-img dropdown-img2" alt="Edit Profile" title="Edit Profile"> Product History</a></p>
                            </div>  
                        </div>     -->
                        <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/document.png" class="cart-img" alt="Report" title="Report"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a> 
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><b>Bonus Report</b></p>
                                <p class="dropdown-p"><a href="bonusReport.php"  class="menu-padding dropdown-a"><img src="img/bonus.png" class="cart-img dropdown-img2" alt="Sponsor Bonus" title="Sponsor Bonus"> Sponsor Bonus</a></p>
                                <!-- <p class="dropdown-p"><a href="leadershipBonus.php"  class="menu-padding dropdown-a"><img src="img/leader.png" class="cart-img dropdown-img2" alt="Leadership Matching Bonus" title="Leadership Matching Bonus"> Leadership M. Bonus</a></p>
                            	<p class="dropdown-p"><a href="annualBonus.php"  class="menu-padding dropdown-a"><img src="img/annual-bonus.png" class="cart-img dropdown-img2" alt="Annual Bonus" title="Annual Bonus"> Annual Bonus</a></p> -->
								<p class="dropdown-p"><b> </b></p>
                                <!-- <p class="dropdown-p"><b>Cash Report</b></p>
                                <p class="dropdown-p"><a href="withdrawalReport.php"  class="menu-padding dropdown-a"><img src="img/withdraw1.png" class="cart-img dropdown-img2" alt="Withdrawal Report" title="Withdrawal Report"> Withdrawal Report</a></p>
                                <p class="dropdown-p"><a href="cashToPointReport.php"  class="menu-padding dropdown-a"><img src="img/points.png" class="cart-img dropdown-img2" alt="Convert Cash to Point" title="Convert Cash to Point"> Convert to Point</a></p>
								<p class="dropdown-p"><b> </b></p>                                 -->
                                <p class="dropdown-p"><b>Point Report</b></p>
                                <p class="dropdown-p"><a href="transferPointReport.php"  class="menu-padding dropdown-a"><img src="img/transfer-report.png" class="cart-img dropdown-img2" alt="Transfer Point Report" title="Transfer Point Report"> Transfer Point Report</a></p>
                                <!-- <p class="dropdown-p"><a href="signUpReport.php"  class="menu-padding dropdown-a"><img src="img/register.png" class="cart-img dropdown-img2" alt="Sign Up Report" title="Sign Up Report"> Sign Up Report</a></p> -->

                            </div> 
                            
                        </div>  
                        <!-- <div class="dropdown hover1">
                            <a  class="menu-padding"><img src="img/language-white.png" class="cart-img" alt="Change Language" title="Change Language"></a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a> 
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href=""  class="menu-padding dropdown-a"><img src="img/english.png" class="cart-img dropdown-img2" alt="English" title="English"> English</a></p>
                                <p class="dropdown-p"><a href=""  class="menu-padding dropdown-a"><img src="img/chinese.png" class="cart-img dropdown-img2" alt="中文" title="中文"> 中文</a></p>
                            </div> 
                        </div>                                                 -->
                        <a href="logout.php"  class="menu-padding"><img src="img/logout-white.png" class="cart-img" alt="Logout" title="Logout"></a> 
                        			<!-- Codrops top bar -->

					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
								  <li><a href="announcement.php">Announcement</a></li>
                                  <li class="title-li">Profile</li>
                                  <li><a href="profile.php" class="mini-li">My Profile</a></li>
                                  <li><a href="editProfile.php"  class="mini-li">Edit Profile</a></li>
                                  <li><a href="editPassword.php"  class="mini-li">Edit Password</a></li>
                                  <li><a href="addePin.php" class="mini-li">E-Pin</a></li>
                                  <li class="title-li">My Team</li>
                                  <li><a href="referee.php" class="mini-li">My Referee</a></li>
                                  <li><a href="addReferee.php" class="mini-li">Add Referee</a></li>
                                  <!-- <li class="title-li">Purchase</li>
                                  <li><a href="product.php" class="mini-li">Purchase Product</a></li>
                                  <li><a href="purchaseHistory.php" class="mini-li">Purchase History</a></li> -->
                                  <li class="title-li">Bonus Report</li>
                                  <li><a href="bonusReport.php" class="mini-li">Sponsor Bonus</a></li>
                                  <!-- <li><a href="leadershipBonus.php" class="mini-li">Leadership M. Bonus</a></li>                                  
                                  <li><a href="annualBonus.php" class="mini-li">Annual Bonus</a></li> -->
                                  <!-- <li class="title-li">Cash Report</li>
                                  <li><a href="withdrawalReport.php" class="mini-li">Withdrawal Report</a></li>
                                  <li><a href="cashToPointReport.php" class="mini-li">Convert to Point</a></li>                                   -->
                                  <li class="title-li">Point Report</li>
                                  <li><a href="transferPointReport.php" class="mini-li">Transfer Point Report</a></li>
                                  <!-- <li><a href="signUpReport.php" class="mini-li">Sign Up Report</a></li>  -->
                                  <!-- <li class="title-li">Language</li>
                                  <li><a class="mini-li">English</a></li>
                                  <li><a class="mini-li">中文</a></li>                                                                   -->
                                  <li><a href="logout.php">Logout</a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->

                        
                        
                        
                                                   
    

                    </div>
                    <?php
                }
                else 
                {
                    ?>
                    <div class="right-menu-div float-right">
                    	<a href="faq.php" class="menu-padding white-to-yellow faq-padding">FAQ</a>
                        <a  class="menu-padding white-to-yellow div-div open-login">Login</a>
                        <a class="open-menu"><img src="img/white-menu.png" class="menu-icon"></a>
                    </div>
                    <?php
                }
            }
            else 
            {
                if(isset($_SESSION['uid']))
                {
                ?>
                <div class="middle-menu-div right-menu-div1">
                    <a href="adminDashboard.php" class="menu-padding white-to-yellow">Dashboard</a>
                    <a href="adminPayment.php" class="menu-padding white-to-yellow">Payment</a>
                    <a href="adminShipping.php" class="menu-padding white-to-yellow">Shipping</a>
                    <a href="adminWithdrawal.php" class="menu-padding white-to-yellow">Withdrawal</a>
                    <!-- <a href="adminSales.php" class="menu-padding white-to-yellow">Sales</a> -->
                    <a href="adminProduct.php" class="menu-padding white-to-yellow">Product</a>
                    <!-- <a href="adminPayout.php" class="menu-padding white-to-yellow">Payout</a> -->
                    <a href="adminMember.php" class="menu-padding white-to-yellow">Member</a>
                    <!-- <a href="adminAdmin.php" class="menu-padding white-to-yellow">Admin</a> -->
                    <div class="dropdown hover1">
                            <a  class="menu-padding">
                                <img src="img/settings.png" class="cart-img hover1a" alt="Settings" title="Settings">
                                <img src="img/settings-yellow.png" class="cart-img hover1b" alt="Settings" title="Settings">
                            </a>
                            <a  class="menu-padding dropdown-btn">
                                <img src="img/dropdown.png" class="dropdown-img hover1a" alt="dropdown" title="dropdown">
                                <img src="img/dropdown-yellow.png" class="dropdown-img hover1b" alt="dropdown" title="dropdown">
                            </a> 
                            <div class="dropdown-content ywllo-dropdown-content">
                                <p class="dropdown-p"><a href="adminProfile.php"  class="menu-padding dropdown-a">Edit Profile</a></p>
                                <p class="dropdown-p"><a href="adminPassword.php"  class="menu-padding dropdown-a hide">Edit Password</a></p>
                                <p class="dropdown-p"><a href="adminBonusReport.php"  class="menu-padding dropdown-a">Bonus Report</a></p>
                                <!-- <p class="dropdown-p"><a href="adminRate.php" class="menu-padding dropdown-a">Rate</a></p> -->
                                <p class="dropdown-p"><a href="announcement.php" class="menu-padding dropdown-a">Announcement</a></p>
                                <!-- <p class="dropdown-p"><a href="adminReason.php" class="menu-padding dropdown-a">Reason</a></p> -->
                                <p class="dropdown-p"><a href="logout.php" class="menu-padding dropdown-a">Logout</a></p>
                            </div>  
                    </div>
                    </div>
                    <div class="float-right right-menu-div admin-mobile">
                          	<div id="dl-menu" class="dl-menuwrapper admin-mobile-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
 								  <li><a href="adminDashboard.php">Dashboard</a></li>
                                  <li><a href="adminPayment.php">Payment</a></li>
                                  <li><a href="adminShipping.php">Shipping</a></li>
                                  <li><a href="adminWithdrawal.php" >Withdrawal</a></li>
                                  <!-- <li><a href="adminSales.php">Sales</a></li> -->
                                  <li><a href="adminProduct.php">Product</a></li>
                                  <!-- <li><a href="adminPayout.php">Payout</a></li> -->
                                  <li><a href="adminMember.php">Member</a></li>
                                  <!-- <li><a href="adminAdmin.php">Admin</a></li> -->
                                  <li><a href="adminProfile.php">Edit Profile</a></li>
                                  <li><a href="adminPassword.php">Edit Password</a></li>
                                  <li><a href="adminBonusReport.php">Bonus Report</a></li>
                                  <!-- <li><a href="adminRate.php">Rate</a></li> -->
                                  <li><a href="announcement.php">Announcement</a></li>                                  
                                  <!-- <li><a href="adminReason.php">Reason</a></li> -->
                                  <li><a href="logout.php">Logout</a></li>
                                </ul>
							</div><!-- /dl-menuwrapper -->
                     </div>     
                                    
                
                <?php
                 }
                 else 
                 {
                    ?>
                     <div class="right-menu-div float-right">
                        <!-- <a  class="menu-padding white-to-yellow login-a open-login">Login</a>
                        <a  class="menu-padding white-to-yellow div-div open-register">Sign Up</a>             -->
						<a href="faq.php" class="menu-padding white-to-yellow faq-padding">FAQ</a>
                        <a  class="menu-padding white-to-yellow div-div open-login">Login</a>            
 					<div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                                  <li><a href="faq.php">FAQ</a></li>
                                  <li><a class="open-login">Login</a></li>
						</ul>
					</div><!-- /dl-menuwrapper -->  
                    </div>
                    <?php
                 }
            }
            ?>
        </div>
    
    </header>