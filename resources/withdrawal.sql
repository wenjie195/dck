-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 08, 2019 at 07:23 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_oilxag`
--

-- --------------------------------------------------------

--
-- Table structure for table `withdrawal`
--

CREATE TABLE `withdrawal` (
  `uid` varchar(255) NOT NULL,
  `withdrawal_number` int(255) NOT NULL,
  `withdrawal_status` text NOT NULL,
  `contact` int(255) NOT NULL,
  `date_created` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `date_updated` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `amount` int(255) NOT NULL,
  `final_amount` int(255) NOT NULL,
  `withdrawal_method` varchar(255) NOT NULL,
  `withdrawal_amount` int(255) DEFAULT NULL,
  `withdrawal_note` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `acc_number` int(255) NOT NULL,
  `point` int(255) NOT NULL,
  `owner` varchar(255) NOT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `name` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `withdrawal`
--

INSERT INTO `withdrawal` (`uid`, `withdrawal_number`, `withdrawal_status`, `contact`, `date_created`, `date_updated`, `amount`, `final_amount`, `withdrawal_method`, `withdrawal_amount`, `withdrawal_note`, `username`, `bank_name`, `acc_number`, `point`, `owner`, `receipt`, `name`) VALUES
('3e072a0ea0c47d4429499685c4464afd', 153, 'ACCEPTED', 65432, '2019-10-31 09:34:51.934040', '2019-10-31 09:34:22.095919', 240, -250, 'iPay88', 240, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 154, 'REJECTED', 65432, '2019-10-31 09:42:45.445983', '2019-10-31 09:36:03.827863', 490, 1000, 'iPay88', 490, 'HYEEEE', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 155, 'ACCEPTED', 65432, '2019-10-31 09:44:46.165990', '2019-10-31 09:44:21.320098', 490, -400, 'iPay88', 490, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 156, 'REJECTED', 65432, '2019-10-31 09:47:48.845392', '2019-10-31 09:47:05.377298', 490, 1000, 'iPay88', 490, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 157, 'REJECTED', 65432, '2019-10-31 09:48:59.087976', '2019-10-31 09:48:18.865330', 90, 500, 'iPay88', 90, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 158, 'REJECTED', 65432, '2019-10-31 09:52:34.166227', '2019-10-31 09:51:22.699244', 490, 1000, 'iPay88', 490, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 159, 'REJECTED', 65432, '2019-10-31 09:58:20.086064', '2019-10-31 09:57:09.179117', 490, 1000, 'iPay88', 490, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 160, 'REJECTED', 65432, '2019-10-31 10:01:22.542955', '2019-10-31 09:59:08.339620', 190, 1000, 'iPay88', 190, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 161, 'ACCEPTED', 65432, '2019-10-31 10:01:40.171941', '2019-10-31 09:59:17.328946', 90, 100, 'iPay88', 90, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 162, 'ACCEPTED', 65432, '2019-10-31 10:01:51.460766', '2019-10-31 09:59:29.143099', 90, 700, 'iPay88', 90, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 163, 'ACCEPTED', 65432, '2019-10-31 10:14:31.930718', '2019-10-31 10:14:21.439526', 490, 1000, 'iPay88', 490, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 164, 'REJECTED', 65432, '2019-10-31 10:16:48.477623', '2019-10-31 10:16:12.435845', 90, 500, 'iPay88', 90, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 165, 'PENDING', 65432, '2019-11-07 07:00:45.314991', '2019-10-31 10:16:22.718129', 90, 400, 'iPay88', 90, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 166, 'PENDING', 65432, '2019-11-07 07:00:42.651046', '2019-11-01 01:06:23.801364', 90, 100, 'iPay88', 90, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 167, 'PENDING', 65432, '2019-11-07 07:00:40.257775', '2019-11-01 01:08:42.835776', 90, 100, 'iPay88', 90, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 168, 'PENDING', 65432, '2019-11-07 07:00:37.661254', '2019-11-01 01:15:36.636845', 990, 1000, 'iPay88', 990, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 169, 'PENDING', 65432, '2019-11-07 07:00:34.863088', '2019-11-01 01:16:08.843712', 990, 1000, 'iPay88', 990, '', 'ATLET008', 'CIMB', 123, 0, 'atlet008', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 170, 'PENDING', 65432, '2019-11-07 07:00:32.340790', '2019-11-05 08:38:00.015326', 490, 1000, 'iPay88', 490, '', 'MICHAELWONG', 'BANGKOK BANK BERHAD', 2147483647, 0, 'atlet008', 'backup.PNG', ''),
('2c993aba1e050e81f459693c5dee2093', 171, 'PENDING', 456, '2019-11-05 08:45:45.714131', '2019-11-05 08:45:45.714131', 490, 500, '', NULL, '', 'Fssfsf', 'AFFIN BANK BERHAD', 2147483, 0, 'vv', NULL, ''),
('3e072a0ea0c47d4429499685c4464afd', 172, 'PENDING', 65432, '2019-11-06 04:35:30.259757', '2019-11-06 04:35:30.259757', 490, 500, '', NULL, '', '', 'CIMB BANK BERHAD', 234567, 0, 'atlet008', NULL, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `withdrawal`
--
ALTER TABLE `withdrawal`
  ADD PRIMARY KEY (`withdrawal_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `withdrawal`
--
ALTER TABLE `withdrawal`
  MODIFY `withdrawal_number` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=173;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
