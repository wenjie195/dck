<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/profile.php" />
    <meta property="og:title" content="Profile | DCK Supreme" />
    <title>Profile | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces 
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration, 
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/profile.php" />
    <?php include 'css.php'; ?>    
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<!-- <div class="yellow-body padding-from-menu same-padding"> -->
<div class="yellow-body padding-from-menu same-padding"  id="firefly">

<!--function to display profile picture-->
<?php include 'profilePictureDispalyPartA.php'; ?>

    <div class="right-profile-div">

        <div class="profile-tab width100"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2"}'>
        	<a href="#" class="profile-tab-a active-tab-a">ABOUT</a>            
            <a href="referee.php" class="profile-tab-a">MY REFEREE</a>
        </div>

        <div class="left-profile-div-user"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2"}'>
            <h1 class="username smaller-font"> <?php echo $userDetails->getUsername();?> <span class="unbold"> &nbsp; &nbsp;| &nbsp; &nbsp; Agent &nbsp; &nbsp;| &nbsp; &nbsp; Maintenance Date: 30/11/2019 &nbsp; &nbsp;</span></h1>
        </div>
        <div class="right-profile-div-transfer hide">    
        	
            <img src="img/star.png" class="voucher-icon" alt="Transfer Points" title="Transfer Points">
        	<img src="img/coupon.png" class="voucher-icon voucher1" alt="Transfer Voucher" title="Transfer Voucher">
        </div>   
        <div class="clear"></div>


        <div class="width100 oveflow wallet-big-div"   data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "2"}'>
        	<div class="width50 first-50">
            	<div class="white50div">
                    <img src="img/cash2.png" class="cash-icon">
                    <h2>Cash</h2>
                    <p>RM<?php echo $userDetails -> getWithdrawAmount(); ?></p>   
                </div>
                <div class="open-withdraw withdraw-button gold-hover-div">Withdraw</div>
                <div class="open-convert convert-button gold-hover-div">Convert to Points</div>         
            </div>

            <div class="width50 second-width50 wallet-big-div"   data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "2"}'>
				<div class="white50div">
                    <img src="img/points.png" class="cash-icon">
                    <h2>Point</h2>
                    <p><?php echo $userDetails -> getUserPoint(); ?></p>
                </div>  

                <div class="open-transfer withdraw-button gold-hover-div">Transfer</div>
                <a href="addReferee.php" class="open-convert convert-button gold-hover-div color-black display-block">Add Referee</a> 
          

            </div>            
        </div>    
        <div class="clear"></div>    
        <div class="divider-yes"></div>
        <div class="clear"></div>
        <div class="width100 oveflow hide">
         	<div class="three-point-div">
            	<h2>Sponsor Bonus</h2>
                <p>100</p>
            </div>
         	<div class="three-point-div middle-point-div">
            	<h2>Leadership Matching Bonus</h2>
                <p>100</p>
            </div>
         	<div class="three-point-div">
            	<h2>Annual Bonus</h2>
                <p>100</p>
            </div>                        
         </div>
        <div class="clear"></div>


        <h2 class="profile-title" data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "3"}'>BASIC INFORMATION</h2>
        <table class="profile-table"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "3.5"}'>
        	<tr class="profile-tr">
            	<td class="profile-td1">IC Number</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getIcNo();?></td>
            </tr>
        	<tr class="profile-tr">
            	<td class="profile-td1">Full Name</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getFullname();?></td>
            </tr>            
        	<tr class="profile-tr">
            	<td class="profile-td1">Username</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getUsername();?></td>
            </tr>
        	<tr class="profile-tr">
            	<td class="profile-td1">Birthday</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getBirthday();?></td>
            </tr>
        	<tr class="profile-tr">
            	<td class="profile-td1">Gender</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getGender();?></td>
            </tr>                                    
        </table>


        <h2 class="profile-title"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "4"}'>BANK INFORMATION</h2>
        <table class="profile-table"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "4.5"}'>
        	<tr class="profile-tr">
            	<td class="profile-td1">Bank</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getBankName();?></td>
            </tr>       
        	<tr class="profile-tr">
            	<td class="profile-td1">Acc. Holder Name</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getBankAccountHolder();?></td>
            </tr>     
            <tr class="profile-tr">
            	<td class="profile-td1">Account No.</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getBankAccountNo();?></td>
            </tr>                     
        </table>

        <h2 class="profile-title"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "5"}'>VEHICLES INFORMATION</h2>
        <table class="profile-table"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "5.5"}'>
        	<tr class="profile-tr">
            	<td class="profile-td1">Car Model</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getCarModel();?></td>
            </tr>       
        	<tr class="profile-tr">
            	<td class="profile-td1">Year</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getCarYear();?></td>
            </tr>                          
        </table>

        <h2 class="profile-title"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "6"}'>CONTACT INFORMATION</h2>
        <table class="profile-table"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "6.5"}'>
        	<tr class="profile-tr">
            	<td class="profile-td1">Email</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getEmail();?></td>
            </tr>
        	<tr class="profile-tr">
            	<td class="profile-td1">Phone</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><span class="web-phone"><?php echo $userDetails->getPhoneNo();?></td>
            </tr>            
        	<tr class="profile-tr">
            	<td class="profile-td1">Address</td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getAddress();?></td>
            </tr>                          
        </table>        
    </div>

</div>

<!-- Withdraw Points Modal -->
<div id="withdraw-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css login-modal-content withdraw-modal-content">
    <span class="close-css close-withdraw">&times;</span>
    <h1 class="white-h1">WITHDRAW</h1>
    <form >
    <!-- Wen Jie, help them fill in their bank details-->
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/bank.png" class="login-input-icon" alt="Bank" title="Bank"></span>
        	<input class="login-input name-input clean" type="text" placeholder="Bank">
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/bank-account-number.png" class="login-input-icon" alt="Account No." title="Account No."></span>
        	<input class="login-input name-input clean" type="text" placeholder="Account No.">
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/user.png" class="login-input-icon" alt="Account No." title="Account No."></span>
        	<input class="login-input name-input clean" type="text" placeholder="Bank Account Holder Name">
        </div>
        <div class="input-grey-div">
        	<div class="left-points-div">
                <span class="input-span"><img src="img/coin.png" class="login-input-icon" alt="Points" title="Points"></span>
                <input class="login-input name-input clean" type="number" placeholder="Withdraw Amount">
            </div>
            <div class="right-rm-div">
            	<!--- Total Amount here-->RM40
            </div>
        </div> 
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="E-Pin" title="E-Pin"></span>
        	<input class="login-input password-input clean" type="password" placeholder="E-Pin"><span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" ></span>
        </div>        
        <!-- Wen Jie I think can remove remember me-->       
   <!--
        if(isset($_COOKIE['remember-oilxag']) && $_COOKIE['remember-oilxag'] == 1) 
        {
          ?>
            <div class="left-check"><input checked type="checkbox" class="checkbox-remember-me" name="remember-me"> <p class="remember-me-p"> Remember Me</p></div>
       
        }
        else 
        {
    
            <div class="left-check"><input type="checkbox" class="checkbox-remember-me" name="remember-me"> <p class="remember-me-p"> Remember Me</p></div>
      
        }
    -->
        <div class="clear"></div>
        <button class="yellow-button clean" type="submit">Withdraw</button>
      
    </form>
  </div>

</div>

<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Data Update Successfully";
        }
        if($_GET['type'] == 11)
        {
            $messageType = "Picture Update Successfully";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Picture upload failed, please try again.";
        }
        if($_GET['type'] == 13)
        {
            $messageType = "Please select a picture.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "E-Pin Renew Successfully";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "E-Pin Added Successfully";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Add E-Pin !";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Error !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 4)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Withdraw Request Success!";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "The Withdraw Money Should Not Pass the Amount On The Wallet!!";
        }

        if($_GET['type'] == 3)
        {
            $messageType = "Wrong E-Pin!";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "You Cant Proceed To Convert the Cash To Point Until Previous Withdraw Request Has Completed!!";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Transfer Cash To Point Success";
        }
        if($_GET['type'] == 6)
        {
            $messageType = "You Cant Proceed To Withdraw the Cash Until Previous Withdraw Request Has Completed!!";
        }
        if($_GET['type'] == 7)
        {
            $messageType = "No Existing IC Number!!";
        }
        if($_GET['type'] == 8)
        {
            $messageType = "The Point Succesfully Transfer!!";
        }
        if($_GET['type'] == 9)
        {
            $messageType = "Atleast 10 Points Should Left On The Wallet!!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 5)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Complete Your Profile To Withdraw The Cash!!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Withdraw Error!";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Picture upload failed, please try again.";
        }
        if($_GET['type'] == 13)
        {
            $messageType = "Please select a picture.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>