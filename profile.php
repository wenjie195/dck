<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Images.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/TransactionHistory.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];
$userPic = getImages($conn," WHERE pid = ? ",array("pid"),array($userDetails->getPicture()),"s");
$userProPic = $userPic[0];

$asd = $userDetails->getBankAccountNo();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php require_once dirname(__FILE__) . '/header.php'; ?>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dcksupreme.asia/profile.php" />
    <meta property="og:title" content="Profile | DCK Supreme" />
    <title>Profile | DCK Supreme</title>
    <meta property="og:description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="description" content="DCK Engine Oil Booster is suitable for all type of engine oil, manual transmission fluid and hydraulic fluid." />
    <meta name="keywords" content="DCK®, dck supreme,supreme,dck, engine oil booster, engine oil, booster, manual transmission fluid, hydraulic fluid, price, protects machinery, reduces
    breakdown, downtime, prolongs engine lifespan, restores wear and tear parts, reduces maintenance cost, extends oil change interval, saves fuel, reduces engine vibration,
    noisiness and temperature, dry cold start,etc">
    <link rel="canonical" href="https://dcksupreme.asia/profile.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header-sherry.php'; ?>
<div class="clear"</div>

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<!-- <div class="yellow-body padding-from-menu same-padding"> -->
<div class="yellow-body padding-from-menu same-padding"  id="firefly">

<!--function to display profile picture-->
<?php include 'profilePictureDispalyPartA.php'; ?>

    <?php
    if($asd == "")
    {
    ?>

    <div class="right-profile-div">

            <h3>Please Update Your Bank Details</h3>
            <a href="editProfile.php" >
                <button class="confirm-btn text-center white-text clean black-button">Click Here !</button>
            </a>

    </div>

    <?php
    }
    else
    { ?>

    <div class="right-profile-div">

        <div class="profile-tab width100"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2"}'>
        	<!-- <a href="#" class="profile-tab-a active-tab-a">ABOUT</a>
            <a href="referee.php" class="profile-tab-a">MY REFEREE</a> -->
            <a href="#" class="profile-tab-a active-tab-a"><?php echo _MAINJS_PROFILE_ABOUT ?></a>
            <a href="referee.php" class="profile-tab-a"><?php echo _MAINJS_PROFILE_MY_REFEREE ?></a>

            <div class="clear"></div>

            
        </div>

        <div class="left-profile-div-user left-100"   data-animate-scroll='{"x": "100","y": "0", "alpha": "0", "duration": "2"}'>
            <h1 class="username left-info"> <?php echo $userDetails->getUsername();?> </h1>
            <h1 class="h1-title h1-before-border shipping-h1 right-info"><a href="profile.php?lang=en" class="white-text title-tab-a">EN</a> | <a href="profile.php?lang=ch" class="white-text title-tab-a">中文</a></h1>
        </div>
        <div class="right-profile-div-transfer hide">

            <img src="img/star.png" class="voucher-icon" alt="Transfer Points" title="Transfer Points">
        	<img src="img/coupon.png" class="voucher-icon voucher1" alt="Transfer Voucher" title="Transfer Voucher">
        </div>
        <div class="clear"></div>


        <div class="width100 oveflow wallet-big-div"   data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "2"}'>
        	<div class="width50 first-50">
            	<div class="white50div">
                    <img src="img/cash2.png" class="cash-icon">
                    <!-- <h2>Cash</h2> -->
                    <h2><?php echo _MAINJS_PROFILE_CASH ?></h2>
                    <p>RM<?php echo $userDetails -> getWithdrawAmount(); ?></p>
                </div>
                <!-- <div class="open-withdraw withdraw-button gold-hover-div">Withdrawal</div>
                <div class="open-convert convert-button gold-hover-div">Convert to Points</div> -->
                <div class="open-withdraw withdraw-button gold-hover-div"><?php echo _MAINJS_PROFILE_WITHDRAWAL ?></div>
                <div class="open-convert convert-button gold-hover-div"><?php echo _MAINJS_PROFILE_CONVERT_TO_POINTS ?></div>
            </div>

            <div class="width50 second-width50 wallet-big-div"   data-animate-scroll='{"x": "0","y": "-100", "alpha": "0", "duration": "2"}'>
				<div class="white50div">
                    <img src="img/points.png" class="cash-icon">
                    <!-- <h2>Point</h2> -->
                    <h2><?php echo _MAINJS_PROFILE_POINT ?></h2>
                    <p><?php echo $userDetails -> getUserPoint(); ?></p>
                </div>

                <!-- <div class="open-transfer withdraw-button gold-hover-div">Transfer</div>
                <a href="addReferee.php" class="open-convert convert-button gold-hover-div color-black display-block">Add Referee</a> -->
                <div class="open-transfer withdraw-button gold-hover-div"><?php echo _MAINJS_PROFILE_TRANSFER ?></div>
                <a href="addReferee.php" class="open-convert convert-button gold-hover-div color-black display-block"><?php echo _MAINJS_PROFILE_ADD_REFEREE ?></a>


            </div>
        </div>
        <div class="clear"></div>
        <div class="divider-yes"></div>
        <div class="clear"></div>
        <div class="width100 oveflow hide">
         	<div class="three-point-div">
            	<h2>Sponsor Bonus</h2>
                <p>100</p>
            </div>
         	<div class="three-point-div middle-point-div">
            	<h2>Leadership Matching Bonus</h2>
                <p>100</p>
            </div>
         	<div class="three-point-div">
            	<h2>Annual Bonus</h2>
                <p>100</p>
            </div>
         </div>
        <div class="clear"></div>


        <!-- <h2 class="profile-title" data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "3"}'>BASIC INFORMATION</h2> -->
        <h2 class="profile-title" data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "3"}'><?php echo _MAINJS_PROFILE_BASIC_INFORMATION ?></h2>
        <table class="profile-table"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "3.5"}'>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">IC Number</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_IC_NUMBER ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getIcNo();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Full Name</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_FULLNAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getFullname();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Username</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_USERNAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getUsername();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Birthday</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_BIRTHDAY ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getBirthday();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Gender</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_GENDER ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getGender();?></td>
            </tr>
        </table>


        <!-- <h2 class="profile-title"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "4"}'>BANK INFORMATION</h2> -->
        <h2 class="profile-title"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "4"}'><?php echo _MAINJS_PROFILE_BANK_INFORMATION ?></h2>
        <table class="profile-table"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "4.5"}'>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Bank</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_BANK ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getBankName();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Acc. Holder Name</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_ACCOUNT_HOLDER_NAME ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getBankAccountHolder();?></td>
            </tr>
            <tr class="profile-tr">
                <!-- <td class="profile-td1">Account No.</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_ACCOUNT_NUMBER ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getBankAccountNo();?></td>
            </tr>
        </table>

        <!-- <h2 class="profile-title"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "5"}'>VEHICLES INFORMATION</h2> -->
        <h2 class="profile-title"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "5"}'><?php echo _MAINJS_PROFILE_VEHICLES_INFORMATION ?></h2>
        <table class="profile-table"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "5.5"}'>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Car Model</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_CAR_MODEL ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getCarModel();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Year</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_YEAR ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getCarYear();?></td>
            </tr>
        </table>

        <!-- <h2 class="profile-title"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "6"}'>CONTACT INFORMATION</h2> -->
        <h2 class="profile-title"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "6"}'><?php echo _MAINJS_PROFILE_CONTACT_INFORMATION ?></h2>
        <table class="profile-table"  data-animate-scroll='{"x": "-100","y": "0", "alpha": "0", "duration": "6.5"}'>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Email</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_EMAIL ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getEmail();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Phone</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_PHONE ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><span class="web-phone"><?php echo $userDetails->getPhoneNo();?></td>
            </tr>
        	<tr class="profile-tr">
                <!-- <td class="profile-td1">Address</td> -->
                <td class="profile-td1"><?php echo _MAINJS_PROFILE_ADDRESS ?></td>
                <td class="profile-td2">:</td>
                <td class="profile-td3"><?php echo $userDetails->getAddress();?></td>
            </tr>
        </table>

    <?php
    }
    ?>

    </div>

</div>


<!-- Withdraw Points Modal -->
<div id="withdraw-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css login-modal-content withdraw-modal-content">
    <span class="close-css close-withdraw">&times;</span>
    <h1 class="white-h1">WITHDRAWAL</h1>
    <form method="POST"  action="utilities/withdrawAmountFunction.php">
    <!-- Wen Jie, help them fill in their bank details-->
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/bank.png" class="login-input-icon" alt="Bank" title="Bank"></span>
        	<input oninput="this.value = this.value.toUpperCase()" class="login-input name-input clean" type="text" id="bank_name" name="bank_name" readonly value="<?php echo $userDetails->getBankName();?>">
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/bank-account-number.png" class="login-input-icon" alt="Account No." title="Account No."></span>
        	<input class="login-input name-input clean" type="text" id="acc_number" name="acc_number"  readonly="readonly" value="<?php echo $userDetails->getBankAccountNo();?>">
        </div>
    	<div class="input-grey-div">
            <span class="input-span"><img src="img/user.png" class="login-input-icon" alt="Account No." title="Account No."></span>
        	<input oninput="this.value = this.value.toUpperCase()" class="login-input name-input clean" type="text" id="usernameaccount" name="usernameaccount" readonly value="<?php echo $userDetails->getBankAccountHolder();?>">
        </div>
        <div class="input-grey-div">
        	<div class="left-points-div">
                <span class="input-span"><img src="img/coin.png" class="login-input-icon" alt="Points" title="Points"></span>
                <input class="login-input name-input clean" id="withdraw_amount" name="withdraw_amount" type="number" placeholder="Withdraw Amount" >
            </div>

        </div>
				<div class="input-grey-div">
					<span class="input-span"><img src="img/lock.png" class="login-input-icon" alt="E-Pin" title="E-Pin"></span>
				<input class="login-input password-input clean" id="withdraw_epin" type="password" placeholder="E-Pin" name="withdraw_epin"><span class="visible-span"><img src="img/visible.png" class="login-input-icon" alt="View Password" title="View Password" required></span>
				</div>

                <p class="white-text explanation-p">
                    Withdrawal Charges: RM 10 per transaction<br>
                    Min Withdrawal amount: RM 100 per transaction
                </p>

        <div class="clear"></div>
        <button class="yellow-button clean" type="submitwithdraw" value="<?php if($_SERVER['REQUEST_METHOD'] == 'POST'){
				    createOrder($conn,$uid);} ?>" >Withdraw</button>

    </form>




  </div>

</div>


<?php require_once dirname(__FILE__) . '/footer.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Data Update Successfully";
        }
        if($_GET['type'] == 11)
        {
            $messageType = "Picture Update Successfully";
        }
        if($_GET['type'] == 12)
        {
            $messageType = "Picture upload failed, please try again.";
        }
        if($_GET['type'] == 13)
        {
            $messageType = "Please select a picture.";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "E-Pin Renew Successfully";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "E-Pin Added Successfully";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Fail To Add E-Pin !";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Error !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 4)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Withdraw Request Success!";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "The Withdraw Money Should Not Pass the Amount On The Wallet!!";
        }
        // if($_GET['type'] == 2)
        // {
        //     $messageType = "The Amount Should Atleast RM10 Should Left On The Wallet!!";
        // }
        if($_GET['type'] == 3)
        {
            $messageType = "Wrong E-Pin!";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "You Cant Proceed To Convert the Cash To Point Until Previous Withdraw Request Has Completed!!";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Transfer Cash To Point Success";
        }
        if($_GET['type'] == 6)
        {
            $messageType = "You Cant Proceed To Withdraw the Cash Until Previous Withdraw Request Has Completed!!";
        }
        if($_GET['type'] == 7)
        {
            $messageType = "No Existing Username!!";
        }
        if($_GET['type'] == 8)
        {
            $messageType = "The Point Succesfully Transfer!!";
        }
        if($_GET['type'] == 9)
        {
            $messageType = "Your Transfer Point Should Not Pass Your Current Point!!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 5)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Complete Your Profile To Withdraw The Cash!!";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "The Point Fail To Transfer. Can Only Transfer on Your Own Downline!!";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Cant Transfer To Own Account.";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "The Point Fail To Transfer. No Username Found!!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>
